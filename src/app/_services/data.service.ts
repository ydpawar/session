import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class DataService {

  private menuData = new BehaviorSubject({});

  constructor() {
  }

  fetchMenuData(): Observable<any> {
    return this.menuData.asObservable();
  }

  sendMenuData(data) {
    this.menuData.next(data);
  }

  getSportMarketPath(sportId: any) {
    switch (sportId) {
      case '4':
        return 'cricket/inplay';
        break;
      case '2':
        return 'tennies/tennies-list';
        break;
      case '1':
        return 'football/football-list';
        break;
      case '7':
        return 'binary/binary-list';
        break;
      case '6':
        return 'election/election-list';
        break;
      case '10':
        return 'jackpot/jackpot-list';
        break;
      case '11':
        return 'casino/casino-list';
        break;
      case '998917':
        return 'games/volleyball/' + sportId;
        break;
      case '7522':
        return 'games/basketball/' + sportId;
        break;
      case '3503':
        return 'games/dart/' + sportId;
        break;
      case '22':
        return  'games/table-tennis/' + sportId;
        break;
      case '13':
        return  'games/badminton/' + sportId;
        break;
      case '14':
        return  'games/kabaddi/' + sportId;
        break;
      case '16':
        return  'games/boxing/' + sportId;
        break;
    }
  }

}
