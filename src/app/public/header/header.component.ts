import {Component, OnInit, AfterViewInit} from '@angular/core';
import {LoginService} from '../../_api/index';
import {MenuService} from '../../_api';
import {AuthIdentityService, DataService, ToastrService} from './../../_services/index';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

declare var $;


interface HeaderManage {
  username: string;
  id: string;
  role: any;
  activityLogCount: any;
}

class HeaderManageObj implements HeaderManage {
  username: string;
  id: string;
  role: any;
  activityLogCount: any;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [LoginService, AuthIdentityService]
})


export class HeaderComponent implements OnInit, AfterViewInit {
  aList: HeaderManage[] = [];
  username: string;
  id: string;
  role: any;
  isEmpty = false;
  activityLogCount: any;

  // tslint:disable-next-line:max-line-length
  constructor(private service: MenuService, private auth: LoginService, private route: Router, private authIdentity: AuthIdentityService, private toastr: ToastrService, private dataServices: DataService) {
  }

  ngOnInit() {
    this.role = 0;
  }

  ngAfterViewInit() {
    this.dataServices.fetchMenuData().subscribe((response: any) => {
      if (response) {
        this.username = response.username;
        this.id = response.id;
        this.role = response.role;
        this.activityLogCount = response.activityLogCount;
      } else {
        this.isEmpty = true;
      }
    });
  }

  logout() {
    swal.fire({
      title: 'Are you sure to logout?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, logged out!'
    }).then((result) => {
      if (result.value) {
        this.auth.doLogout().subscribe((res) => this.onSuccess(res));
      }
    });
  }

  onSuccess(res) {
    this.authIdentity.logOut(this.route);
    // console.log(res);
  }


  onOpenRightMenu(e) {
    e.preventDefault();
    $('body').toggleClass('right-bar-enabled');
  }

  onOpenLeftMenu(e) {
    e.preventDefault();
    const $body = $('body'), $window = $(window);
    $body.toggleClass('sidebar-enable'), 768 <= $window.width() ? $body.toggleClass('enlarged') : $body.removeClass('enlarged'), this._resetSidebarScroll();
  }

  _resetSidebarScroll() {
    $('.slimscroll-menu').slimscroll({
      height: 'auto',
      position: 'right',
      size: '8px',
      color: '#9ea5ab',
      wheelStep: 5,
      touchScrollStep: 20
    });
  }


}
