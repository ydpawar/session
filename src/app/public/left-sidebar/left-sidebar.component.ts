import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MenuService} from '../../_api';
import {Router} from '@angular/router';
import {AuthIdentityService, ToastrService, DataService} from '../../_services';
import {ScriptLoaderService} from '../../_services';
import swal from 'sweetalert2';

declare const $: any;


@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.css']
})
export class LeftSidebarComponent implements OnInit, AfterViewInit {

  mainMenuList: any;
  userinfo: any;
  onLineClient: string;
  user: any;

  // tslint:disable-next-line:max-line-length
  constructor(private service: MenuService,
              private route: Router,
              private authIdentity: AuthIdentityService,
              private toaster: ToastrService,
              private script: ScriptLoaderService,
              private dataServices: DataService) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.getMainMenuData();
  }

  ngAfterViewInit() {
    // this.loadScript();
    // this.script.load('script', 'assets/js/common-fun.js');
  }

  async getMainMenuData() {
    await this.service.getList().subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      (data) => {
        this.onSuccess(data);
      },
      error => {
        this.toaster.error(error.message, 'Something want wrong..!');
      });
  }

  onSuccess(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.mainMenuList = response.data;
        this.userinfo = response.userinfo;
        this.onLineClient = response.onLineClient;
        const datasend = {username: response.userinfo.username, id: response.userinfo.id, role: response.userinfo.role, activityLogCount: response.activityLogCount};
        this.dataServices.sendMenuData(datasend);
      }
      // console.log(this.mainMenuList);
    }
  }


  allUserLogout() {
    swal.fire({
      title: 'Are you sure to want all user logout?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, logged out!'
    }).then((result) => {
      if (result.value) {
        this.service.allUserLogout().subscribe((res) => this.onSuccessUserLogout(res));
      }
    });
  }

  onSuccessUserLogout(res) {
    if (res.status === 1) {
      this.getMainMenuData();
    }
  }

  /*changeClass(id) {
    if (id === 1) {
      $('.mainmenuother').removeClass('active');
      $(this).addClass('active');

      $('.activesubmenu1,.activesubmenu').toggle();
    } else {
      $('.mainMenuUser').removeClass('active');
      $('.mainmenuother').removeClass('active');
      $(this).addClass('active');
      $('.activesubmenu1,.activesubmenu').hide();
    }

  //  alert('item' + id);
  }*/


  changeClass(id, title, selectedId, role) {
    $('.activemenu').removeClass('active');
    $('#' + selectedId).parent('li').addClass('active');
    $('#' + selectedId).addClass('active');
    if (title === 'Users') {
      $('.inActivegroup').toggle();
      if (role !== 1) {
        $('.activesubmenu1').hide();
      }
    } else {
      $('.inActivegroup').hide();
    }
  }

  changeSubClass(selectedId) {
    $('.activemenu').removeClass('active');
    $('#' + selectedId).parent('li').addClass('active');
    $('#' + selectedId).addClass('active');
  }


}


