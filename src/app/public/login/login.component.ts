import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../_api/index';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthIdentityService, ToastrService} from './../../_services/index';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthIdentityService]
})
export class LoginComponent implements OnInit {
  model: any = {};
  passType = 'password';
  returnUrl: string;
  errorMsg: string;
  public submitted: boolean;

  constructor(private auth: LoginService, private route: Router, private authIdentity: AuthIdentityService, private toastr: ToastrService) {
    this.submitted = false;
  }

  ngOnInit() {
    // this.doLogin();
  }

  doLogin() {
    if (this.model.email && this.model.password) {
      this.submitted = true;
      const data = {
        username: this.model.email,
        password: this.model.password,
      };

      this.auth.doLogin(data).subscribe(
        (data) => {
          this.intSuccessLogin(data);
        },
        error => {
          this.submitted = false;
          console.log('fff');
          this.toastr.error(error.message, 'Something want wrong..!');
        });
    } else {
      this.errorMsg = 'Please enter username and password';
    }
  }

  intSuccessLogin(response) {
    if (response.status !== undefined && response.status) {
      this.submitted = false;
      this.authIdentity.setUser(response.data);
      this.route.navigate(['/']);
    } else {
      // this.toastr.error(response.error.message, 'Something want wrong..!');
    }
  }

}
