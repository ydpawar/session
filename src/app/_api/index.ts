export { LoginService } from './auth/login.service';
export { CricketService } from './events/cricket.service';
export { UsersService } from './users/users.service';
export { MenuService } from './common/menu.service';
export { MarketService } from './events/market.service';
export { GameOverService } from  './events/GameOverService.service';
export { DashboardService } from  './events/dashboard.service';
export { SettingService } from  './setting/setting.service';

export { HistoryService } from  './history/history.service';
