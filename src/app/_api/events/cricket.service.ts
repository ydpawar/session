import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';


import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';
// import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from '../../_services';

@Injectable()
export class CricketService {
  constructor(private http: HttpService) { }


  changestatus(data, status): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'changesportstatus?id=' + data + '&status=' + status;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  changerulestatus(data, status): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'changerulestatus?id=' + data + '&status=' + status;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  updateAccountTable(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'update-account-table?id=' + data;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }


  changesportSetting(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'changesportSetting';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }



  changeeventstatus(data, status): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'changeeventstatus?id=' + data + '&status=' + status;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  seteventstatus(data, status): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'seteventstatus?id=' + data + '&status=' + status;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createnewevent(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'createnewevent'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  commentaryupdate(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'commentaryupdate'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  commentarylist(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'commentarylist'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }




  getEventSetting(eid, sid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'getEventSetting?eid=' + eid + '&sid=' + sid;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = eid;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  addEventSetting(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'addEventSetting' ;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createnewgametype(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'addGameType'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  // getSportList
  getSportList() {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'sportlist';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }


  getRules(id) {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'sportrule?id=' + id;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }


  createnewrule(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'createnewrule'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }


  createnewtable(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'createnewtable'; // setting/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createnewsportevent(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'createnewsport'; // events/cricket/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  addserries(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'add-serries';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  eventListLoad(data, sid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'get-events-list?sid=' + sid;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  sportruleListLoad(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'sportrulelist';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gettablelist(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'gettablelist';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  sportListLoad(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'sportlist';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }


  cricketeventListLoad(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'cricketeventlist?sid=' + data;
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotGameType(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'getGameType';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage =  'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

 /* filter(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  create(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/create';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  currenteventscore(id): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/current-event-score/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameover(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    //httpRequestModel.url = 'events/cricket/gameover-' + data.type;
    httpRequestModel.url = 'events/game-over/match-odds';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameoverMOdd(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    //httpRequestModel.url = 'events/cricket/gameover-' + data.type;
    httpRequestModel.url = 'events/game-over/manual-match-odds';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  abundant(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/game-over/abundant';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gamerecall(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/game-recall/match-odds';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameovermfancy(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    //httpRequestModel.url = 'events/cricket/gameover-fancy';
    httpRequestModel.url = 'events/game-over/manual-fancy';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  addrulebookmaker(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    //httpRequestModel.url = 'events/cricket/gameover-fancy';
    httpRequestModel.url = 'events/cricket/manual-match-odds-add-rule';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameoverfancy(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    //httpRequestModel.url = 'events/cricket/gameover-fancy';
    httpRequestModel.url = 'events/game-over/fancy';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameoverLottery(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    //httpRequestModel.url = 'events/cricket/gameover-' + data.type;
    httpRequestModel.url = 'events/game-over/lottery';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameoverJackpot(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/game-over/jackpot';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gamerecallfancy(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/game-recall/fancy';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gamerecalllottery(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/game-recall/lottery';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gamerecalljackpot(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/game-recall/jackpot';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  setting(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();

    if(data.id != undefined){
      httpRequestModel.url = 'events/cricket/setting';
    }else{
      httpRequestModel.url = 'events/cricket/setting/' + id;
    }

    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  createmanualsession(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/create-manual-session/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  } */



  /*createmanualsessionlottery(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/create-manual-session-lottery/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  updatemanualsession(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/update-manual-session';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  updatefancy(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/setting';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  updatemanualsessionlotterynumber(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/update-manual-session-lottery-number/';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  updatemanualsessionballtoball(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/update-manual-session-balltoball/';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionmatchodd(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-match-odd/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  generatematchodd(id): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/generate-match-odd/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = '';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  removematchodd(id): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/remove-match-odd/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = '';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  updatemanualsessionmatch(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/update-manual-session-match-odd';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsession(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionlottery(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-lottery/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionlotterynumber(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-lottery-number/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionballtoball(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-balltoball/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  changestatus(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/status';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionchangestatus(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-status/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  lotteryChangeStatus(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-lottery-status/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionchangestatusballtoball(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-status-balltoball/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionballtoballchangestatus(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-balltoball-status/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessiondelete(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-delete/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  manualsessionballtoballdelete(id,data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-balltoball-delete/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  eventlistchangestatus(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/eventlist-status';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  changestatusmarket(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/statusmarket';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  refreshmarket(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/refresh-market';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  refreshdreammarket(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/refresh-dream-market';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  delete(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/delete';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  view(id): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/view/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  settingview(id, data?): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.method = 'POST';
    httpRequestModel.header = 'Auth';

    if(data != undefined && data.id != undefined){
      httpRequestModel.url = 'events/cricket/setting-view';
      httpRequestModel.body = data;
    }else{
      httpRequestModel.url = 'events/cricket/setting-view/' + id;
    }

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  update(id, data) {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/update/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  market(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/market';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  commentarylist(id): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/commentary-list/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  commentaryupdate(id, data) {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/commentary/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  inplay(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/inplay';

    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  trash(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/trash';

    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  upcoming(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/upcoming';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }


  exchange(id): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/exchange-web/' + id;
    httpRequestModel.method = 'POST';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getSessionData(id){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-match-odd-data/'+id;
    httpRequestModel.method = 'GET';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getBetListData(id){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-match-odd-bet-list/'+id;
    httpRequestModel.method = 'GET';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getSessionData1(id){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-data/'+id;
    httpRequestModel.method = 'GET';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getBetListData1(id){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/manual-session-bet-list/'+id;
    httpRequestModel.method = 'GET';
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getProfitLoss(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'users/dashbord/get-profit-loss-fancy-book';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = false;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  getEventTitle(id) {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket/get-event-name/'+id;
    httpRequestModel.method = 'POST';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  } */

  /*JACKPOT API-END POINT*/

 /*  jackpotcreate(data) {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/create';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotmanage(id) {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/manage';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = {'event_id': id};
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotimport(data) {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/import-xls';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotsetting(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/setting';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotupdate(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/update';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;
    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotchangestatus(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/status';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;
    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotstatusdelete(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/delete';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;
    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotstatusdeleteall(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/delete-all';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;
    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotsuspendedall(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/suspended-all';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;
    return this.http.init(httpRequestModel, processDisplay);
  }

  jackpotsuspendedstatus(data){
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'events/cricket-jackpot/suspended';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = true;
    return this.http.init(httpRequestModel, processDisplay);
  } */

}
