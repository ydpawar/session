import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpService) { }

  doLogin(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'login';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'NoAuth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Validating User!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  urlEncode(obj: Object): string {
    const urlSearchParams = new URLSearchParams();
    for (const key in obj) {
      urlSearchParams.append(key, obj[key]);
    }
    return urlSearchParams.toString();
  }

  doLogout(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'logout';
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Validating User!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  changePassword(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'users/change-password';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Validating User!";
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  checkNewUser(data): Observable<JSON> {
    let httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'setting/check-new-user';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    let processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = "Loading Data!";
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }
}
