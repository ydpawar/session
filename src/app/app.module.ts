import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BaseComponent} from './common/commonComponent';
import {HttpClientModule} from '@angular/common/http';
import {HttpService, ScriptLoaderService, DataService} from './_services/index';
import {ToastrService} from './_services/toastr.service';
import {AuthGuard, AuthGuardChild} from './_guards/auth.guard';
import {HrefPreventDefaultDirective} from './_directives/href-prevent-default.directive';
import {UnwrapTagDirective} from './_directives/unwrap-tag.directive';
import {SafePipe} from './_directives/safe.pipe';


import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './public/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ThemeComponent} from './theme/theme.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

 import { DataTablesModule } from 'angular-datatables';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    loadChildren: './theme/theme.module#ThemeModule',
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BaseComponent,
    HrefPreventDefaultDirective,
    UnwrapTagDirective,
    SafePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
     DataTablesModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // AppRoutingModule,
    RouterModule.forRoot(routes),
  ],
  providers: [HttpService, ToastrService, AuthGuard, AuthGuardChild, ScriptLoaderService, DataService],
  exports: [
    HrefPreventDefaultDirective
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
