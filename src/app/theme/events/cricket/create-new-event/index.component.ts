import {Component, OnInit, AfterViewInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {CricketService} from '../../../../_api/index';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-create-new-event',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class CreateNewEventComponent implements OnInit, AfterViewInit {
  frm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      event_name: ['', Validators.required],
      event_league: ['', Validators.required],
      event_time: ['', Validators.required],
      event_play_type: ['', Validators.required],
      runner1: ['', Validators.required],
      runner2: ['', Validators.required],
      sid: ['']
    });
  }


  submitForm2() {
    const data = this.getFormData2();
    if (this.frm.valid) {
      this.spinner.show();
      this.service.createnewevent(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.spinner.hide();
      this.router.navigate(['/events']);
    }
  }

  getFormData2() {
    this.frm.patchValue({
      sid: 4
    });

    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmEventName() {
    return this.frm.get('event_name');
  }

  get frmEventLeague() {
    return this.frm.get('event_league');
  }

  get frmEventTime() {
    return this.frm.get('event_time');
  }

  get frmEventPlayType() {
    return this.frm.get('event_play_type');
  }

  get frmRunner1() {
    return this.frm.get('runner1');
  }

  get frmRunner2() {
    return this.frm.get('runner2');
  }


}
