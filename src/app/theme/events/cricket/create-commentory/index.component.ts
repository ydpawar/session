import {Component, OnInit, AfterViewInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {CricketService} from '../../../../_api/index';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from './../../../../_services/toastr.service';

declare const $: any;

@Component({
  selector: 'app-create-commentory',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class CreateCommentoryComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  eid: string;
  commentaryList: any;
  event_name: string;
  SelectedId: string;
  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute, private spinner: NgxSpinnerService
  ) {
    this.createForm();
  }

  ngOnInit() {

    this.eid = this.route.snapshot.params.eid;
    this.SelectedId = 'All';
    this.getDetails();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: [''],
      eid: ['']
    });
  }


  submitForm() {
    this.spinner.show();
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.commentaryupdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  getDetails() {
    this.spinner.show();
    const data = {eid: this.eid};
    this.service.commentarylist(data).subscribe((res) => this.getDetailsSuccess(res));
  }

  getDetailsSuccess(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined && res.data != null) {
      this.commentaryList = res.data;
      this.event_name = res.name;
    }
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }


  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events']);
    }
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }


  getFormData() {
    this.frm.patchValue({
      sid: 4, eid: this.eid
    });
    const data = this.frm.value;
    return data;
  }

  changeCommentary(item) {
    this.spinner.show();
    $('#' + item).addClass('cmt-active');
    const data = {eid: this.eid, title: item, mType: this.SelectedId};
    this.service.commentaryupdate(data).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  applyFilters() {
  //  this.spinner.show();
    this.SelectedId = $('#SelectedId').val();
    // alert(this.SelectedId);
  }


  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      $('.btnActive').removeClass('cmt-active');
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    } else {
      this.toastr.error(res.error.message);
    }
  }

  ngAfterViewInit() {
    const nthis = this;
    document.onkeyup = function(e) {
      if (e.which === 83) { // sft+s
        //  nthis.diffrance = 1;
        nthis.changeCommentary('Suspended');
      }
    };
  }

  get frmtitle() {
    return this.frm.get('title');
  }
}
