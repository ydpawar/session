import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CricketComponent} from './index.component';

import {InplayComponent} from './../cricket/inplay/index.component';
import {InplayModule} from './../cricket/inplay/index.module';


import {CreateNewEventComponent} from './../cricket/create-new-event/index.component';
import {CreateNewEventModule} from './../cricket/create-new-event/index.module';

import {CreateCommentoryComponent} from './../cricket/create-commentory/index.component';
import {CreateCommentoryModule} from './../cricket/create-commentory/index.module';

const routes: Routes = [
  {
    path: '',
    component: CricketComponent,
    children: [
      {
        path: '',
        component: InplayComponent,
      },
      {
        path: 'create-commentory/:eid/:sid',
        component: CreateCommentoryComponent,
      },
      {
        path: 'create-new-event',
        component: CreateNewEventComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    InplayModule,
    CreateNewEventModule,
    CreateCommentoryModule
  ],
  exports: [RouterModule],
  declarations: [CricketComponent],
})
export class CricketModule {
}
