import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CricketService } from '../../../../_api/index';
import {DataService} from '../../../../_services';

@Component({
  selector: 'app-create-new-event',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class SetLimitsEventComponent implements OnInit {
  frm: FormGroup;
  eid: any;
  sid: any;
  setUrl: any;
  CList: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute,
    private dataServices: DataService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.eid = this.route.snapshot.params.id;
    this.sid = this.route.snapshot.params.sid;
    this.setUrl = this.dataServices.getSportMarketPath(this.sid);
    this.getSetting();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      upcoming_min_stack: ['', Validators.required],
      upcoming_max_stack: ['', Validators.required],
      max_odd: ['', Validators.required],
      overall_profit: ['', Validators.required],
      max_stack: [''],
      min_stack: [''],
      max_profit: [''],
      bet_delay: [''],
      sid: [''],
      eid: ['']
    });
  }


  getSetting() {
    this.service.getEventSetting(this.eid, this.sid).subscribe((res) => this.onRunnerSuccess(res));
  }
  onRunnerSuccess(res) {
    if (res) {
      this.CList = res.data;
      this.frm.patchValue({

        upcoming_min_stack: res.data.upcoming_min_stake,
        upcoming_max_stack: res.data.upcoming_max_stake,
        max_odd: res.data.max_odd_limit,
        max_stack: res.data.max_stack,
        min_stack: res.data.min_stack,
        max_profit: res.data.max_profit_limit,
        overall_profit: res.data.overall_profit_limit,
        bet_delay: res.data.bet_delay
      });
    }

  }
  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
     this.service.addEventSetting(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events/' + this.setUrl]);
    }
  }


  getFormData() {
    this.frm.patchValue({
      eid: this.eid
    });
    const data = this.frm.value;
    return data;
  }

  get frmUpcomingMinStack() { return this.frm.get('upcoming_min_stack'); }
  get frmUpcomingMaxStack() { return this.frm.get('upcoming_max_stack'); }
  get frmMaxOdd() { return this.frm.get('max_odd'); }
  get frmOverallProfit() { return this.frm.get('overall_profit'); }
  get frmMaxStack() { return this.frm.get('max_stack'); }
  get frmMinStack() { return this.frm.get('min_stack'); }
  get frmMaxProfit() { return this.frm.get('max_profit'); }
  get frmBetDelay() { return this.frm.get('bet_delay'); }

}
