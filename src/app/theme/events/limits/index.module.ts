import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { LimitsComponent } from './index.component';

import { InplayComponent } from './../cricket/inplay/index.component';
import { InplayModule } from './../cricket/inplay/index.module';


import { SetLimitsEventComponent } from './../limits/set-limits/index.component';
import { SetLimitsEventModule } from './../limits/set-limits/index.module';

import {AuthGuard, AuthGuardChild} from './../../../_guards/auth.guard';
import {AuthIdentityService} from './../../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: LimitsComponent,
    children: [
      {
        path: 'set-limits/:id',
        component: SetLimitsEventComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    InplayModule,
    SetLimitsEventModule
  ],
  exports: [RouterModule],
  declarations: [LimitsComponent],
})
export class LimitsModule {
}
