import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CricketService } from '../../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../../_services';

interface InplayManage {
  id: string;
  sportId: string;
  name: string;
  type: string;
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}

@Component({
  selector: 'app-inpay',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class InplayComponent implements OnInit, AfterViewInit {

  page = { start: 1, end: 5 };

  aList: InplayManage[] = [];
  actionRsList: InplayManage[] = [];
  marketList: InplayManage[] = [];
  eventList: InplayManage[] = [];
  actionBtnList: InplayManage[] = [];
  cItem: InplayManage;
  isEmpty = false;
  isLoading = false;

  constructor( private service: CricketService, private loadScript: ScriptLoaderService ) { }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    const data = 4;
    this.service.cricketeventListLoad(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
        const items = res.data.eventList;

        if (items.length) {
          // this.aList = res.data;
          this.actionBtnList = res.data.actionBtnList;
          this.actionRsList = res.data.actionRsList;
          this.marketList = res.data.marketList;
          this.eventList = res.data.eventList;
        } else {
          this.isEmpty = true;
        }

        // console.log(data);
        this.page.end = this.page.end + items.length - 1;
        setTimeout(() => { this.loadTreeScript(); }, 500);
    }
  }


  changeStatus(item: InplayManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';
    swal.fire({
      title: 'You want to ' + status + ' ' + item.name + '?',
      // text: "You won't be able to recover this entry!",
      // dangerMode: true,
      icon: 'warning',
     // buttons: ['Cancel', 'Yes'],
    })
      .then((willChange) => {
        if (willChange) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.changestatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');
  }

}
