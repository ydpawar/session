import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { FootballComponent } from './index.component';

import { FootballListComponent } from './../football/football-list/index.component';
import {  FootballListModule } from './../football/football-list/index.module';

import {AuthGuard, AuthGuardChild} from './../../../_guards/auth.guard';
import {AuthIdentityService} from './../../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component:  FootballComponent,
    children: [
      {
        path: '',
        component:   FootballListComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    FootballListModule
  ],
  exports: [RouterModule],
  declarations: [ FootballComponent],
})
export class  FootballModule {
}
