import {Component, OnInit, AfterViewInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {CricketService} from '../../../../_api/index';

@Component({
  selector: 'app-create-new-event',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateNewfootballComponent implements OnInit, AfterViewInit {
  frm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      event_name: ['', Validators.required],
      event_league: ['', Validators.required],
      event_time: ['', Validators.required],
      event_play_type: ['', Validators.required],
      runner1: ['', Validators.required],
      runner2: ['', Validators.required],
      sid: ['']
    });
  }


  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
      // this.frm.reset();
      this.service.createnewevent(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events/football/football-list']);
    }
  }


  getFormData() {
    this.frm.patchValue({
      sid: 1
    });
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmEventName() {
    return this.frm.get('event_name');
  }

  get frmEventLeague() {
    return this.frm.get('event_league');
  }

  get frmEventTime() {
    return this.frm.get('event_time');
  }

  get frmEventPlayType() {
    return this.frm.get('event_play_type');
  }

  get frmRunner1() {
    return this.frm.get('runner1');
  }

  get frmRunner2() {
    return this.frm.get('runner2');
  }


}
