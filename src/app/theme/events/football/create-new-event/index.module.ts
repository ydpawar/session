import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateNewfootballComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: CreateNewfootballComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateNewfootballComponent
  ]
})
export class CreateNewfootballModule {

}
