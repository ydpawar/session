import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CricketService } from '../../../../_api/index';

@Component({
  selector: 'app-create-election',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateElectionComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  sid: string;
  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.sid = this.route.snapshot.params.sid;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      event_name: ['', Validators.required],
      event_league: ['', Validators.required],
      sid: ['']
    });
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.createnewevent(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events/election/election-list']);
    }
  }


  getFormData() {
    this.frm.patchValue({
      sid: this.sid
    });
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmEventName() { return this.frm.get('event_name'); }
  get frmEventLeague() { return this.frm.get('event_league'); }


}
