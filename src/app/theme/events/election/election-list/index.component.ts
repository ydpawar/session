import {Component, OnInit, AfterViewInit} from '@angular/core';
import {CricketService} from '../../../../_api/index';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from './../../../../_services/toastr.service';

declare const $: any;

interface ElectionListManage {
  id: string;
  sportId: string;
  eventId: string;
  name: string;
  type: string;
  time: string;
  game_over: number;
  status: {
    text: string;
    cssClass: string;
  };
}

class ElectionListManageObj implements ElectionListManage {
  id: string;
  sportId: string;
  eventId: string;
  name: string;
  // tslint:disable-next-line:variable-name
  type: string;
  // tslint:disable-next-line:variable-name
  time: string;
  game_over: number;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-election-list',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class ElectionListComponent implements OnInit, AfterViewInit {

  page = {start: 1, end: 5};
  user: any;
  aList: ElectionListManage[] = [];
  cItem: ElectionListManage;
  actionRsList: ElectionListManage[] = [];
  marketList: ElectionListManage[] = [];
  eventList: ElectionListManage[] = [];
  actionBtnList: ElectionListManage[] = [];
  isEmpty = false;
  isLoading = false;
  pageLoad: any;

  constructor(private service: CricketService, private loadScript: ScriptLoaderService, private toastr: ToastrService,
              private spinner: NgxSpinnerService) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  setTitleOnClick(title) {
    window.sessionStorage.setItem('title', title);
  }

  applyFilters() {
    this.spinner.show();
    const data = 6;
    this.service.cricketeventListLoad(data).subscribe((res) => this.onSuccess(res));

  }

  onSuccess(res) {
    // declare const $: any;
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data.eventList;
        const data: ElectionListManage[] = [];

        if (items !== 'null') {
          this.aList = res.data;
          this.actionBtnList = res.data.actionBtnList;
          this.actionRsList = res.data.actionRsList;
          this.marketList = res.data.marketList;
          this.eventList = res.data.eventList;
        } else {
          this.isEmpty = true;
          this.spinner.hide();
        }

        // this.aList = data;
        // console.log(data);
        // this.page.end = this.page.end + items.length - 1;

        setTimeout(() => {
          this.loadTreeScript();
        }, 500);

        // this.config.totalItems = res.data.count;
      }
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }

  changeStatus(item: ElectionListManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }


  initStatus() {
    this.service.changeeventstatus(this.cItem.id, this.cItem.status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  closedStatus(item: ElectionListManage) {
    this.cItem = item;

    swal.fire({
      title: 'Confirm Game Over?',
      text: item.name,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad('closed');
        }
      });
  }

  changeEventType(id, title) {
    swal.fire({
      title: 'Confirm event type change?',
      text: title,
      input: 'select',
      inputOptions: {
        IN_PLAY: 'IN_PLAY',
        UPCOMING: 'UPCOMING'
      },
      inputPlaceholder: 'change event type',
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        // console.log(result);
        if (result && result.value && result.value !== undefined) {
          this.setChangeEventType(id, result.value);
        }
      });

  }

  setChangeEventType(id, result) {
    this.service.seteventstatus(id, result).subscribe((res) => this.onGameOverSuccess(res));
  }

  deleteStatus(item: ElectionListManage) {
    this.cItem = item;

    swal.fire({
      title: 'You want to delete ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad('delete');
        }
      });
  }


  initclosedStatusLoad(type) {
    this.service.seteventstatus(this.cItem.id, type).subscribe((res) => this.onGameOverSuccess(res));
  }

  onGameOverSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script', 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
