import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ElectionComponent} from './index.component';
import {ElectionListComponent} from './../election/election-list/index.component';
import {ElectionListModule} from './../election/election-list/index.module';
import {CreateElectionComponent} from './../election/create-election/index.component';
import {CreateElectionModule} from './../election/create-election/index.module';

const routes: Routes = [
  {
    path: '',
    component: ElectionComponent,
    children: [
      {
        path: '',
        component: ElectionListComponent,
      },
      {
        path: 'create-election/:sid',
        component: CreateElectionComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    ElectionListModule,
    CreateElectionModule
  ],
  exports: [RouterModule],
  declarations: [ElectionComponent],
})
export class ElectionModule {
}
