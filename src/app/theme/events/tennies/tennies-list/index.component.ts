import { Component, OnInit, AfterViewInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { CricketService } from '../../../../_api/index';
import { Router} from '@angular/router';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService, DataService} from '../../../../_services';
import { NgxSpinnerService } from 'ngx-spinner';
import {ToastrService} from './../../../../_services/toastr.service';
import {Subject} from 'rxjs';
declare const $: any;

interface ListOfRunner {
  runner: string;
  secId: string;
}
interface TenniesListManage {
  id: string;
  sportId: string;
  name: string;
  type: string;
  time: string;


  status: {
    text: string;
    cssClass: string;
  };
}

class TenniesListManageObj implements TenniesListManage {
  id: string;
  sportId: string;
  // tslint:disable-next-line:variable-name
  name: string;
  // tslint:disable-next-line:variable-name
  type: string;
  // tslint:disable-next-line:variable-name
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-election-list',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class TenniesListComponent implements OnInit, AfterViewInit {

  page = { start: 1, end: 5 };
  user: any;
  aList: TenniesListManage[] = [];
  actionRsList: TenniesListManage[] = [];
  marketList: TenniesListManage[] = [];
  rList: ListOfRunner[] = [];
  eventList: TenniesListManage[] = [];
  actionBtnList: TenniesListManage[] = [];
  cItem: TenniesListManage;
  isEmpty = false;
  isLoading = false;
  pageLoad: any;

  dtOptions1: DataTables.Settings = {};
  dtTrigger: Subject<TenniesListManage> = new Subject();

  dataTable: any;
  dtOptions: any;
  tableData = [];

  @ViewChild('dataTable', {static: false}) table;

  constructor( private service: CricketService, private loadScript: ScriptLoaderService, private toastr: ToastrService,
               private spinner: NgxSpinnerService,
               private dataServices: DataService,
               private renderer: Renderer2,
               private router: Router,
               private elRef: ElementRef
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
    document.querySelector('body').addEventListener('click', (event) => {
      const target: any = event.target; // Cast EventTarget into an Element
      if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'info') {
          //  this.changetitle(data.eid, data.marketid, data.title, data.info);
        } else if (data.type === 'status') {
          this.changeStatus(data.id, data.title, data.status);
        } else if (data.type === 'setting') {
          this.router.navigate(['/events/limits/set-limits/' + data.eid + '/' + data.sport]);
        } else if (data.type === 'delete') {
          this.deleteStatus(data.id, data.title);
        } else if (data.type === 'commentory') {
          this.router.navigate(['/events/cricket/create-commentory/' + data.eid + '/' + data.sport]);
        } else if (data.type === 'game-over') {
          this.closedStatus(data.id, data.title);
        }
      } else if (target.tagName.toLowerCase() === 'i' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'isTv') {
          this.doStatusUpdate(data.id, data.title);
        }
      } else  if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('typeLinksClicks')) {
        const data = target.dataset;
        if (data.type === 'eventtype' && data.etype !== 'CLOSED') {
          this.changeEventType(data.id, data.title);
        }
      } else if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('linksClicksTennis')) {
        const data = target.dataset;
        if (target.hasAttribute('target') && data.type === 'market') {
          // this.router.navigate([data.link]);
          this.setTitleOnClick(data.title);
          this.router.navigate([]).then(result => {  window.open(data.link, '_blank'); });
        }
      }
    });
  }



  setTitleOnClick(title) {
    window.sessionStorage.setItem('title', title);
  }
  applyFilters() {
    this.spinner.show();
    const data = 2;
    this.service.cricketeventListLoad(data).subscribe((res) => this.onSuccess1(res));

  }

  onSuccess1(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.tableData = [];
      const items = res.data.eventList;
      this.actionRsList = [];
      this.marketList = [];
      this.eventList = [];
      this.actionBtnList = [];
      if (items !== 'null') {
        this.actionBtnList = res.data.actionBtnList;
        this.actionRsList = res.data.actionRsList;
        this.marketList = res.data.marketList;
        this.eventList = res.data.eventList;
      } else {
        this.isEmpty = true;
        this.spinner.hide();
      }
      this.spinner.hide();
      this.tableData = res.data.eventList;

      this.initDataTableData();
      // this.bindEvents();

      this.spinner.hide();
    } else {
      this.spinner.hide();
      //  this.errorMsg = res.success.message;
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.DataTable(this.dtOptions);
    }
  }

  initDataTableData() {
    // console.log('initDataTableData');
    this.dtOptions = {
      destroy: true,
      data: this.tableData,
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      ordering:  false,
      columns: [
        {title: 'event name', data: 'name'},
        {
          title: 'Type',
          render: (data: any, type: any, full: any) => {
            let htm = '';
            let cls = '';
            if (full.type === 'IN_PLAY') {
              cls = 'bg-success';
            }
            htm = '<a  class="btn btn-xs btn-dark text-light border-0 ' + cls + ' ng-star-inserted typeLinksClicks" data-type="eventtype" data-id="' + full.id + '"  data-title="' + full.name + '" data-etype="' + full.type + '">' + full.type + '</a>';
            return htm;
          }
        },
        {title: 'date & time', data: 'eventTime'},
        {
          title: 'Market',
          render: (data: any, type: any, full: any) => {
            let htm = '';

            if (full.slug !== 'Jackpot') {


              // tslint:disable-next-line:only-arrow-functions
              $.each(full.marketList, function(i, v) {
                let cls = '';
                if (full.isBM === 1 && v.id === 1) {
                  cls = 'em-active';
                } else if (full.isBB === 1 && v.id === 8) {
                  cls = 'em-active';
                } else if (full.isF2 === 1 && v.id === 4) {
                  cls = 'em-active';
                } else if (full.isFF === 1 && v.id === 3) {
                  cls = 'em-active';
                } else if (full.isF3 === 1 && v.id === 5) {
                  cls = 'em-active';
                } else if (full.isKD === 1 && v.id === 7) {
                  cls = 'em-active';
                } else if (full.isOM === 1 && v.id === 10) {
                  cls = 'em-active';
                } else if (full.isVM === 1 && v.id === 11) {
                  cls = 'em-active';
                } else if (full.isOE === 1 && v.id === 12) {
                  cls = 'em-active';
                }
                // tslint:disable-next-line:max-line-length
                htm += '<a target="_blank" data-type="market" data-sport="' + full.sportId + '" data-link="' + v.url + '/' + full.eventId + '/' + full.sportId + '" data-title="' + full.name + '" class="btn btn-xs border border-success text-success text-uppercase mb-1 mr-1 ' + cls + ' linksClicksTennis">' + v.slug + '</a>';
              });
            }

            return htm;

          }
        },
        {
          title: 'Action',
          render: (data: any, type: any, full: any) => {
            let htm = '';
            let cls = 'btn-danger';
            let status = 'Inactive';
            if (full.status === 1) {
              cls = 'btn-success';
              status = 'Active';
            }
            if (full.game_over === 0 && this.user.role === 1 && full.type !== 'CLOSED') {
              htm += '<a class="btn btn-xs text-light border-0 bg-' + full.class1 + ' mr-1 mb-1"><i data-type="isTv" data-sport="' + full.sportId + '" data-id="' + full.id + '" class="mdi ' + full.class2 + ' modalClicks"></i></a>';
            }
            if (full.game_over === 0 && this.user.role === 1 && full.type !== 'CLOSED' && full.isStatus === 1) {
              htm += '<a data-type="status" data-sport="' + full.sportId + '" data-id="' + full.id + '" data-title="' + full.name + '" data-status="' + full.status + '" class="btn btn-xs mr-1 bg-secondary text-white ' + cls + ' success mb-1 mr-1 modalClicks">' + status + '</a>';
            }
            if (full.game_over === 0 && full.type !== 'CLOSED' && full.isSetting === 1) {
              htm += '<a data-type="setting" data-sport="' + full.sportId + '" data-eid="' + full.eventId + '" data-title="' + full.name + '" class="btn btn-xs mr-1 btn-dark text-white mb-1 modalClicks">Setting</a>';
            }

            if (full.game_over === 0 && this.user.role !== 7) {
              htm += '<a data-type="game-over"  data-sport="' + full.sportId + '" data-id="' + full.id + '" data-title="' + full.name + '" class="btn btn-xs btn-dark text-light text-uppercase mr-1 mb-1 modalClicks">Gameover</a>';
            }
            if (this.user.role === 1) {
              htm += '<a data-type="delete" data-sport="' + full.sportId + '" data-id="' + full.id + '"  data-title="' + full.name + '" class="btn btn-xs btn-danger text-light text-uppercase  mb-1 mr-1 modalClicks">Delete</a>';
            }

            return htm;
          }
        },
      ]
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }

  filterById(): void {
    this.dataTable.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  onSuccess(res) {
    // declare const $: any;
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data.eventList;
        const data: TenniesListManage[] = [];

        if (items !== 'null') {
          // this.aList = res.data;
          this.actionBtnList = res.data.actionBtnList;
          this.actionRsList = res.data.actionRsList;
          this.marketList = res.data.marketList;
          this.eventList = res.data.eventList;
        } else {
          this.isEmpty = true;
          this.spinner.hide();
        }

       // this.aList = data;
        // console.log(data);
      //  this.page.end = this.page.end + items.length - 1;

        setTimeout(() => { this.loadTreeScript(); }, 500);

       // this.config.totalItems = res.data.count;
      }
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  changeStatus(id, name, status) {

    let statustxt = 'Active';
    // @ts-ignore
    if (status === '1') {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus(id, status);
        }
      });
  }

  initStatus(id, status) {
    this.service.changeeventstatus(id, status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  closedStatus(id, name) {
    swal.fire({
      title: 'Confirm Game Over?',
      text: name,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad(id, 'closed');
        }
      });
  }

  doStatusUpdate(id, name) {

    swal.fire({
      title: 'Confirm live tv status change?',
      text: name,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad(id, 'isTv');
        }
      });
  }


  changeEventType(id,  title) {
    swal.fire({
      title: 'Confirm event type change?',
      text: title,
      input: 'select',
      inputOptions: {
        IN_PLAY: 'IN_PLAY',
        UPCOMING: 'UPCOMING'
      },
      inputPlaceholder: 'change event type',
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        // console.log(result);
        if (result && result.value && result.value !== undefined) {
          this.setChangeEventType(id, result.value);
        }
      });

  }

  setChangeEventType(id, result) {
    this.service.seteventstatus(id, result).subscribe((res) => this.onGameOverSuccess(res));
  }

  deleteStatus(id, name) {
    swal.fire({
      title: 'You want to delete ' + name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad(id, 'delete');
        }
      });
  }


  initclosedStatusLoad(id, type) {
    this.service.seteventstatus(id, type).subscribe((res) => this.onGameOverSuccess(res));
  }

  onGameOverSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
