import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { TenniesComponent } from './index.component';

import { TenniesListComponent } from './../tennies/tennies-list/index.component';
import {  TenniesListModule } from './../tennies/tennies-list/index.module';

import { CreateNewComponent } from './../tennies/create-new-event/index.component';
import { CreateNewModule } from './../tennies/create-new-event/index.module';

import {AuthGuard, AuthGuardChild} from './../../../_guards/auth.guard';
import {AuthIdentityService} from './../../../_services/auth-identity.service';


// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component:  TenniesComponent,
    children: [
      {
        path: '',
        component:   TenniesListComponent,
      },
      {
        path: 'create-new',
        component: CreateNewComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    TenniesListModule,
    CreateNewModule
  ],
  exports: [RouterModule],
  declarations: [ TenniesComponent],
})
export class  TenniesModule {
}
