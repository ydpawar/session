import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateCasinoComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: CreateCasinoComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateCasinoComponent
  ]
})
export class CreateCasinoModule {

}
