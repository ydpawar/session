import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { EventsComponent } from './index.component';

import { CricketComponent } from './cricket/index.component';
import { CricketModule } from './cricket/index.module';
import { GamesModule } from './games/index.module';
import {LimitsComponent} from './limits/index.component';
import {LimitsModule} from './limits/index.module';

import { InplayComponent } from './cricket/inplay/index.component';
import { InplayModule } from './cricket/inplay/index.module';

import { InplayGameComponent } from './games/inplay-game/index.component';
import { InplayGameModule } from './games/inplay-game/index.module';

import { CreateNewGameComponent } from './games/create-new-game/index.component';
import { CreateNewGameModule } from './games/create-new-game/index.module';


import { CreateGameCommentoryComponent } from './games/create-game-commentory/index.component';
import { CreateGameCommentoryModule } from './games/create-game-commentory/index.module';

import { CreateNewEventComponent } from './cricket/create-new-event/index.component';
import { CreateNewEventModule } from './cricket/create-new-event/index.module';

import {SetLimitsEventComponent} from './../events/limits/set-limits/index.component';
import {SetLimitsEventModule} from './../events/limits/set-limits/index.module';

import { CreateCommentoryComponent } from './cricket/create-commentory/index.component';
import { CreateCommentoryModule } from './cricket/create-commentory/index.module';

import { ElectionComponent } from './election/index.component';
import { ElectionModule } from './election/index.module';

import { CasinoModule } from './casino/index.module';

import { JackpotModule } from './jackpot/index.module';

import { ElectionListComponent } from './election/election-list/index.component';
import { ElectionListModule } from './election/election-list/index.module';

import { CreateElectionComponent } from './election/create-election/index.component';
import { CreateElectionModule } from './election/create-election/index.module';

import { CasinoListComponent } from './casino/casino-list/index.component';
import { CasinoListModule } from './casino/casino-list/index.module';

import { JackpotListComponent } from './jackpot/jackpot-list/index.component';
import { JackpotListModule } from './jackpot/jackpot-list/index.module';

import { CreateGameTypeComponent } from './jackpot/create-game-type/index.component';
import { CreateGameTypeModule } from './jackpot/create-game-type/index.module';

import { GameTypeComponent } from './jackpot/game-type/index.component';
import { GameTypeModule } from './jackpot/game-type/index.module';

import { CreateCasinoComponent } from './casino/create-casino/index.component';
import { CreateCasinoModule } from './casino/create-casino/index.module';

import { BinaryModule } from './binary/index.module';

import { BinaryListComponent } from './binary/binary-list/index.component';
import { BinaryListModule } from './binary/binary-list/index.module';

import { CreateBinaryComponent } from './binary/create-binary/index.component';
import { CreateBinaryModule } from './binary/create-binary/index.module';

import { CreateJackpotComponent } from './jackpot/create-jackpot/index.component';
import { CreateJackpotModule } from './jackpot/create-jackpot/index.module';

import { TenniesComponent } from './tennies/index.component';
import { TenniesModule } from './tennies/index.module';

import { CreateNewComponent } from './tennies/create-new-event/index.component';
import { CreateNewModule } from './tennies/create-new-event/index.module';

import { TenniesListComponent } from './tennies/tennies-list/index.component';
import { TenniesListModule } from './tennies/tennies-list/index.module';

import { FootballComponent } from './football/index.component';
import { FootballModule } from './football/index.module';

import { FootballListComponent } from './football/football-list/index.component';
import { FootballListModule } from './football/football-list/index.module';

import { CreateNewfootballComponent } from './football/create-new-event/index.component';
import { CreateNewfootballModule } from './football/create-new-event/index.module';

import {AuthGuard, AuthGuardChild} from './../../_guards/auth.guard';
import {AuthIdentityService} from './../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent,
    children: [
      {
        path: 'cricket/inplay',
        component: InplayComponent,
      },
      {
        path: 'cricket/create-new-event',
        component: CreateNewEventComponent,
      },
      {
        path: 'cricket/create-commentory/:eid/:sid',
        component: CreateCommentoryComponent,
      },
      {
        path: 'games/inplay-game/:sname/:sid',
        data: {
          shouldReuse: true,
        //  defaultReuseStrategy: true, // Ignore our custom route strategy
        //  resetReuseStrategy: true
        },
        component: InplayGameComponent,
      },
      {
        path: 'games/basketball/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/volleyball/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/dart/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/table-tennis/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/badminton/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/kabaddi/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/boxing/:sid',
        component: InplayGameComponent,
      },
      {
        path: 'games/create-new-game/:sid',
        component: CreateNewGameComponent,
      },
      {
        path: 'games/create-game-commentory/:eid/:sid',
        component: CreateGameCommentoryComponent,
      },
      {
        path: 'limits/set-limits/:id/:sid',
        component: SetLimitsEventComponent,
      },

      {
        path: 'tennies/tennies-list',
        component: TenniesListComponent,
      },
      {
        path: 'tennies/create-new',
        component: CreateNewComponent,
      },
      {
        path: 'football/football-list',
        component: FootballListComponent,
      },
      {
        path: 'football/create-new-football',
        component: CreateNewfootballComponent,
      },
      {
        path: 'election/create-new-event',
        component: CreateNewEventComponent,
      },
      {
        path: 'election/election-list',
        component: ElectionListComponent,
      },
      {
        path: 'election/create-election/:sid',
        component: CreateElectionComponent,
      },
      {
        path: 'casino/casino-list',
        component: CasinoListComponent,
      },
      {
        path: 'casino/create-casino/:sid',
        component: CreateCasinoComponent,
      },
      {
        path: 'binary/binary-list',
        component: BinaryListComponent,
      },
      {
        path: 'binary/create-binary/:sid',
        component: CreateBinaryComponent,
      },
      {
        path: 'jackpot/jackpot-list',
        component: JackpotListComponent,
      },
      {
        path: 'jackpot/game-type',
        component: GameTypeComponent,
      },
      {
        path: 'jackpot/create-jackpot/:sid',
        component: CreateJackpotComponent,
      },
      {
        path: 'jackpot/create-game-type',
        component: CreateGameTypeComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    InplayModule,
    CricketModule,
    GamesModule,
    CreateNewGameModule,
    CreateGameCommentoryModule,
    InplayGameModule,
    TenniesModule,
    TenniesListModule,
    FootballModule,
    FootballListModule,
    ElectionModule,
    ElectionListModule,
    CreateNewEventModule,
    CreateElectionModule,
    CasinoModule,
    CasinoListModule,
    CreateCasinoModule,
    BinaryModule,
    BinaryListModule,
    CreateBinaryModule,
    CreateNewModule, // tennies
    CreateNewfootballModule,
    JackpotModule,
    JackpotListModule,
    CreateJackpotModule,
    GameTypeModule,
    CreateGameTypeModule,
    SetLimitsEventModule,
    CreateCommentoryModule,
    LimitsModule
  ],
  exports: [RouterModule],
  declarations: [EventsComponent],
})
export class EventsModule {
}
