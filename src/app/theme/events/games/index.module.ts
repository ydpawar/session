import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { GamesComponent } from './index.component';

import { InplayGameComponent } from './../games/inplay-game/index.component';
import { InplayGameModule } from './../games/inplay-game/index.module';


import { CreateNewGameComponent } from './../games/create-new-game/index.component';
import { CreateNewGameModule } from './../games/create-new-game/index.module';

import { CreateGameCommentoryComponent } from './../games/create-game-commentory/index.component';
import { CreateGameCommentoryModule } from './../games/create-game-commentory/index.module';

import {AuthGuard, AuthGuardChild} from './../../../_guards/auth.guard';
import {AuthIdentityService} from './../../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: GamesComponent,
    children: [
      {
        path: '',
       // data: { shouldReuse: true },
        data: { reuseRoute: true,
          defaultReuseStrategy: true, // Ignore our custom route strategy
          resetReuseStrategy: true
        },
        component: InplayGameComponent,
      },
      {
        path: 'create-game-commentory/:eid/:sid',
        component: CreateGameCommentoryComponent,
      },
      {
        path: 'create-new-game/:sid',
        component: CreateNewGameComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    InplayGameModule,
    CreateNewGameModule,
    CreateGameCommentoryModule
  ],
  exports: [RouterModule],
  declarations: [GamesComponent],
})
export class GamesModule {
}
