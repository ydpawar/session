import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CricketService } from '../../../../_api/index';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services';
import { NgxSpinnerService } from 'ngx-spinner';
import {ToastrService} from './../../../../_services/toastr.service';
import {ActivatedRoute} from '@angular/router';
declare const $: any;

interface InplayGameManage {
  id: string;
  sportId: string;
  sportname: string;
  name: string;
  type: string;
  time: string;


  status: {
    text: string;
    cssClass: string;
  };
}

class InplayGameManageObj implements InplayGameManage {
  id: string;
  sportId: string;
  sportname: string;
  // tslint:disable-next-line:variable-name
  name: string;
  // tslint:disable-next-line:variable-name
  type: string;
  // tslint:disable-next-line:variable-name
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-inpay-game',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class InplayGameComponent implements OnInit, AfterViewInit {

  page = { start: 1, end: 5 };
  user: any;
  sportId: string;
  sportname: string;
  aList: InplayGameManage[] = [];
  actionRsList: InplayGameManage[] = [];
  marketList: InplayGameManage[] = [];
  eventList: InplayGameManage[] = [];
  actionBtnList: InplayGameManage[] = [];
  cItem: InplayGameManage;
  isEmpty = false;
  isLoading = false;
  pageLoad: string;

  constructor( private service: CricketService, private route: ActivatedRoute, private loadScript: ScriptLoaderService, private toastr: ToastrService,
               private spinner: NgxSpinnerService) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    } }

  ngOnInit() {
    this.sportId = this.route.snapshot.params.sid;
    this.applyFilters();
  }


  ngAfterViewInit() {
  //  this.loadScript.load('script' , 'assets/js/datatables.init.js');
  }
  setTitleOnClick(title) {
    window.sessionStorage.setItem('title', title);
  }

  applyFilters() {
    this.spinner.show();
    if (this.sportId === '998917') {
        this.sportname = 'volleyball';
    }
    if (this.sportId === '7522') {
      this.sportname = 'basketball';
    }
    if (this.sportId === '3503') {
      this.sportname = 'dart';
    }
    if (this.sportId === '22') {
      this.sportname = 'table-tennis';
    }
    if (this.sportId === '13') {
      this.sportname = 'badminton';
    }
    if (this.sportId === '14') {
      this.sportname = 'kabaddi';
    }
    if (this.sportId === '16') {
      this.sportname = 'boxing';
    }
    const data = this.sportId;
    this.service.cricketeventListLoad(data).subscribe((res) => this.onSuccess(res));

  }

  onSuccess(res) {
    // declare const $: any;
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data.eventList;
        const data: InplayGameManage[] = [];
        this.isEmpty = true;
        this.actionRsList = [];
        this.marketList = [];
        this.eventList = [];
        this.actionBtnList = [];
        if (items !== 'null') {
          // this.aList = res.data;
          this.actionBtnList = res.data.actionBtnList;
          this.actionRsList = res.data.actionRsList;
          this.marketList = res.data.marketList;
          this.eventList = res.data.eventList;
        } else {
          this.isEmpty = true;
          this.spinner.hide();
        }
        setTimeout(() => {
          this.loadTreeScript();
        }, 500);
      }
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }




  changeStatus(item: InplayGameManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatusLoad();
        }
      });
  }


  initStatusLoad() {
    this.service.changeeventstatus(this.cItem.id, this.cItem.status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  closedStatus(item: InplayGameManage) {
    this.cItem = item;

    swal.fire({
      title: 'Confirm Game Over?',
      text: item.name,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad('closed');
        }
      });
  }
  changeEventType(id,  title) {
    swal.fire({
      title: 'Confirm event type change?',
      text: title,
      input: 'select',
      inputOptions: {
        IN_PLAY: 'IN_PLAY',
        UPCOMING: 'UPCOMING'
      },
      inputPlaceholder: 'change event type',
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        // console.log(result);
        if (result && result.value && result.value !== undefined) {
          this.setChangeEventType(id, result.value);
        }
      });

  }

  setChangeEventType(id, result) {
    this.service.seteventstatus(id, result).subscribe((res) => this.onChangeClosedSuccess(res));
  }
  deleteStatus(item: InplayGameManage) {
    this.cItem = item;

    swal.fire({
      title: 'You want to delete ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad('delete');
        }
      });
  }


  initclosedStatusLoad(type) {
    this.service.seteventstatus(this.cItem.id, type).subscribe((res) => this.onChangeClosedSuccess(res));
  }

  onChangeClosedSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
