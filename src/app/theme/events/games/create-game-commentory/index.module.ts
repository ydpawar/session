import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateGameCommentoryComponent } from './index.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import {InplayGameModule} from '../inplay-game/index.module';

const routes: Routes = [
  {
    path: '',
    component: CreateGameCommentoryComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateGameCommentoryComponent
  ]
})
export class CreateGameCommentoryModule {

}
