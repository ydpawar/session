import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CricketService } from '../../../../_api/index';
import { NgxSpinnerService } from 'ngx-spinner';
import {ScriptLoaderService} from '../../../../_services';
import {ToastrService} from './../../../../_services/toastr.service';
import {InplayGameModule} from '../inplay-game/index.module';
declare const $: any;

@Component({
  selector: 'app-create-game-commentory',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class CreateGameCommentoryComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  sportId: string;
  eid: string;
  commentaryList: any;
  event_name: string;
  sportname: string;
  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute, private spinner: NgxSpinnerService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.sportId = this.route.snapshot.params.sid;
    if (this.sportId === '998917') {
      this.sportname = 'volleyball';
    }
    if (this.sportId === '7522') {
      this.sportname = 'basketball';
    }
    if (this.sportId === '3503') {
      this.sportname = 'dart';
    }
    if (this.sportId === '22') {
      this.sportname = 'table-tennis';
    }
    if (this.sportId === '13') {
      this.sportname = 'badminton';
    }
    if (this.sportId === '14') {
      this.sportname = 'kabaddi';
    }
    if (this.sportId === '16') {
      this.sportname = 'boxing';
    }
    this.eid = this.route.snapshot.params.eid;
    this.getDetails(this.eid);
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: [''],
      eid: ['']
    });
  }


  submitForm() {
    this.spinner.show();
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
     // this.frm.reset();
      this.service.commentaryupdate(data).subscribe((res) => this.onSuccess(res));
    }
  }

  getDetails(eid) {
    this.spinner.show();
    const data = { eid: this.eid };
    this.service.commentarylist(data).subscribe((res) => this.getDetailsSuccess(res));
  }
  getDetailsSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data != null) {
        this.commentaryList = res.data;
        this.event_name = res.name;
      }
    }
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }


  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events/games/' + this.sportname + '/' + this.sportId]);
     // this.router.navigate(['/events']);
    }
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }


  getFormData() {
    this.frm.patchValue({
      sid : 4, eid: this.eid
    });
    const data = this.frm.value;
    return data;
  }

  changeCommentary(item) {
    this.spinner.show();
    $('#' + item).addClass('cmt-active');
    const data = { eid: this.eid, title: item };
    this.service.commentaryupdate(data).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      $('.btnActive').removeClass('cmt-active');
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    } else {
      this.toastr.error(res.error.message);
    }
  }

  ngAfterViewInit() {
    const nthis = this;
    // tslint:disable-next-line:only-arrow-functions
    document.onkeyup = function(e) {
      if (e.which === 83) { // sft+s
        //  nthis.diffrance = 1;
        nthis.changeCommentary('Suspended');
      }

    };
  }

  get frmtitle() { return this.frm.get('title'); }


}
