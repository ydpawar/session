import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';

import { JackpotComponent } from './index.component';

import { JackpotListComponent } from './../jackpot/jackpot-list/index.component';
import { JackpotListModule } from './../jackpot/jackpot-list/index.module';


import { CreateJackpotComponent } from './../jackpot/create-jackpot/index.component';
import { CreateJackpotModule } from './../jackpot/create-jackpot/index.module';

import { CreateGameTypeComponent } from './../jackpot/create-game-type/index.component';
import { CreateGameTypeModule } from './../jackpot/create-game-type/index.module';
import {AuthGuard, AuthGuardChild} from './../../../_guards/auth.guard';
import {AuthIdentityService} from './../../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: JackpotComponent,
    children: [
      {
        path: '',
        component:  JackpotListComponent,
      },
      {
        path: 'create-jackpot/:sid',
        component: CreateJackpotComponent,
      },
      {
        path: 'jackpot/create-game-type',
        component: CreateGameTypeComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    JackpotListModule,
    CreateJackpotModule,
    CreateGameTypeModule

  ],
  exports: [RouterModule],
  declarations: [JackpotComponent],
})
export class JackpotModule {
}
