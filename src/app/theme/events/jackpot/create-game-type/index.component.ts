import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CricketService } from '../../../../_api/index';

@Component({
  selector: 'app-create-game-type',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateGameTypeComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  sid: string;
  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.sid = this.route.snapshot.params.sid;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: ['', Validators.required],
      type: ['', Validators.required],


    });
  }


  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
     // this.frm.reset();
      this.service.createnewgametype(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events/jackpot/game-type']);
    }
  }


  getFormData() {
    this.frm.patchValue({
      sid: this.sid
    });
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmTitle() { return this.frm.get('title'); }
  get frmType() { return this.frm.get('type'); }



}
