import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateJackpotComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: CreateJackpotComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateJackpotComponent
  ]
})
export class CreateJackpotModule {

}
