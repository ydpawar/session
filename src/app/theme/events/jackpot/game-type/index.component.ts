import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CricketService } from '../../../../_api/index';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services';
import {NgxSpinnerModule, NgxSpinnerService} from 'ngx-spinner';

interface GameTypeManage {
  id: string;
  sportId: string;
  name: string;
  type: string;
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}

class GameTypeManageObj implements GameTypeManage {
  id: string;
  sportId: string;
  // tslint:disable-next-line:variable-name
  name: string;
  // tslint:disable-next-line:variable-name
  type: string;
  title: string;
  // tslint:disable-next-line:variable-name
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-game-type',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class GameTypeComponent implements OnInit, AfterViewInit {

  page = { start: 1, end: 5 };
   user: any;
  aList: GameTypeManage[] = [];
  cItem: GameTypeManage;
  actionRsList: GameTypeManage[] = [];
  marketList: GameTypeManage[] = [];
  eventList: GameTypeManage[] = [];
  actionBtnList: GameTypeManage[] = [];
  isEmpty = false;
  isLoading = false;

  constructor( private service: CricketService, private loadScript: ScriptLoaderService, private spinner: NgxSpinnerService ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
    //  this.loadScript.load('script' , 'assets/js/datatables.init.js');
  }


  applyFilters() {
    this.spinner.show();
    const data = 10;
    this.service.jackpotGameType(data).subscribe((res) => this.onSuccess(res));

  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== null &&  res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        const data: GameTypeManage[] = [];

        if (items) {
          this.aList = res.data;


        } else {
          this.isEmpty = true;
        }

        // this.aList = data;
        // console.log(data);
        this.page.end = this.page.end + items.length - 1;

        setTimeout(() => { this.loadTreeScript(); }, 500);
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
        // this.config.totalItems = res.data.count;
      }

      this.spinner.hide();
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  changeStatus(item: GameTypeManage) {

    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';
    swal.fire({
      title: 'You want to ' + status + ' ' + item.name + '?',
      // text: "You won't be able to recover this entry!",
      // dangerMode: true,
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((willChange) => {
        if (willChange) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.changestatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');
  }

}
