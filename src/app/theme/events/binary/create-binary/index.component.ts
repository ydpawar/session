import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CricketService } from '../../../../_api/index';

@Component({
  selector: 'app-create-binary',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateBinaryComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  sid: string;
  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.sid = this.route.snapshot.params.sid;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      event_name: ['', Validators.required],
      event_league: ['Binary', Validators.required],
      sid: ['']
    });
  }


  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
      this.frm.reset();
      this.service.createnewevent(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/events/binary/binary-list']);
    }
  }


  getFormData() {
    this.frm.patchValue({
      sid: this.sid
    });
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmEventName() { return this.frm.get('event_name'); }
  get frmEventLeague() { return this.frm.get('event_league'); }


}
