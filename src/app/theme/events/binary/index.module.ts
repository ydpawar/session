import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';

import { BinaryComponent } from './index.component';

import { BinaryListComponent } from './../binary/binary-list/index.component';
import { BinaryListModule } from './../binary/binary-list/index.module';


import { CreateBinaryComponent } from './../binary/create-binary/index.component';
import { CreateBinaryModule } from './../binary/create-binary/index.module';

import {AuthGuard, AuthGuardChild} from './../../../_guards/auth.guard';
import {AuthIdentityService} from './../../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: BinaryComponent,
    children: [
      {
        path: '',
        component:  BinaryListComponent,
      },
      {
        path: 'create-binary/:sid',
        component: CreateBinaryComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    BinaryListModule,
    CreateBinaryModule
  ],
  exports: [RouterModule],
  declarations: [BinaryComponent],
})
export class BinaryModule {
}
