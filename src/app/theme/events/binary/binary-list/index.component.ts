import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CricketService } from '../../../../_api/index';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService, ToastrService} from '../../../../_services';
import { NgxSpinnerService } from 'ngx-spinner';
declare const $: any;

interface BinaryListManage {
  id: string;
  sportId: string;
  name: string;
  type: string;
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}

class BinaryListManageObj implements BinaryListManage {
  id: string;
  sportId: string;
  // tslint:disable-next-line:variable-name
  name: string;
  // tslint:disable-next-line:variable-name
  type: string;
  // tslint:disable-next-line:variable-name
  time: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-binary-list',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class BinaryListComponent implements OnInit, AfterViewInit {

  page = { start: 1, end: 5 };
  user: any;
  aList: BinaryListManage[] = [];
  cItem: BinaryListManage;
  actionRsList: BinaryListManage[] = [];
  marketList: BinaryListManage[] = [];
  eventList: BinaryListManage[] = [];
  actionBtnList: BinaryListManage[] = [];
  isEmpty = false;
  isLoading = false;
  pageLoad: any;

  constructor( private service: CricketService, private loadScript: ScriptLoaderService, private spinner: NgxSpinnerService, private toastr: ToastrService ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  //  this.loadScript.load('script' , 'assets/js/datatables.init.js');
  }


  applyFilters() {
    this.spinner.show();
    const data = 7;
    this.service.cricketeventListLoad(data).subscribe((res) => this.onSuccess(res));

  }

  onSuccess(res) {

    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data.eventList;
        const data: BinaryListManage[] = [];

        if (items !== 'null') {

          this.aList = res.data;
          this.actionBtnList = res.data.actionBtnList;
          this.actionRsList = res.data.actionRsList;
          this.marketList = res.data.marketList;
          this.eventList = res.data.eventList;
        } else {

          this.isEmpty = true;
          this.spinner.hide();
        }

       // this.aList = data;
        // console.log(data);
      //  this.page.end = this.page.end + items.length - 1;

        setTimeout(() => { this.loadTreeScript(); }, 500);
       // this.config.totalItems = res.data.count;
      }
      this.spinner.hide();

    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }




  changeStatus(item: BinaryListManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.changeeventstatus(this.cItem.id, this.cItem.status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  setTitleOnClick(title) {
    window.sessionStorage.setItem('title', title);
  }


  closedStatus(item: BinaryListManage) {
    this.cItem = item;

    swal.fire({
      title: 'Confirm Game Over?',
      text: item.name,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad('closed');
        }
      });
  }

  deleteStatus(item: BinaryListManage) {
    this.cItem = item;

    swal.fire({
      title: 'You want to delete ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initclosedStatusLoad('delete');
        }
      });
  }


  initclosedStatusLoad(type) {
    this.service.seteventstatus(this.cItem.id, type).subscribe((res) => this.onGameOverSuccess(res));
  }

  onGameOverSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }


  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
