import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {EventDetailsComponent} from './index.component';
import {JackpotDetailsComponent} from './jackpot.component';

const routes: Routes = [
  {
    path: ':sport/:eid',
    component: EventDetailsComponent,
  },
  {
    path: ':sport/:eid/:uid',
    component: EventDetailsComponent,
  },
  {
    path: 'jackpot-details/:eid/:type/:mid',
    component: JackpotDetailsComponent,
  },
  {
    path: 'jackpot-details/:eid/:type/:mid/:uid',
    component: JackpotDetailsComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
        EventDetailsComponent, JackpotDetailsComponent
    ]
})
export class EventDetailsModule {
}
