import {NgModule} from '@angular/core';
import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {LeftSidebarComponent} from './../public/left-sidebar/left-sidebar.component';
// import {RightSidebarComponent} from './../public/right-sidebar/right-sidebar.component';
import {HeaderComponent} from './../public/header/header.component';
// import {FooterComponent} from './../public/footer/footer.component';
import {AuthGuard, AuthGuardChild} from './../_guards/auth.guard';
import {AuthIdentityService} from './../_services/auth-identity.service';

const routes: Routes = [
  {
    path: '',
    component: ThemeComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuardChild],
    children: [
      {
        path: '',
        loadChildren: './home/index.module#HomeModule'
      },
      {
        path: 'dashbord',
        loadChildren: './home/index.module#HomeModule'
      },
      {
        path: 'sport',
        loadChildren: './sports/index.module#SportsModule'
      },
      {
        path: 'rules',
        loadChildren: './rules/index.module#RulesModule'
      },
      {
        path: 'dev-tools',
        loadChildren: './dev-tools/index.module#ToolsModule'
      },
      {
        path: 'users',
        loadChildren: './users/index.module#UsersModule'
      },
      {
        path: 'dashboard/event-details',
        loadChildren: './event-details/index.module#EventDetailsModule'
      },
      {
        path: 'events',
        loadChildren: './events/index.module#EventsModule'
      },
      {
        path: 'markets',
        loadChildren: './markets/index.module#MarketsModule'
      },
      {
        path: 'add-events',
        loadChildren: './add-events/index.module#AddEventsModule'
      },
      {
        path: 'setting',
        loadChildren: './setting/index.module#SettingModule'
      },
      {
        path: 'reports',
        loadChildren: './reports/index.module#ReportsModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule
  ],
  exports: [RouterModule],
  declarations: [ThemeComponent, HeaderComponent, LeftSidebarComponent],
  providers: [AuthIdentityService]
})
export class ThemeModule {
}
