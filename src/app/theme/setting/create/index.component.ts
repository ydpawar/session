import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {SettingService} from '../../../_api/index';
import {Location} from '@angular/common';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-create',
  templateUrl: './index.component.html',
  providers: [SettingService, NgxSpinnerService]
})
export class CreateComponent implements OnInit, OnDestroy {
  frm: FormGroup;
  aList: any;
  keyname: string;
  id: string;
  keyvalue: string;
  title = 'Setting - Create';
  breadcrumb: any = [{title: 'Setting', url: '/'}, {title: 'Create', url: '/'}];

  constructor(
    private formBuilder: FormBuilder,
    private service: SettingService,
    private router: Router,
    private route: ActivatedRoute,
    // tslint:disable-next-line:variable-name
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {

  }

  ngOnInit() {
    this.spinner.show();
    this.id = this.route.snapshot.params.id;
    this.applyFilters();
  }

  ngOnDestroy(): void {
    this.aList = [];
    this.keyname = '';
    this.keyvalue = '';
  }

  applyFilters() {
    this.service.getSettingData(this.id).subscribe((res) => this.onSuccessData(res));

  }

  onSuccessData(res) {
    if (res.status === 1) {
      this.aList = res.data;
      this.keyname = res.data.key_name;
      this.keyvalue = res.data.value;
    }
    this.createForm();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      key: [this.keyname, Validators.required],
      value: [this.keyvalue, Validators.required],
    });
    this.spinner.hide();
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }


  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['setting/manage']);
    }
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  onCancel() {
    this._location.back();
  }

  get frmKey() {
    return this.frm.get('key');
  }

  get frmValue() {
    return this.frm.get('value');
  }

}
