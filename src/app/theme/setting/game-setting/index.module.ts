import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GamesettingComponent} from './index.component';

const routes: Routes = [
  {
    path: 'game-setting/:id',
    component: GamesettingComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    GamesettingComponent
  ]
})
export class GamesettingModule {

}
