import {Component, OnInit, AfterViewInit} from '@angular/core';
import {SettingService} from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import {ActivatedRoute} from '@angular/router';

interface Setting {
  id: string;
  sportId: string;
  slug: string;
  mType: string;
  status: {
    val: string; text: string; cssClass: string;
  };
}

class SettingObj implements Setting {
  id: string;
  sportId: string;
  slug: string;
  mType: string;
  status: {
    val: string; text: string; cssClass: string;
  };
}


@Component({
  selector: 'app-game-setting',
  templateUrl: './index.component.html',
  providers: [SettingService]
})
export class GamesettingComponent implements OnInit, AfterViewInit {

  page = {start: 1, end: 5};
  sid: any;
  aList: Setting[] = [];
  cItem: Setting;
  isEmpty = false;
  isLoading = false;

  title = 'Game Setting';
  createUrl = '/setting/create-game-setting';
  breadcrumb: any = [{title: 'Setting', url: '/'}, {title: 'Setting', url: '/'}];

  constructor(private service: SettingService,
              private route: ActivatedRoute,
              private loadScript: ScriptLoaderService,
              private loadJs: ScriptLoaderService) {
  }

  ngOnInit() {
    this.sid = this.route.snapshot.params.id;
    this.applyFilters();
  }


  ngAfterViewInit() {
  }

  applyFilters() {

    this.service.gameSetting(this.sid).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1  && res.data !== undefined) {
        const items = res.data;
        const data: Setting[] = [];

        if (items.length) {
          this.aList = [];
          for (const item of items) {
            const cData: Setting = new SettingObj();

            cData.id = item.id;
            cData.sportId = item.sportId;
            cData.slug = item.slug;
            cData.mType = item.mType;
            cData.status = {
              val: item.status,
              text: item.status === 1 ? 'Active' : 'Inactive',
              cssClass: item.status === 1 ? 'text-success  border-success' : 'border-danger text-danger'
            };

            data.push(cData);
          }

          this.loadScript.load('script', 'assets/js/datatables.init.js');
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        // this.page.end = this.page.end + items.length - 1;
        this.loadJs.load('script', 'assets/js/datatables.init.js');

    }
  }

  changeStatus(item) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, do it!'
    }).then((result) => {
      if (result.value) {
        // tslint:disable-next-line:triple-equals
        const data = {id: item.id, sid: this.sid, status: (item.status.val == 1 ? 0 : 1)};
        this.service.gameSettingstatus(data).subscribe((res) => this.onChangeStatusSuccess(res));
      }
    });
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

}
