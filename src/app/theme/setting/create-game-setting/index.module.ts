import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {CreateGameSettingComponent} from './index.component';

const routes: Routes = [
  {
    path: 'create-game-setting/:id/:sid',
    component: CreateGameSettingComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateGameSettingComponent
  ]
})
export class CreateGameSettingModule {

}
