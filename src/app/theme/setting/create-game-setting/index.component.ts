import {Component, OnInit, AfterViewInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {SettingService} from '../../../_api/index';
import {Location} from '@angular/common';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-create-game-setting',
  templateUrl: './index.component.html',
  providers: [SettingService, NgxSpinnerService]
})
export class CreateGameSettingComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  aList: any;
  mname: string;
  id: string;
  sid: string;
  eid: string;
  mtype: string;
  slug: string;
  // tslint:disable-next-line:variable-name
  min_stake: any = 50;
  // tslint:disable-next-line:variable-name
  max_stake: any = 10000;
  // tslint:disable-next-line:variable-name
  max_profit_limit: any = 100000;
  // tslint:disable-next-line:variable-name
  isblock: any = 0;
  suspended: any = 1;
  // tslint:disable-next-line:variable-name
  betallowed: any = 1;
  title = 'Game Setting - Create';
  breadcrumb: any = [{title: 'Game Setting', url: '/'}, {title: 'Create Game Setting', url: '/'}];

  constructor(
    private formBuilder: FormBuilder,
    private service: SettingService,
    private router: Router,
    private route: ActivatedRoute,
    // tslint:disable-next-line:variable-name
    private _location: Location,
    private spinner: NgxSpinnerService
  ) {

  }

  ngOnInit() {
    this.spinner.show();
    this.id = this.route.snapshot.params.id;
    this.sid = this.route.snapshot.params.sid;
    this.applyFilters();
  }


  applyFilters() {
    this.service.getGameSettingData(this.id, this.sid).subscribe((res) => this.onSuccessData(res));

  }

  onSuccessData(res) {
    if (res.status === 1) {
      this.aList = res.data;
      this.mname = res.data.name;
      this.mtype = res.data.mType;
      this.slug = res.data.slug;
      this.min_stake = res.data.min_stake;
      this.max_stake = res.data.max_stake;
      this.max_profit_limit = res.data.max_profit_limit;
      this.isblock = res.data.is_block;
      this.suspended = res.data.suspend;
      this.betallowed = res.data.bet_allowed;

      if (res.data.eventId && res.data.eventId !== undefined) {
        this.eid = res.data.eventId;
      }
    }
    this.createForm();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      id: [this.id, ''],
      sid: [this.sid, ''],
      marketname: [this.mname, ''],
      eventId: [this.eid, ''],
      mname: [this.mname, Validators.required],
      mtype: [this.mtype, Validators.required],
      slug: [this.slug, Validators.required],
      min_stake: [this.min_stake, Validators.required],
      max_stake: [this.max_stake, Validators.required],
      max_profit_limit: [this.max_profit_limit, Validators.required],
      is_block: [this.isblock, Validators.required],
      suspend: [this.suspended, Validators.required],
      bet_allowed: [this.betallowed, Validators.required],
    });
    this.spinner.hide();
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.createNewGame(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['setting/game-setting/' + this.sid]);
    }
  }

  getFormData() {
    this.frm.patchValue({
      id: this.id, sid: this.sid
    });
    const data = this.frm.value;
    return data;
  }

  onCancel() {
    this._location.back();
  }

  ngAfterViewInit() {
  }

  get frmMarketname() {
    return this.frm.get('marketname');
  }

  get frmEventId() {
    return this.frm.get('eventId');
  }

  get frmMname() {
    return this.frm.get('mname');
  }

  get frmMtype() {
    return this.frm.get('mtype');
  }

  get frmSlug() {
    return this.frm.get('slug');
  }

  get frmMinstake() {
    return this.frm.get('min_stake');
  }

  get frmMaxstake() {
    return this.frm.get('max_stake');
  }

  get frmMaxprofitLimit() {
    return this.frm.get('max_profit_limit');
  }

  get frmIsblock() {
    return this.frm.get('is_block');
  }

  get frmSuspend() {
    return this.frm.get('suspend');
  }

  get frmBetAllowed() {
    return this.frm.get('bet_allowed');
  }

}
