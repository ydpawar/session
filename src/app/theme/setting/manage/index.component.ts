import {Component, OnInit, AfterViewInit} from '@angular/core';
import {SettingService} from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';

declare const $: any;

interface Setting {
  id: string;
  key: string;
  value: string;
  status: {
    val: string; text: string; cssClass: string;
  };
}

class SettingObj implements Setting {
  id: string;
  key: string;
  value: string;
  status: {
    val: string; text: string; cssClass: string;
  };
}


@Component({
  selector: 'app-manage',
  templateUrl: './index.component.html',
  providers: [SettingService]
})
export class ManageComponent implements OnInit, AfterViewInit {

  page = {start: 1, end: 5};
  aList: Setting[] = [];
  cItem: Setting;
  isEmpty = false;
  isLoading = false;
  pageLoad: string;
  title = 'Setting';
  createUrl = '/setting/create';
  breadcrumb: any = [{title: 'Setting', url: '/'}, {title: 'Manage', url: '/'}];

  constructor(private service: SettingService, private loadScript: ScriptLoaderService, private loadJs: ScriptLoaderService) {
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    this.service.manage().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }
    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      const items = res.data;
      const data: Setting[] = [];

      if (items.length) {
        this.aList = [];
        for (const item of items) {
          const cData: Setting = new SettingObj();

          cData.id = item.id;
          cData.key = item.key_name;
          cData.value = item.value;
          cData.status = {
            val: item.status,
            text: item.status === 1 ? 'Active' : 'Inactive',
            cssClass: item.status === 1 ? 'text-success  border-success' : 'border-danger text-danger'
          };

          data.push(cData);
        }
        setTimeout(() => {
          this.loadTreeScript();
        }, 500);
        // this.loadScript.load('script', 'assets/js/datatables.init.js');
      } else {
        this.isEmpty = true;
      }

      this.aList = data;
      // this.page.end = this.page.end + items.length - 1;
      this.loadJs.load('script', 'assets/js/datatables.init.js');
    }
  }

  loadTreeScript() {
    this.loadScript.load('script', 'assets/js/datatables.init.js');
    setTimeout(() => {
      // this.spinner.hide();
    }, 500);
  }

  changeStatus(item) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, do it!'
    }).then((result) => {
      if (result.value) {
        // tslint:disable-next-line:triple-equals
        const data = {id: item.id, status: (item.status.val == 1 ? 0 : 1)};
        this.service.status(data).subscribe((res) => this.onChangeStatusSuccess(res));
      }
    });
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

}
