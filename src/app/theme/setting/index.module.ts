import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { SettingComponent } from './index.component';

import { ManageComponent } from './manage/index.component';
import { ManageModule } from './manage/index.module';
import { CreateComponent } from './create/index.component';
import { CreateModule } from './create/index.module';

import { GamesettingComponent } from './game-setting/index.component';
import { GamesettingModule } from './game-setting/index.module';
import { CreateGameSettingComponent } from './create-game-setting/index.component';
import { CreateGameSettingModule } from './create-game-setting/index.module';

const routes: Routes = [
  {
    path: '',
    component: SettingComponent,
    children: [
      {
        path: '',
        component: ManageComponent,
      },
      {
        path: 'manage',
        component: ManageComponent,
      },
      {
        path: 'create/:id',
        component: CreateComponent,
      },
      {
        path: 'game-setting/:id',
        component: GamesettingComponent,
      },
      {
        path: 'create-game-setting/:id/:sid',
        component: CreateGameSettingComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    ManageModule,
    CreateModule,
    GamesettingModule,
    CreateGameSettingModule
  ],
  exports: [RouterModule],
  declarations: [SettingComponent],
})
export class SettingModule {
}
