import {Component, OnInit, AfterViewInit} from '@angular/core';
import {CricketService} from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';

declare const $: any;

interface RuleManage {
  id: string;
  categoryname: string;
  sub_category: string;
  rule: string;

  status: {
    text: string;
    cssClass: string;
  };
}

class RuleManageObj implements RuleManage {
  id: string;
  categoryname: string;
  // tslint:disable-next-line:variable-name
  sub_category: string;
  // tslint:disable-next-line:variable-name
  rule: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class RuleComponent implements OnInit, AfterViewInit {
  page = {start: 1, end: 5};
  aList: RuleManage[] = [];
  cItem: RuleManage;
  isEmpty = false;
  isLoading = false;

  constructor(private service: CricketService, private loadScript: ScriptLoaderService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }


  applyFilters() {
    this.spinner.show();
    const data = '';
    this.service.sportruleListLoad(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.isLoading = false;
      const items = res.data;
      const data: RuleManage[] = [];

      if (items.length) {
        this.aList = [];
        for (const item of items) {
          const cData: RuleManage = new RuleManageObj();

          cData.id = item.id;
          cData.sub_category = item.sub_category;
          cData.categoryname = item.category_name;
          cData.rule = item.rule;
          cData.status = {
            text: item.status === 1 ? 'Active' : 'Inactive',
            cssClass: item.status === 1 ? 'success' : 'dark'
          };

          data.push(cData);
        }
      } else {
        this.isEmpty = true;
      }

      this.aList = data;
      // console.log(data);
      this.page.end = this.page.end + items.length - 1;
      setTimeout(() => {
        this.loadTreeScript();
      }, 500);
    } else {
      this.spinner.hide();
    }
  }


  changeStatus(item: RuleManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.sub_category + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }


  initStatus() {
    this.service.changerulestatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    // this.loadScript.load('script', 'assets/js/datatables.init.js');
    this.initDataTables();
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

  initDataTables() {
    $('.scroll-horizontal-datatable').DataTable({
      destroy: true,
      scrollX: true,
      retrieve: true,
      stateSave: true,
      // "processing": true,
      // "bServerSide": true,
      language: {
        paginate: {
          previous: '<i class=\'mdi mdi-chevron-left\'>',
          next: '<i class=\'mdi mdi-chevron-right\'>'
        }
      },
      drawCallback: function drawCallback() {
        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
      },
    }); // Complex headers with column visibility Datatable
  }

}
