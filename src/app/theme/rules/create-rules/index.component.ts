import {Component, OnInit, AfterViewInit} from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {CricketService} from '../../../_api/index';

@Component({
  selector: 'app-create-rules',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateRulesComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  sportArr: any = null;
  sid: string;
  // categoryname: string;
  // subcategory: string;
  // rule: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getsportList();
    this.sid = this.route.snapshot.params.id;
    if (this.sid !== '0') {
      this.getRules(this.sid);
    }
  }

  ngAfterViewInit() {

  }


  getRules(id) {
    this.service.getRules(id).subscribe((res) => {
      if (res.status === 1 && res.data !== undefined) {
        this.frm.patchValue({
          id: this.sid,
          category_name: res.data.category_name,
          sub_category: res.data.sub_category,
          rule: res.data.rule
        });
      }
    });
  }

  getsportList() {
    this.service.getSportList().subscribe((res) => {
      if (res.status === 1) {
        this.sportArr = res.data;
      }
    });
  }

  createForm() {
    this.frm = this.formBuilder.group({
      id: [''],
      category_name: ['', Validators.required],
      sub_category: ['', Validators.required],
      rule: ['', Validators.required]
    });
  }


  submitForm() {
    const data = this.frm.value;
    // console.log(data);
    if (this.frm.valid) {
      this.frm.reset();
      this.service.createnewrule(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/rules']);
    }
  }


  get frmSportName() {
    return this.frm.get('category_name');
  }

  get frmCategory() {
    return this.frm.get('sub_category');
  }

  get frmrule() {
    return this.frm.get('rule');
  }

}
