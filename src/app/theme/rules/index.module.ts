import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import { RulesComponent } from './index.component';

import { RuleComponent } from './rule/rule.component';
import { RuleModule } from './rule/index.module';


import { CreateRulesComponent } from './create-rules/index.component';
import { CreateRulesModule } from './create-rules/index.module';

const routes: Routes = [
  {
    path: '',
    component: RulesComponent,
    children: [
      {
        path: '',
        component: RuleComponent,
      },
      {
        path: 'create-rules/:id',
        component: CreateRulesComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    RuleModule,
    CreateRulesModule
  ],
  exports: [RouterModule],
  declarations: [RulesComponent],
})

export class RulesModule {
}
