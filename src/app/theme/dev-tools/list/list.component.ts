import { Component, OnInit, AfterViewInit } from '@angular/core';
import {CricketService, MarketService} from '../../../_api/index';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import { NgxSpinnerService } from 'ngx-spinner';
declare const $: any;

interface ListManage {
  id: string;
  table_name: string;
  table_type: string;
  rule: string;

  status: {
    text: string;
    cssClass: string;
  };
}

class ListManageObj implements ListManage {
  id: string;
  // tslint:disable-next-line:variable-name
  table_name: string;
  // tslint:disable-next-line:variable-name
  table_type: string;
  // tslint:disable-next-line:variable-name
  rule: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-dev-tools',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class ListComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  frmUpload: FormGroup;
  page = { start: 1, end: 5 };

  aList: ListManage[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;

  constructor( private service: CricketService, private loadScript: ScriptLoaderService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService ) { }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  //  this.loadScript.load('script' , 'assets/js/datatables.init.js');
  }


  applyFilters() {
    this.spinner.show();
    const data = '';
    this.service.gettablelist(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        const data: ListManage[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: ListManage = new ListManageObj();

            cData.id = item.id;
            cData.table_name = item.table_name;
            cData.table_type = item.table_type;
            cData.status = {
              text: item.status === 1 ? 'Pending' : 'Completed',
              cssClass: item.status === 2 ? 'success' : 'dark'
            }

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
       // console.log(data);
        this.page.end = this.page.end + items.length - 1;
        setTimeout(() => { this.loadTreeScript(); }, 500);
      }
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }



  createForm() {
    this.frm = this.formBuilder.group({
      max_profit_limit: [''],
      bet_delay: [''],
      id: ['']
    });
  }

  changeStatus(item: ListManage) {
    this.cItem = item;
    const status = item.status.text === 'Pending' ? 'Update table for' : 'Update table for';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.table_name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#dd3333',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }


  initStatus() {
    this.service.updateAccountTable(this.cItem.id).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
