import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CricketService } from '../../../_api/index';

@Component({
  selector: 'app-create-table',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateTableComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  sportArr: any = null;
  sid: string;
  categoryname: string;
  subcategory: string;
  rule: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
  //  this.getsportList();
    this.sid = this.route.snapshot.params.id;
   /* if (this.sid !== '0') {
      this.getRules(this.sid);
    }*/
  }

 /* getRules(id) {
    this.service.getRules(id).subscribe((res) => {
      if (res.status === 1) {
        if (res.data !== undefined && res.data !== undefined) {
          this.categoryname = res.data.category_name;
          this.subcategory = res.data.sub_category;
          this.rule = res.data.rule;
          this.frm.patchValue({
            id: this.sid,
            category_name: this.categoryname,
            sub_category: this.subcategory,
            rule : this.rule
          });
        }
      }
    });
  }*/

 /* getsportList() {
    this.service.getSportList().subscribe((res) => {
      if (res.status === 1) {
        this.sportArr = res.data;
      }
    });
  }*/

  createForm() {
    this.frm = this.formBuilder.group({
      id: [''],
      month: ['', Validators.required],
      year: ['', Validators.required]
    });
  }


  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
      this.frm.reset();
      this.service.createnewtable(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/dev-tools']);
    }
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmMonth() { return this.frm.get('month'); }
  get frmYear() { return this.frm.get('year'); }

}
