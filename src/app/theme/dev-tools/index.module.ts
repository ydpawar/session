import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { ToolsComponent } from './index.component';

import { ListComponent } from './list/list.component';
import { ListModule } from './list/index.module';


import { CreateTableComponent } from './create-table/index.component';
import { CreateTableModule } from './create-table/index.module';

import {AuthGuard, AuthGuardChild} from './../../_guards/auth.guard';
import {AuthIdentityService} from './../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: ToolsComponent,
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: 'create-table/:id',
        component: CreateTableComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    ListModule,
    CreateTableModule
  ],
  exports: [RouterModule],
  declarations: [ToolsComponent],
})

export class ToolsModule {
}
