import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UsersComponent} from './index.component';

import {UserListComponent} from './admin-list/index.component';
import {UserListModule} from './admin-list/index.module';

import {CreateUserComponent} from './create-user/index.component';
import {CreateUserModule} from './create-user/index.module';

import {SessionUserComponent} from './session-user/index.component';
import {SessionUserModule} from './session-user/index.module';

import {CreateSessionUserComponent} from './create-session-user/index.component';
import {CreateSessionUserModule} from './create-session-user/index.module';

import {UpdateSessionUserComponent} from './update-session-user/index.component';
import {UpdateSessionUserModule} from './update-session-user/index.module';

import {ResetPasswordComponent} from './action/reset-password/index.component';
import {ResetPasswordModule} from './action/reset-password/index.module';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: '',
        component: UserListComponent,
      },
      {
        path: 'create-user',
        component: CreateUserComponent,
      },
      {
        path: 'session-user',
        component: SessionUserComponent,
      },
      {
        path: 'create-session-user',
        component: CreateSessionUserComponent,
      },
      {
        path: 'update-session-user/:uid',
        component: UpdateSessionUserComponent,
      },
      {
        path: 'reset-password/:uid',
        component: ResetPasswordComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    UserListModule,
    SessionUserModule,
    CreateUserModule,
    CreateSessionUserModule, ResetPasswordModule, UpdateSessionUserModule
  ],
  exports: [RouterModule],
  declarations: [UsersComponent],
})
export class UsersModule {
}
