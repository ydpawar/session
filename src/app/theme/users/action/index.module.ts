import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ActionComponent} from './index.component';

import {ResetPasswordComponent} from './reset-password/index.component';
import {ResetPasswordModule} from './reset-password/index.module';


const routes: Routes = [
  {
    path: '',
    component: ActionComponent,
    children: [
      {
        path: 'reset-password/:uid',
        component: ResetPasswordComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    ResetPasswordModule
  ], exports: [
    RouterModule
  ], declarations: [ActionComponent]
})
export class ActionModule {

}
