import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {UsersService} from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [UsersService]
})
export class ResetPasswordComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Users - Reset Password';
  breadcrumb: any = [{title: 'Users', url: '/'}, {title: 'Reset Password', url: '/'}];
  passType = 'password';
  uid: string;
  frm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: UsersService,
    private router: Router,
    private route: ActivatedRoute,
    // tslint:disable-next-line:variable-name
    private _location: Location
  ) {
    this.uid = this.route.snapshot.params.uid;
    // tslint:disable-next-line:triple-equals
    if (window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined) {
      this.title = 'Reset Password - ' + window.localStorage.getItem('title');
    }
  }

  ngOnInit() {
    this.createForm();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem('title');
  }

  createForm() {
    this.frm = this.formBuilder.group({
      uid: [this.uid],
      password: ['', Validators.required],
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.resetPassword(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
    }
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmPassword() {
    return this.frm.get('password');
  }

}

