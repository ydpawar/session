import {Component, OnInit, AfterViewInit} from '@angular/core';
import {UsersService} from '../../../_api/index';
import {AuthIdentityService, ScriptLoaderService} from '../../../_services/index';
import swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';

declare const $: any;

interface UserListManage {
  id: string;
  name: string;
  username: string;
  user_rights: any;

  status: {
    text: string;
    cssClass: string;
  };
}

class UserListManageObj implements UserListManage {
  id: string;
  name: string;
  username: string;
  user_rights: any;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [UsersService, NgxSpinnerService]
})
export class UserListComponent implements OnInit, AfterViewInit {

  page = {start: 1, end: 5};
  user: any;
  aList: UserListManage[] = [];
  cItem: UserListManage;
  isEmpty = false;
  isLoading = false;
  pageLoad: any;

  constructor(private service: UsersService, private loadScript: ScriptLoaderService, private spinner: NgxSpinnerService) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    this.spinner.show();
    const data = '';
    this.service.userListLoad(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    this.aList = [];
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1 && res.data !== null && res.data !== undefined) {
      // this.isLoading = false;
      const items = res.data;
      const data: UserListManage[] = [];

      if (items.length) {

        for (const item of items) {
          const cData: UserListManage = new UserListManageObj();

          cData.id = item.id;
          cData.name = item.name;
          cData.username = item.username;
          cData.user_rights = item.user_rights;
          cData.status = {
            text: item.status === 1 ? 'Active' : 'Inactive',
            cssClass: item.status === 1 ? 'success' : 'dark'
          };

          data.push(cData);
        }
      } else {
        this.isEmpty = true;
      }

      this.aList = data;
      this.loadScript.load('script', 'assets/js/datatables.init.js');
      setTimeout(() => {
        this.spinner.hide();
      }, 500);

    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }

  changeStatus(item: UserListManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus('status');
        }
      });
  }

  deleteStatus(item: UserListManage) {
    this.cItem = item;

    swal.fire({
      title: 'You want to delete ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus('delete');
        }
      });
  }


  initStatus(type) {
    if (type === 'delete') {
      this.service.changeuserstatus(this.cItem.id, type).subscribe((res) => this.onChangeStatusSuccess(res));
    } else {
      this.service.changeuserstatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
    }
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script', 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }



}

