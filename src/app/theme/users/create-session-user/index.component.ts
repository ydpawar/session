import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {UsersService} from '../../../_api/index';
import {ToastrService} from '../../../_services/index';


// tslint:disable-next-line:no-empty-interface
interface CreateSessionUserManage {
  id: string;
  name: string;
  username: string;
  status: {
    text: string;
    cssClass: string;
  };
}

class CreateSessionUserManageObj implements CreateSessionUserManage {
  id: string;
  name: string;
  username: string;
  status: {
    text: string;
    cssClass: string;
  };
}

@Component({
  selector: 'app-create-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [UsersService]
})


export class CreateSessionUserComponent implements OnInit {
  frm: FormGroup;
  aList: CreateSessionUserManage[] = [];
  marketList: CreateSessionUserManage[] = [];
  sportList: CreateSessionUserManage[] = [];
  actionList: CreateSessionUserManage[] = [];
  isEmpty = false;
  tempArr: any = [];
  tempMarketArr: any = [];
  tempActionArr: any = [];
  tempsportVal: string;
  tempmarketVal: string;
  tempactionVal: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: UsersService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.applyFilters();
  }

  applyFilters() {
    const data = '';
    this.service.sportListLoad(data).subscribe((res) => this.onSuccessResult(res));
  }

  onSuccessResult(res) {

    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.isLoading = false;
      const items = res.data;
      this.actionList = res.data.actionList;
      this.sportList = res.data.sportList;
      this.marketList = res.data.marketList;
      // this.aList = items;

    } else {
      this.isEmpty = true;
    }
  }


  createForm() {
    this.frm = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      assignsport: ['', Validators.required],
      assignmarket: ['', Validators.required],
      assignresult: [''],
      sportVal: [''],
      marketVal: [''],
      actionVal: [''],
    });
  }


  onChangeSport(event, cat: any) { // Use appropriate model type instead of any
    this.tempArr.push(cat.id);
    if (this.tempsportVal === undefined || this.tempsportVal === '') {
      this.tempsportVal = cat.id;
    } else {
      this.tempsportVal += ',' + cat.id;
    }
  }


  onChangeMarket(event, cat: any) { // Use appropriate model type instead of any
    this.tempMarketArr.push(cat.id);
    if (this.tempmarketVal === undefined || this.tempmarketVal === '') {
      this.tempmarketVal = cat.id;
    } else {
      this.tempmarketVal += ',' + cat.id;
    }
  }

  onChangeAction(event, cat: any) { // Use appropriate model type instead of any
    this.tempActionArr.push(cat.id);
    // console.log(this.tempActionArr);
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.createSessionUser(data).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccess(data);
        },
        error => {
          this.toastr.error('failed , please try again', 'Something want wrong..!');
        });
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/users/session-user']);
    }
  }


  getFormData() {
    this.frm.patchValue({
      sportVal: this.tempArr, marketVal: this.tempMarketArr, actionVal: this.tempActionArr, roleId: 7 // , marketVal: this.tempMarketArr
    });
    const data = this.frm.value;
    return data;
  }

  get frmName() {
    return this.frm.get('name');
  }

  get frmUserName() {
    return this.frm.get('username');
  }

  get frmPassword() {
    return this.frm.get('password');
  }

}
