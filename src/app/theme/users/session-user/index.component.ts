import { Component, OnInit, AfterViewInit } from '@angular/core';
import { UsersService } from '../../../_api/index';
import {AuthIdentityService, ScriptLoaderService} from '../../../_services/index';
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
 declare const $: any;

interface SessionUserManage {
  id: string;
  name: string;
  username: string;
  user_rights: any;

  status: {
    text: string;
    cssClass: string;
  };
}

class SessionUserManageObj implements SessionUserManage {
  id: string;
  name: string;
  username: string;
  // tslint:disable-next-line:variable-name
  user_rights: any;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [UsersService, NgxSpinnerService]
})
export class SessionUserComponent implements OnInit, AfterViewInit {

  page = { start: 1, end: 5 };
  user: any;
  aList: SessionUserManage[] = [];
  cItem: SessionUserManage;
  isEmpty = false;
  isLoading = false;
  pageLoad: any;

  constructor( private service: UsersService, private loadScript: ScriptLoaderService, private spinner: NgxSpinnerService) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
   // alert('ddd');
   // this.loadScript.load('script' , 'assets/js/foo-tables.init.js');
   // this.loadScript.replacejscssfile('foo-tables.init.js', 'foo-tables.init.js', 'js');

  }

  applyFilters() {
    this.spinner.show();
    const data = '';
    this.service.sessionUserListLoad(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    // declare const $: any;
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== null && res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        const data: SessionUserManage[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: SessionUserManage = new SessionUserManageObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.username = item.username;
            cData.user_rights = item.user_rights;
            cData.status = {
              text: item.status === 1 ? 'Active' : 'Inactive',
              cssClass: item.status === 1 ? 'success' : 'dark'
            }

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.loadScript.load('script', 'assets/js/datatables.init.js');
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
       // console.log(data);
       // this.page.end = this.page.end + items.length - 1;
      //  setTimeout(() => { this.loadTreeScript(); }, 500);
      } else {
        this.spinner.hide();
      }
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }



  changeStatus(item: SessionUserManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus('status');
        }
      });
  }

  deleteStatus(item: SessionUserManage) {
    this.cItem = item;

    swal.fire({
      title: 'You want to delete ' + item.name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus('delete');
        }
      });
  }

  initStatus(type) {
    if (type === 'delete') {
      this.service.changeuserstatus(this.cItem.id, type).subscribe((res) => this.onChangeStatusSuccess(res));
    } else {
      this.service.changeuserstatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
    }
  }


  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script' , 'assets/js/datatables.init.js');

  }

}

