import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {UsersService} from '../../../_api/index';
import {AuthIdentityService, ToastrService} from '../../../_services/index';

declare const $: any;
// tslint:disable-next-line:no-empty-interface
interface UpdateSessionUserManage {
  id: string;
  name: string;
  username: string;
  userrole: any;
  status: {
    text: string;
    cssClass: string;
  };
}

class UpdateSessionUserManageObj implements UpdateSessionUserManage {
  id: string;
  name: string;
  username: string;
  userrole: any;
  status: {
    text: string;
    cssClass: string;
  };
}

@Component({
  selector: 'app-create-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [UsersService]
})


export class UpdateSessionUserComponent implements OnInit {
  frm: FormGroup;
  aList: UpdateSessionUserManage[] = [];
  marketList: UpdateSessionUserManage[] = [];
  sportList: UpdateSessionUserManage[] = [];
  actionList: UpdateSessionUserManage[] = [];
  userroleList: UpdateSessionUserManage[] = [];
  userList: UpdateSessionUserManage[] = [];
  isEmpty = false;
  tempArr: any = [];
  tempMarketArr: any = [];
  tempActionArr: any = [];
  uid: string;
  name: string;
  username: string;
  userrole: any;
  tempsportVal: string;
  tempmarketVal: string;
  tempactionVal: string;
  redirectUrl: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: UsersService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.uid = this.route.snapshot.params.uid;

  }

  ngOnInit() {
    this.applyFilters();
    this.createForm();
  }

  applyFilters() {
    const data = {id: this.uid};
    this.service.sportListLoad(data).subscribe((res) => this.onSuccessResult(res));

  }

  onSuccessResult(res) {

    if (res.status !== undefined && res.status === 1) {
      // this.isLoading = false;
      if (res.data !== undefined && res.data !== undefined) {
        const items = res.data;
        this.actionList = res.data.actionList;
        this.sportList = res.data.sportList;
        this.marketList = res.data.marketList;
        this.userList = res.data.userList;
        this.userroleList = res.data.userroleList;
       // this.aList = items;
        this.username = res.data.userList.username;
        this.name = res.data.userList.name;
        this.userrole = res.data.userList.role;
        this.redirectUrl = '/users/session-user';
        // @ts-ignore
        if (this.userrole === 5) {
          this.redirectUrl = '/users';
        }

        let item;
        let items2;
        item = res.data.userroleList.market_role;
        if (item !== 0) {
          for (items2 of item) {
            this.tempMarketArr.push(items2);
          }
        }

        let sportrole;
        let sportroles;
        sportrole = res.data.userroleList.sport_role;
        if (sportrole !== 0) {
          for (sportroles of sportrole) {
            this.tempArr.push(sportroles);
          }
        }

        let actionrole; let actionroles;
        actionrole = res.data.userroleList.action_role;
        if (actionrole !== 0) {
          for (actionroles of actionrole) {
            this.tempActionArr.push(actionroles);
          }
        }

       // console.log( this.tempArr);
        this.frm.patchValue({
          name: this.name,
          username: this.username,
          sportVal: [''],
          marketVal: [''],
          actionVal: [''],
        });
      } else {
        this.isEmpty = true;
      }


    }
  }


  createForm() {

    this.frm = this.formBuilder.group({
      name: ['', Validators.required],
      username: [this.username, Validators.required],
      assignsport: [''],
      assignmarket: [''],
      assignresult: [''],
      sportVal: [''],
      marketVal: [''],
      actionVal: [''],
      uid: ['']
    });
  }


  onChangeSport(event, cat: any) { // Use appropriate model type instead of any
   //  this.tempArr.push(cat.id);
     if (event.target.checked) {
      this.tempArr.push(cat.id);
    } else {
      let item;
      let items2;
      item = this.tempArr;
      this.tempArr = [];
      for (items2 of item) {
        if (items2 !== cat.id) {
          this.tempArr.push(items2);
        }
      }
    }


     if (this.tempsportVal === undefined || this.tempsportVal === '') {
       this.tempsportVal = cat.id;
     } else {
       this.tempsportVal += ',' + cat.id;
     }

    // console.log(this.tempArr);
  }


  onChangeMarket(event, cat: any) { // Use appropriate model type instead of any
    if (event.target.checked) {
      this.tempMarketArr.push(cat.id);
    } else {
      let item;
      let items2;
      item = this.tempMarketArr;
      this.tempMarketArr = [];
      for (items2 of item) {
        if (items2 !== cat.id) {
          this.tempMarketArr.push(items2);
        }
      }
    }

    if (this.tempmarketVal === undefined || this.tempmarketVal === '') {
      this.tempmarketVal = cat.id;
    } else {
      this.tempmarketVal += ',' + cat.id;
    }
     // console.log(event);
    // console.log(this.tempMarketArr);
  }

  onChangeAction(event, cat: any) { // Use appropriate model type instead of any
   // this.tempActionArr.push(cat.id);

    if (event.target.checked) {
      this.tempActionArr.push(cat.id);
    } else {
      let item;
      let items2;
      item = this.tempActionArr;
      this.tempActionArr = [];
      for (items2 of item) {
        if (items2 !== cat.id) {
          this.tempActionArr.push(items2);
        }
      }
    }
   // console.log(this.tempActionArr);
  }


  submitForm() {
    const data = this.getFormData();
  //  console.log(data);
   // if (this.frm.valid) {
      // this.frm.reset();
    this.service.updateSessionUser(data).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccess(data);
        },
        error => {
          this.toastr.error('failed , please try again', 'Something want wrong..!');
        });
      // this.service.createSessionUser(data).subscribe((res) => this.onSuccess(res));
   // }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate([this.redirectUrl]);
    }
  }


  getFormData() {
    this.frm.patchValue({
       uid: this.uid, sportVal: this.tempArr, marketVal: this.tempMarketArr , actionVal: this.tempActionArr // , marketVal: this.tempMarketArr
    });
    const data = this.frm.value;
    return data;
  }

  get frmName() {
    return this.frm.get('name');
  }

  get frmUserName() {
    return this.frm.get('username');
  }


}
