import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateSessionUserComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: UpdateSessionUserComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateSessionUserComponent
  ]
})
export class UpdateSessionUserModule {

}
