import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { MarketsComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: MarketsComponent,
    children: [
      {
        path: 'manual-fancy',
        loadChildren: './manual-fancy/index.module#ManualFancyModule'
      },
      {
        path: 'fancy',
        loadChildren: './fancy/index.module#FancyModule'
      },
      {
        path: 'binary',
        loadChildren: './binary/index.module#BinaryModule'
      },
      {
        path: 'bookmaker',
        loadChildren: './bookmaker/index.module#BookmakerModule'
      },
      {
        path: 'virtual-session',
        loadChildren: './virtual-session/index.module#VirtualSessionModule'
      },
      {
        path: 'casino',
        loadChildren: './casino/index.module#CasinoModule'
      },
      {
        path: 'jackpot',
        loadChildren: './jackpot/index.module#JackpotModule'
      },

      {
        path: 'khado',
        loadChildren: './khado/index.module#KhadoModule'
      },
      {
        path: 'single-fancy',
        loadChildren: './single-fancy/index.module#SingleFancyModule'
      },
      {
        path: 'odd-even',
        loadChildren: './odd-even/index.module#OddEvenModule'
      },
      {
        path: 'ballbyball',
        loadChildren: './ballbyball/index.module#BallbyballModule'
      },
      {
        path: 'limits',
        loadChildren: './limits/index.module#LimitMarketModule'
      },
      {
        path: 'bet-fair',
        loadChildren: './bet-fair/index.module#BetFairModule'
      },
      {
        path: 'winner',
        loadChildren: './winner/index.module#WinnerModule'
      },
      {
        path: 'meter',
        loadChildren: './meter/index.module#MeterModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [MarketsComponent],
})
export class MarketsModule {
}
