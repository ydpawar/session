import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LimitMarketComponent} from './index.component';
import {SetLimitsMarketComponent} from './set-limits/index.component';
import {SetLimitsMarketModule} from './set-limits/index.module';

const routes: Routes = [
  {
    path: '',
    component: LimitMarketComponent,
    children: [
      {
        path: 'set-limits/:eid/:sid',
        component: SetLimitsMarketComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, SetLimitsMarketModule
  ], exports: [
    RouterModule
  ], declarations: [LimitMarketComponent]
})
export class LimitMarketModule {

}
