import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SetLimitsMarketComponent} from './index.component';

const routes: Routes = [
  {
    path: '',
    component: SetLimitsMarketComponent
  },
  {
    path: 'set-limits/:eid/:mid/:sid/:type',
    component: SetLimitsMarketComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    SetLimitsMarketComponent
  ]
})
export class SetLimitsMarketModule {

}
