import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';

@Component({
  selector: 'app-set-limits',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService]
})
export class SetLimitsMarketComponent implements OnInit {
  frm: FormGroup;
  eid: string;
  mid: any;
  CList: any;
  sportId: string;
  mtype: string;
  setUrl: any;
  // tslint:disable-next-line:variable-name
  // max_odd_limit: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.mid = this.route.snapshot.params.mid;
    this.sportId = this.route.snapshot.params.sid;
    this.mtype = this.route.snapshot.params.type;

    if (this.mtype === 'fancy2') {
      this.setUrl = '/markets/manual-fancy/';
    } else if (this.mtype === 'fancy') {
      this.setUrl = '/markets/fancy/';
    } else if (this.mtype === 'fancy3') {
      this.setUrl = '/markets/single-fancy/';
    } else if (this.mtype === 'binary') {
      this.setUrl = '/markets/binary/';
    } else if (this.mtype === 'oddeven') {
      this.setUrl = '/markets/odd-even/';
    } else if (this.mtype === 'ballbyball') {
      this.setUrl = '/markets/ballbyball/';
    } else if (this.mtype === 'khado') {
      this.setUrl = '/markets/khado/';
    } else if (this.mtype === 'bookmaker') {
      this.setUrl = '/markets/bookmaker/';
    } else if (this.mtype === 'virtual_cricket') {
      this.setUrl = '/markets/virtual-session/';
    } else if (this.mtype === 'casino') {
      this.setUrl = '/markets/casino/';
    } else if (this.mtype === 'jackpot') {
      this.setUrl = '/markets/jackpot/';
    } else if (this.mtype === 'match_odd') {
      this.setUrl = '/markets/bet-fair/';
    } else if (this.mtype === 'winner') {
      this.setUrl = '/markets/winner/';
    } else if (this.mtype === 'completed_match') {
      this.setUrl = '/markets/bet-fair/';
    } else if (this.mtype === 'tied_match') {
      this.setUrl = '/markets/bet-fair/';
    } else if (this.mtype === 'goals') {
      this.setUrl = '/markets/bet-fair/';
    } else if (this.mtype === 'set_market') {
      this.setUrl = '/markets/bet-fair/';
    } else if (this.mtype === 'meter') {
      this.setUrl = '/markets/meter/';
    }

    this.getSetting();
  }

  getSetting() {
    const data = {mid: this.mid, sid: this.sportId};
    this.service.getMarketSetting(data).subscribe((res) => this.onRunnerSuccess(res));
  }

  onRunnerSuccess(res) {
    if (res) {
      this.CList = res.data;
      this.frm.patchValue({

        max_stack: res.data.max_stack,
        min_stack: res.data.min_stack,
        max_profit_limit: res.data.max_profit_limit,
        max_odd_limit: res.data.max_odd_limit,
        bet_delay: res.data.bet_delay
      });

      if (this.sportId === '10') {
        this.frm.patchValue({
          commentary: res.jackinfo.commentary,
          rules: res.jackinfo.rules
        });
      }
    }
  }


  createForm() {
    this.frm = this.formBuilder.group({
      max_stack: [0, Validators.required],
      min_stack: [0, Validators.required],
      max_profit_limit: [0, Validators.required],
      max_odd_limit: [0, Validators.required],
      bet_delay: ['', Validators.required],
      eid: [''],
      sid: [''],
      mid: [''],
      commentary: [''],
      rules: [''],
      mtype: ['']
    });
  }

  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
      this.frm.reset();
      this.service.addMarketSetting(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate([this.setUrl + this.eid + '/' + this.sportId]);
    }
  }

  getFormData() {
    this.frm.patchValue({
      mid: this.mid, eid: this.eid, sid: this.sportId, mtype: this.mtype
    });
    const data = this.frm.value;
    return data;
  }

  get frmMaxStack() {
    return this.frm.get('max_stack');
  }

  get frmMinStack() {
    return this.frm.get('min_stack');
  }

  get frmMaxProfitLimit() {
    return this.frm.get('max_profit_limit');
  }

  get frmOddLimit() {
    return this.frm.get('max_odd_limit');
  }

  get frmBetDelay() {
    return this.frm.get('bet_delay');
  }

  get frmcommentary() {
    return this.frm.get('commentary');
  }

  get frmrules() {
    return this.frm.get('rules');
  }


}
