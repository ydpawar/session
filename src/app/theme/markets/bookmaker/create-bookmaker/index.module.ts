import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateBookmakerComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateBookmakerComponent
  },
  {
    path: 'create-bookmaker/:eid/:sid',
    component: CreateBookmakerComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateBookmakerComponent
  ]
})
export class CreateBookmakerModule {

}
