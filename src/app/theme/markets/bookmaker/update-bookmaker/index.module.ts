import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateBookmakerComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateBookmakerComponent
  },
  {
    path: 'update-bookmaker/:eid/:sid/:mid',
    component: UpdateBookmakerComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateBookmakerComponent
  ]
})
export class UpdateBookmakerModule {

}
