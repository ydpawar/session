import {Component, ViewEncapsulation, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';
import {AuthIdentityService} from '../../../../_services';

declare const $: any;

@Component({
  selector: 'app-update-bookmaker',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MarketService]
})
export class UpdateBookmakerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('diff', {static: false}) diff: ElementRef;
  frm: FormGroup;
  eid: string;
  sportId: string;
  mid: string;
  // tslint:disable-next-line:variable-name
  s_id: string;
  // tslint:disable-next-line:variable-name
  event_title: string;
  aList: any = [];
  cItem: string;
  fList: any = [];
  betList: any = [];
  bookList: any = [];
  favTeam: string;
  maxLoss: string;
  gameover: string;
  // tslint:disable-next-line:ban-types
  diffrance: Number = 1;
  // tslint:disable-next-line:ban-types
  ldiffrance: Number = 0;
  rateArr: any = [];
  ballrunning = false;
  suspend = false;
  lastPrice: string;
  timeleft = 60;
  sesstionTimeOut: string;
  stime = false;
  tmp: number;
  downloadTimer11: any;
  downloadTimer: any;
  betListFlag = true;
  public interval1000: any;
  public interval300: any;
  cBallRunning: string;
  cSuspended: string;
  marketStatus: string;
  timerSubscriptionBetList: any;
  user: any;
  activity = 1;
  isDestroy: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
    this.createForm();
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.mid = this.route.snapshot.params.mid;
    this.getSessionData();
    this.getBetData();
  }


  rateCreate() {
    let i = 1;
    while (i <= 100) {
      this.rateArr.push(i);
      i++;
    }
  }

  getSessionData() {
    this.service.bookmakerMarketLoad(this.mid, this.activity).subscribe((res) => {
      this.activity = 2;
      this.intSessionData(res);
    });
  }

  getBetData() {
    this.service.bookmakerbetList(this.mid).subscribe((res) => {
      this.intBetData(res);

    });
  }

  intBetData(res) {
    if (res !== 1) {
      this.betList = res.data.items;
      this.bookList = res.data.book;
    }
    if (!this.isDestroy) {
      const xhmBetData = setTimeout(() => {
        clearTimeout(xhmBetData);
        this.getBetData();
      }, 3000);
    }
  }

  intSessionData(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      // this.betList = res.data.betList;
      this.mid = this.mid;
      this.event_title = res.data.title;
      if (!this.s_id) {
        this.s_id = res.data.items.runners[0].secId;
        this.favTeam = res.data.items.runners[0].runner;
        this.gameover = res.data.items.game_over;
        this.sesstionTimeOut = res.data.items.sessionSuspendedTime;
        this.diffrance = res.data.items.diffrance;
      }
    }
  }


  ngOnDestroy() {
    this.isDestroy = true;
    this.betList = [];
    this.bookList = [];
    this.fList = [];
    if (this.interval1000) {
      clearInterval(this.interval1000);
      clearInterval(this.interval300);
    }

    if (this.timerSubscriptionBetList) {
      this.timerSubscriptionBetList.unsubscribe();
    }
    /* if (this.timerSubscriptionBook) {
       this.timerSubscriptionBook.unsubscribe();
     }*/
  }

  getFormData() {
    if (this.frm.value.back === '0' || this.frm.value.back === '00' || this.frm.value.back === '000' || this.frm.value.back === '0000' || this.frm.value.back === '00000' || this.frm.value.back === '000000' || this.frm.value.back === '') {
      // alert(this.frm.value.back);
      this.ballrunning = true;
      this.frm.patchValue({
        back: '',
      });
      this.lastPrice = '';

      // tslint:disable-next-line:max-line-length
      const data1 = {
        ball_running: true,
        suspended: false,
        eid: this.eid,
        mid: this.mid,
        sid: this.sportId,
        sec_id: this.s_id,
        both: this.s_id > '0' ? '0' : '1'
      };

      if (data1) {
        this.service.bookmakerupdate(data1).subscribe((res) => this.onSuccess(res));
      }

      clearInterval(this.downloadTimer11);
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }

    } else {
      const elemnt1: any = document.getElementById('no_run');
      elemnt1.select();

      this.ballrunning = false;
      this.frm.patchValue({
        lay: Number(this.frm.value.back) + Number(this.diffrance)
      });
      this.lastPrice = this.frm.value.back;
      const data = this.frm.value;
      data.eid = this.eid;
      data.mid = this.mid;
      data.sid = this.sportId;
      data.event_name = this.event_title;
      data.sec_id = this.s_id;
      data.ball_running = false;
      data.suspended = false;
      data.diffrance = Number(this.diffrance);
      data.both = this.s_id > '0' ? '0' : '1';

      // console.log('Rate', data);
      if (this.frm.valid || !isNaN(data.back) && data.back.toString().indexOf('.') !== -1) {
        this.service.bookmakerupdate(data).subscribe((res) => this.onSuccess(res));
      }
      if (this.tmp === undefined) {
        this.tmp = 0;
      } else {
        clearInterval(this.downloadTimer11);
      }
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }
    }
  }

  setSession() {
    let downloadTimer;
    if (this.tmp === 1) {
      clearInterval(downloadTimer);
    }
    let timeleft = parseInt(this.sesstionTimeOut, 10);
    const elemnt: any = document.getElementById('countdowntimer');
    elemnt.textContent = timeleft + 's';
    downloadTimer = setInterval(() => {
      timeleft--;
      const nn = timeleft >= 10 ? timeleft : '0' + timeleft;
      elemnt.textContent = nn + 's';
      if (timeleft <= 0) {
        clearInterval(downloadTimer);
      }
      if (timeleft === 0) {
        const elemnt1: any = document.getElementById('suspended');
        elemnt1.click();
      }
    }, 1000);
    this.downloadTimer11 = downloadTimer;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      back: ['', Validators.required],
      lay: ['', Validators.required],
      eid: [''],
      sid: [''],
      mid: ['']
    });
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      // this.betList = res.data.betList;
      this.intSessionData(res);
    }
  }

  ngAfterViewInit() {
    const nthis = this;
    // tslint:disable-next-line:only-arrow-functions
    document.onkeyup = function(e) {
      if (e.shiftKey && (e.which === 192)) { // sft+0
        nthis.diffrance = nthis.ldiffrance = 0;
      } else if (e.shiftKey && (e.which === 49 || e.which === 97)) { // sft+1
        nthis.diffrance = nthis.ldiffrance = 1;
      } else if (e.shiftKey && (e.which === 50 || e.which === 98)) { // sft+2
        nthis.diffrance = nthis.ldiffrance = 2;
      } else if (e.shiftKey && (e.which === 51 || e.which === 99)) { // sft+3
        nthis.diffrance = nthis.ldiffrance = 3;
      } else if (e.shiftKey && (e.which === 52 || e.which === 100)) { // sft+4
        nthis.diffrance = nthis.ldiffrance = 4;
      } else if (e.shiftKey && (e.which === 53 || e.which === 101)) { // sft+5
        nthis.diffrance = nthis.ldiffrance = 5;
      } else if (e.shiftKey && (e.which === 54 || e.which === 102)) { // sft+6
        nthis.diffrance = nthis.ldiffrance = 6;
      } else if (e.shiftKey && (e.which === 55 || e.which === 103)) { // sft+7
        nthis.diffrance = nthis.ldiffrance = 7;
      } else if (e.shiftKey && (e.which === 56 || e.which === 104)) { // sft+8
        nthis.diffrance = nthis.ldiffrance = 8;
      } else if (e.shiftKey && (e.which === 57 || e.which === 105)) { // sft+9
        nthis.diffrance = nthis.ldiffrance = 9;
      } else if (e.shiftKey && (e.which === 48 || e.which === 96)) { // sft+0
        nthis.diffrance = nthis.ldiffrance = 10;
      } else if (e.shiftKey && e.which === 65) { // sft+a
        nthis.selectTeam(nthis.fList.runners[0].secId);
      } else if (e.shiftKey && e.which === 83) { // sft+s
        nthis.selectTeam(nthis.fList.runners[1].secId);
      } else if (e.shiftKey && e.which === 68) { // sft+d
        nthis.selectTeam(nthis.fList.runners[2].secId);
      } else if (e.shiftKey && e.which === 90) { // sft+z
        nthis.selectTeam('0');
      }
    };

  }


  get frmBack() {
    return this.frm.get('back');
  }

  get frmLay() {
    return this.frm.get('lay');
  }


  limit(element) {
    // tslint:disable-next-line:variable-name
    const max_chars = 6;
    if (!this.validate(element.target.value)) {
      this.frm.patchValue({
        back: element.target.value.substr(0, element.target.value.length - 1)
      });
    }

    if (element.target.value.length > max_chars) {
      if (this.validate(element.target.value)) {
        this.frm.patchValue({
          back: element.target.value.substr(0, max_chars)
        });
      }
    }
  }


  setDiff() {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
  }


  diffCheck(element) {
    // alert('test' + element.target.value);
    const numbers = /^[0-9]+$/;
    // if (!this.validate(element.target.value)) {
    // alert('test2');
    this.diffrance = element.target.value.substr(0, element.target.value.length - 1);
    //  deleteCharAt
    this.diffrance = element.target.value;
    this.ldiffrance = element.target.value;
    // }
  }


  getProfitLossKhado(betList: any) {
    try {
      const dataReturn = [];
      let result = [];
      let newbetresult = [];
      const betresult = [];
      if (betList != null) {
        let min = 0;
        let max = 0;
        let index = 0;
        betList.forEach((itemB) => {
          const itemPrice = Number(itemB['price']);
          const itemDiff = Number(itemB['diff']);
          betresult.push({
            'price': itemPrice,
            'bet_type': itemB['bType'],
            'loss': itemB['loss'],
            'win': itemB['win'],
            'difference': itemDiff
          });
          console.log(betresult);
          if (index === 0) {
            min = itemPrice;
            max = itemDiff;
          }
          if (min > itemPrice) {
            min = itemPrice;
          }
          if (max < itemDiff) {
            max = itemDiff;
          }
          index++;
        });


        min = min - 20;
        if (min < 0) {
          min = 0;
        }
// $max = $max+1;
        max = max + 20;
        let betarray = [];
        let bet_type = '';
        let win = 0;
        let loss = 0;
        let count = min;
        let lossVal1 = 0;
        let totalbetcount = betresult.length;

        for (let i = min; i <= max; i++) {
          let winVal = 0;
          let lossVal = 0;
          const currentVal = Number(i);

          betList.forEach((itemB) => {
            if (Number(itemB['diff']) > currentVal && Number(itemB['price']) <= currentVal) {
              winVal += Number(itemB['win']);
            }

            if (Number(itemB['diff']) <= currentVal || Number(itemB['price']) > currentVal) {
              lossVal += Number(itemB['loss']);
            }
          });

          const total = winVal - lossVal;
          dataReturn.push({
            'price': i,
            'profitLoss': Math.round(total)
          });
        }

      }
      return dataReturn;
    } catch (e) {
      console.log('KHADO MARKET BOOK', e);
      return;
    }
  }

  validate(s) {
    // var rgx = /^[0-9]*\.?[0-9]*$/;
    const rgx = /^[0-9]*\.?[0-9]*$/;
    return s.match(rgx);
  }

  addRate(rate: string) {
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();
    this.ldiffrance = this.diffrance;
    // this.diffrance = 0;

    this.frm.patchValue({
      back: rate,
      lay: Number(rate) + Number(this.diffrance)
    });
  }


  zeroOneRate() {
    // debugger;
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();

    const data1 = {
      'zero-one-rate': true,
      eid: this.eid,
      sid: this.sportId,
      mid: this.mid,
      sec_id: this.s_id,
      event_name: this.event_title,
      diff: this.diffrance
    };
    if (data1) {
      $('#no_run').val(0);
      $('#yes_run').val(this.diffrance);
      this.service.bookmakerupdate(data1).subscribe((res) => this.onSuccess(res));
    }
  }

  suspended() {
    const data1 = {
      ball_running: false,
      suspended: true,
      eid: this.eid,
      sid: this.sportId,
      mid: this.mid,
      sec_id: this.s_id,
      event_name: this.event_title
    };
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    if (data1) {
      this.service.bookmakerupdate(data1).subscribe((res) => this.onSuccess(res));
    }
  }


  selectTeam(sId) {
    // debugger;
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();
    if (sId !== '0') {
      this.s_id = sId;
      let favTeam;
      // tslint:disable-next-line:prefer-const
      let diff;
      this.fList.runners.forEach((item) => {
        if (item.secId === sId) {
          favTeam = item.runner;
          diff = item.diffrance;
        }
      });
      this.favTeam = favTeam;
      this.diffrance = diff;
      // if ( this.ldiffrance > 0 ) {
      // this.diffrance = this.ldiffrance;
      // }

    } else {
      this.ldiffrance = this.diffrance;
      this.favTeam = 'Both / All';
      this.s_id = '0';
      this.diffrance = 0;
    }
  }

  doc_keyUp(e) {
    e.preventDefault();
    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode === 13) {
      // call your function to do the thing
      const elemnt1: any = document.getElementById('suspended');
      elemnt1.click();
      return false;
    }

    if (e.ctrlKey && e.keyCode === 40) {
      // call your function to do the thing
      console.log('ball running', e.keyCode);
    }
  }

}
