import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BookmakerComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateBookmakerComponent} from './create-bookmaker/index.component';
import {CreateBookmakerModule} from './create-bookmaker/index.module';

import {UpdateBookmakerComponent} from './update-bookmaker/index.component';
import {UpdateBookmakerModule} from './update-bookmaker/index.module';
const routes: Routes = [
  {
    path: '',
    component: BookmakerComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-bookmaker/:eid/:sid',
        component: CreateBookmakerComponent
      },
      {
        path: 'update-bookmaker/:eid/:sid',
        component: UpdateBookmakerComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateBookmakerModule, UpdateBookmakerModule
  ], exports: [
    RouterModule
  ], declarations: [ BookmakerComponent ]
})
export class BookmakerModule {

}
