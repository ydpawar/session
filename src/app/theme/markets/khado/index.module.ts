import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {KhadoComponent} from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateKhadoComponent} from './create-khado/index.component';
import {CreateKhadoModule} from './create-khado/index.module';

import {UpdateKhadoComponent} from './update-khado/index.component';
import {UpdateKhadoModule} from './update-khado/index.module';

const routes: Routes = [
  {
    path: '',
    component: KhadoComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-khado/:eid/:sid',
        component: CreateKhadoComponent
      },
      {
        path: 'update-khado/:eid/:sid',
        component: UpdateKhadoComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateKhadoModule, UpdateKhadoModule
  ], exports: [
    RouterModule
  ], declarations: [KhadoComponent]
})
export class KhadoModule {

}
