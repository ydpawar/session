import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateKhadoComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateKhadoComponent
  },
  {
    path: 'create-khado/:eid/:sid',
    component: CreateKhadoComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateKhadoComponent
  ]
})
export class CreateKhadoModule {

}
