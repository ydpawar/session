import {Component, ViewEncapsulation, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';
import swal from 'sweetalert2';
import {AuthIdentityService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';

declare const $: any;

@Component({
  selector: 'app-update-khado',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MarketService]
})
export class UpdateKhadoComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('diff', {static: false}) diff: ElementRef;
  frm: FormGroup;
  eid: string;
  sportId: string;
  mid: string;
  // tslint:disable-next-line:variable-name
  event_title: string;
  marketName: string;
  bookDataFancy: any = null;
  aList: any = [];
  cItem: string;
  fList: any = [];
  betList: any = [];
  maxLoss: string;
  // tslint:disable-next-line:ban-types
  diffrance: Number = 1;
  // tslint:disable-next-line:ban-types
  ldiffrance: Number = 0;
  rateArr: any = [];
  ballrunning = false;
  suspend = false;
  lastPrice: string;
  lastYesRate: string;
  lastNoRate: string;
  fancyMarketBookData: any = [];
  timeleft = 60;
  sesstionTimeOut: string;
  stime = false;
  tmp: number;
  downloadTimer11: any;
  downloadTimer: any;
  betListFlag = true;
  public interval1000: any;
  public interval300: any;
  cNoPrice: string;
  cYesPrice: string;
  cNoRate: string;
  cYesRate: string;
  cBallRunning: number;
  cSuspended: number;
  marketStatus: string;
  timerSubscriptionBetList: any;
  isDestroy: boolean = false;
  user: any;
  isEmpty = false;
  activity = 1;
  marketIds: any = [];

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    this.createForm();
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.mid = this.route.snapshot.params.mid;
    this.getSessionData();
    this.getBetData();
    // console.log(this.eid + ' ' + this.sportId);
    // this.diff.nativeElement.value = 1;
  }

  getSessionData() {
    this.service.khadoMarketLoad(this.mid, this.activity).subscribe((res) => {
      this.activity = 2;
      this.intSessionData(res);
    });
  }

  intSessionData(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      // this.betList = res.data.betList;
      this.mid = res.data.items.market_id;
      this.event_title = res.data.title;
      this.sesstionTimeOut = res.data.items.sessionSuspendedTime;
      this.rateArr = res.data.rateOption;

      this.marketStatus = this.fList.status;
      // const elemnt1: any = document.getElementById('no_run') as HTMLElement;
      // elemnt1.focus();
    }
  }

  getBetData() {
    this.service.ballbyballbetList(this.mid).subscribe((res) => {
      this.intBetData(res);
      if (!this.isDestroy) {
        const xhmBetData = setTimeout(() => {
          clearTimeout(xhmBetData);
          this.getBetData();
        }, 3000);
      }
    });
  }

  intBetData(res) {
    if (res !== 1) {
      this.betList = res.data.items;
    }
  }

  ngOnDestroy() {
    this.isDestroy = true;
    this.betList = [];
    this.fList = [];
    if (this.interval1000) {
      clearInterval(this.interval1000);
      clearInterval(this.interval300);
    }

    if (this.timerSubscriptionBetList) {
      this.timerSubscriptionBetList.unsubscribe();
    }

  }

  getFormData() {

    // console.log(this.frm);
    if (this.frm.value.no_val === '0' || this.frm.value.no_val === '00' || this.frm.value.no_val === '000' || this.frm.value.no_val === '0000' || this.frm.value.no_val === '00000' || this.frm.value.no_val === '000000' || this.frm.value.no_val === '') {

      this.ballrunning = true;
      this.cBallRunning = 1;
      /*  this.frm.patchValue({
          no_val: 0,
        });*/
      this.lastPrice = '';
      this.diffrance = this.frm.value.diffrance;
      const data1 = {
        ball_running: true,
        suspended: false,
        mid: this.mid,
        diffrance: this.diffrance,
        sid: this.sportId,
        event_name: this.event_title
      };
      if (data1) {
        $('#no_run').val('');
        this.service.khadoupdate(data1).subscribe((res) => this.onSuccess(res));
      }
      if (this.tmp === 0) {
        this.tmp = 1;
      }
      clearInterval(this.downloadTimer11);
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }

    } else {
      const elemnt1: any = document.getElementById('no_run') as HTMLElement;
      elemnt1.select();

      this.ballrunning = false;
      this.lastPrice = this.frm.value.no_val;
      this.lastYesRate = this.frm.value.yes_rate;

      this.cBallRunning = 0;
      this.cSuspended = 0;
      this.cNoPrice = this.frm.value.no_val;
      this.cYesRate = this.frm.value.yes_rate;
      this.diffrance = this.frm.value.diffrance;

      const data = this.frm.value;
      data.eid = this.eid;
      data.mid = this.mid;
      data.sid = this.sportId;
      data.event_name = this.event_title;
      data.ball_running = false;
      data.suspended = false;

      if (this.frm.valid) {
        this.service.khadoupdate(data).subscribe((res) => this.onSuccess(res));
      }

      if (this.tmp === undefined) {
        this.tmp = 0;
      } else {
        clearInterval(this.downloadTimer11);
      }
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }
    }
  }

  setSession() {
    let downloadTimer;
    if (this.tmp === 1) {
      clearInterval(downloadTimer);
    }
    let timeleft = parseInt(this.sesstionTimeOut, 10);
    const elemnt: any = document.getElementById('countdowntimer') as HTMLElement;
    // console.log(elemnt);
    elemnt.textContent = timeleft + 's';
    downloadTimer = setInterval(() => {
      timeleft--;
      const nn = timeleft >= 10 ? timeleft : '0' + timeleft;
      elemnt.textContent = nn + 's';
      if (timeleft <= 0) {
        clearInterval(downloadTimer);
      }
      if (timeleft === 0) {
        const elemnt1: any = document.getElementById('suspended') as HTMLElement;
        elemnt1.click();
      }
    }, 1000);
    this.downloadTimer11 = downloadTimer;

    if (timeleft === 0) {
      // debugger;
      const data1 = {ball_running: false, suspended: true, mid: this.mid, sid: this.sportId, event_name: this.event_title};
      this.service.khadoupdate(data1).subscribe((res) => this.onSuccess(res));
    }

  }


  createForm() {
    this.frm = this.formBuilder.group({
      diffrance: ['0', Validators.required],
      no_val: ['', Validators.required],
      yes_rate: ['100', Validators.required]
    });
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      this.intSessionData(res);
      // this.betList = res.data.betList;
    }
  }

  suspended() {
    const data1 = {
      ball_running: false,
      suspended: true,
      mid: this.mid,
      sid: this.sportId,
      diffrance: this.diffrance,
      event_name: this.event_title
    };
    this.cSuspended = 1;
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    if (data1) {
      this.service.khadoupdate(data1).subscribe((res) => this.onSuccess(res));
    }
  }

  ngAfterViewInit() {

  }

  get frmKhadoRange() {
    return this.frm.get('diffrance');
  }

  get frmNoVal() {
    return this.frm.get('no_val');
  }

  get frmYesRate() {
    return this.frm.get('yes_rate');
  }


  limit(element) {
    // tslint:disable-next-line:variable-name
    const max_chars = 3;
    if (!this.validate(element.target.value)) {
      this.frm.patchValue({
        no_val: element.target.value.substr(0, element.target.value.length - 1)
      });
    }

    if (element.target.value.length > max_chars) {
      if (this.validate(element.target.value)) {
        this.frm.patchValue({
          no_val: element.target.value.substr(0, max_chars)
        });
      }
    }
  }


  validate(s) {
    // var rgx = /^[0-9]*\.?[0-9]*$/;
    const rgx = /^\d+$/;
    return s.match(rgx);
  }

  addRate(rate: string) {
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();

    const newRate = rate.split('/');
    // this.ldiffrance =  this.diffrance;
    this.diffrance = 0;
    if (newRate[0] === '100' && newRate[1] === '100') {
      this.diffrance = 1;
    }
    this.frm.patchValue({
      yes_rate: newRate[0]
    });
  }

  changeStatus(id) {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    swal.fire({
      title: 'Confirm Change Status?',
      text: 'You won\'t be able to recover this entry!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    }).then((result) => {
      if (result.value) {
        this.initStatus(id);
      }
    });
  }

  initStatus(id) {
    const data = {id};
    this.service.khadostatus(this.mid, data).subscribe((res) => this.onManualSessionChangeStatusSuccess(res));
  }

  onManualSessionChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.getSessionData();
    }
  }


  marketBook() {
    const data1 = {mid: this.mid, sid: this.sportId, eid: this.eid, m_type: 'khado'};
    if (data1) {
      this.service.marketBook(data1).subscribe((res) => this.onSuccessBook(res));
    }
  }

  onSuccessBook(res) {

    // console.log(res);
    $('#bookModal').modal();
    if (res.status === 1) {

      this.fancyMarketBookData = [];
      res.data.forEach(item => {
        this.fancyMarketBookData.push(item);
      });
    }
  }

  getBookDataKhadoNew() {
    this.spinner.show();
    // this.marketName = marketName;
    const data = {userId: 1, marketId: this.mid};
    this.service.getBookDataKhadoNew(data).subscribe((res) => this.onSuccessBookDataKhadoNew(res));
  }


  onSuccessBookDataKhadoNew(res) {
    if (res.status === 1) {
      let i = 0;
      const min = res.data.min;
      const max = res.data.max;
      const dataReturn = [];
      for (i = min; i <= max; i++) {
        const betlist = res.data.betList;
        let total = 0;
        let winVal = 0;
        let lossVal = 0;
        if (res.data.userRole !== 4) {
          // tslint:disable-next-line:only-arrow-functions
          betlist.forEach(function(bets) {
            if (bets.diff > i && bets.price <= i) {
              winVal = winVal + (((bets.win) * (bets.apl) / 100));
            } else if (bets.diff <= i || bets.price > i) {
              lossVal = lossVal + (((bets.loss) * (bets.apl) / 100));
            }
          });
          total = lossVal - winVal;
        } else {
          // tslint:disable-next-line:only-arrow-functions
          betlist.forEach(function(bets) {
            if (bets.diff > i && bets.price <= i) {
              winVal = winVal + bets.win;
            } else if (bets.diff <= i || bets.price > i) {
              lossVal = lossVal + bets.loss;
            }
          });
          total = winVal - lossVal;
        }

        dataReturn.push({price: i, profitLoss: Math.round(total)});
      }

      $('#bookModal').modal();
      this.fancyMarketBookData = dataReturn;


      //  if (res.status === 1) {

      // this.fancyMarketBookData = [];
      // res.data.forEach(item => {  this.fancyMarketBookData.push(item); });
      //  }

    }
    this.spinner.hide();
  }


  closeModal(id: string) {
    $('#' + id).toggleClass('show');
    $('body').removeClass('modal-open');
    clearInterval(this.interval1000);
  }


}
