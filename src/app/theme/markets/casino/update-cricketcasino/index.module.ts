import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateCricketCasinoComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateCricketCasinoComponent
  },
  {
    path: 'update-cricketcasino/:eid/:sid/:mid',
    component: UpdateCricketCasinoComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateCricketCasinoComponent
  ]
})
export class UpdateCricketCasinoModule {

}
