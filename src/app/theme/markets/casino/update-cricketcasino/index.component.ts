import {Component, ViewEncapsulation, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';

@Component({
  selector: 'app-update-bookmaker',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MarketService]
})
export class UpdateCricketCasinoComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('diff', {static: false}) diff: ElementRef;
  frm: FormGroup;
  eid: string;
  sportId: string;
  mid: string;
  // tslint:disable-next-line:variable-name
  s_id: string;
  // tslint:disable-next-line:variable-name
  event_title: string;
  aList: any = [];
  cItem: string;
  fList: any = [];
  betList: any = [];
  favTeam: string;
  maxLoss: string;
  // tslint:disable-next-line:ban-types
  diffrance: Number = 1;
  // tslint:disable-next-line:ban-types
  ldiffrance: Number = 0;
  rateArr: any = [];
  ballrunning = false;
  suspend = false;
  lastPrice: string;
  timeleft = 60;
  sesstionTimeOut: string;
  stime = false;
  tmp: number;
  downloadTimer11: any;
  downloadTimer: any;
  betListFlag = true;
  public interval1000: any;
  public interval300: any;
  cBallRunning: string;
  cSuspended: string;
  marketStatus: string;
  timerSubscriptionBetList: any;
  activity = 1;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.mid = this.route.snapshot.params.mid;
    this.getSessionData();
    document.addEventListener('keyup', this.doc_keyUp, false);
  }

  rateCreate() {
    let i = 1;
    while (i <= 100) {
      this.rateArr.push(i);
      i++;
    }
  }

  getSessionData() {
    this.service.bookmakerMarketLoad(this.mid, this.activity).subscribe((res) => {
      this.activity = 2;
      this.intSessionData(res);
    });
  }

  intSessionData(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      this.betList = res.data.betList;
      this.mid = this.mid;
      this.event_title = res.data.title;
      if (!this.s_id) {
        this.s_id = res.data.items.runners[0].secId;
        this.favTeam = res.data.items.runners[0].runner;
        this.sesstionTimeOut = res.data.items.sessionSuspendedTime;
      }
    }
  }


  ngOnDestroy() {
    if (this.interval1000) {
      clearInterval(this.interval1000);
      clearInterval(this.interval300);
    }

    if (this.timerSubscriptionBetList) {
      this.timerSubscriptionBetList.unsubscribe();
    }
   /* if (this.timerSubscriptionBook) {
      this.timerSubscriptionBook.unsubscribe();
    }*/
  }

  getFormData() {

    if (this.frm.value.back === 0 ) {
      this.ballrunning = true;
      this.frm.patchValue({
        back: '',
      });
      this.lastPrice = '';

      const data1 = {ball_running: true, suspended: false, eid: this.eid, mid: this.mid, sec_id: this.s_id, both: this.s_id > '0' ? '0' : '1'};
      if (data1) {
        this.service.bookmakerupdate(data1).subscribe((res) => this.onSuccess(res));
      }

      clearInterval(this.downloadTimer11);
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }

    } else {
      const elemnt1: any = document.getElementById('no_run');
      elemnt1.select();

      this.ballrunning = false;
      this.frm.patchValue({
        lay: Number(this.frm.value.back) + Number(this.diffrance)
      });
      this.lastPrice = this.frm.value.back;
      const data = this.frm.value;
      data.eid = this.eid;
      data.mid = this.mid;
      data.sid = this.sportId;
      data.event_name = this.event_title;
      data.sec_id = this.s_id;
      data.ball_running = false;
      data.suspended = false;
      data.both = this.s_id > '0' ? '0' : '1';

      // console.log('Rate', data);
      if (this.frm.valid || !isNaN(data.back) && data.back.toString().indexOf('.') !== -1) {
        this.service.bookmakerupdate(data).subscribe((res) => this.onSuccess(res));
      }
      if (this.tmp === undefined) {
        this.tmp = 0;
      } else {
        clearInterval(this.downloadTimer11);
      }
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }
    }
  }

  setSession() {
    let downloadTimer;
    if (this.tmp === 1) {
      clearInterval(downloadTimer);
    }
    let timeleft = parseInt(this.sesstionTimeOut, 10);
    const elemnt: any = document.getElementById('countdowntimer');
    elemnt.textContent = timeleft + 's';
    downloadTimer = setInterval(() => {
      timeleft--;
      const nn = timeleft >= 10 ? timeleft : '0' + timeleft;
      elemnt.textContent = nn + 's';
      if (timeleft <= 0) {
        clearInterval(downloadTimer);
      }
      if (timeleft === 0) {
        const elemnt1: any = document.getElementById('suspended');
        elemnt1.click();
      }
    }, 1000);
    this.downloadTimer11 = downloadTimer;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      back : ['', Validators.required],
      lay : ['', Validators.required],
      eid: [''],
      sid: [''],
      mid: ['']
    });
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      this.betList = res.data.betList;
    }
  }

  ngAfterViewInit() {
    const nthis = this;
    // tslint:disable-next-line:only-arrow-functions
    document.onkeyup = function(e) {
      if (e.shiftKey && (e.which === 192)) { // sft+0
        nthis.diffrance = 0;
      } else if (e.shiftKey && (e.which === 49 || e.which === 97)) { // sft+1
        nthis.diffrance = 1;
      } else if (e.shiftKey && (e.which === 50 || e.which === 98)) { // sft+2
        nthis.diffrance = 2;
      } else if (e.shiftKey && (e.which === 51 || e.which === 99)) { // sft+3
        nthis.diffrance = 3;
      } else if (e.shiftKey && e.which === 65) { // sft+a
        nthis.diffrance = 1;
        nthis.frm.patchValue({no_rate: '100', yes_rate: '100'});
      } else if (e.shiftKey && e.which === 83) { // sft+s
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '110', yes_rate: '90'});
      } else if (e.shiftKey && e.which === 90) { // sft+z
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '115', yes_rate: '85'});
      } else if (e.shiftKey && e.which === 88) { // sft+x
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '120', yes_rate: '80'});
      } else if (e.shiftKey && e.which === 68) { // sft+d
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '120', yes_rate: '90'});
      } else if (e.shiftKey && e.which === 67) { // sft+c
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '125', yes_rate: '75'});
      }
    };


  }


  get frmBack() { return this.frm.get('back'); }
  get frmLay() { return this.frm.get('lay'); }


  limit(element) {
    // tslint:disable-next-line:variable-name
    const max_chars = 6;
    if (!this.validate(element.target.value)) {
      this.frm.patchValue({
        back: element.target.value.substr(0, element.target.value.length - 1)
      });
    }

    if (element.target.value.length > max_chars) {
      if (this.validate(element.target.value)) {
        this.frm.patchValue({
          back: element.target.value.substr(0, max_chars)
        });
      }
    }
  }



  setDiff() {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
  }


  diffCheck(element) {
    // alert('test' + element.target.value);
    const numbers = /^[0-9]+$/;
    // if (!this.validate(element.target.value)) {
     // alert('test2');
    this.diffrance = element.target.value.substr(0, element.target.value.length - 1);
      //  deleteCharAt
    this.diffrance = parseInt(element.target.value, 10);
    this.ldiffrance = parseInt(element.target.value, 10);
   // }
  }

  validate(s) {
    // var rgx = /^[0-9]*\.?[0-9]*$/;
    const rgx = /^[0-9]*\.?[0-9]*$/;
    return s.match(rgx);
  }

  addRate(rate: string) {
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();
    this.ldiffrance =  this.diffrance;
    // this.diffrance = 0;

    this.frm.patchValue({
      back: rate,
      lay: Number(rate) + Number(this.diffrance)
    });
  }


  zeroOneRate() {
    // debugger;
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();

    const data1 = {'zero-one-rate': true, eid: this.eid, sid: this.sportId, mid: this.mid, sec_id: this.s_id, event_name: this.event_title, diff : this.diffrance};
    if (data1) {
      this.service.bookmakerupdate(data1).subscribe((res) => this.onSuccess(res));
    }
  }

  suspended() {
    const data1 = {ball_running: false, suspended: true, eid: this.eid, sid: this.sportId, mid: this.mid, sec_id: this.s_id, event_name: this.event_title };
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    if (data1) {
      this.service.bookmakerupdate(data1).subscribe((res) => this.onSuccess(res));
    }
  }


  selectTeam(sId) {
    // debugger;
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();
    if (sId !== '0') {
      this.s_id = sId;
      let favTeam;
      this.fList.runners.forEach( (item) => { if (item.secId === sId) {  favTeam = item.runner; } });
      this.favTeam = favTeam;
      if ( this.ldiffrance > 0 ) {
        this.diffrance = this.ldiffrance;
      }

    } else {
      this.ldiffrance = this.diffrance;
      this.favTeam = 'Both / All';
      this.s_id = '0';
      this.diffrance = 0;
    }
  }

  doc_keyUp(e) {
    e.preventDefault();
    // this would test for whichever key is 40 and the ctrl key at the same time
    if (e.ctrlKey && e.keyCode === 13) {
      // call your function to do the thing
      const elemnt1: any  = document.getElementById('suspended');
      elemnt1.click();
      return false;
    }

    if (e.ctrlKey && e.keyCode === 40) {
      // call your function to do the thing
      console.log('ball running', e.keyCode);
    }
  }

}
