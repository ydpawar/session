import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateCricketCasinoComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateCricketCasinoComponent
  },
  {
    path: 'create-cricketcasino/:eid/:sid',
    component: CreateCricketCasinoComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateCricketCasinoComponent
  ]
})
export class CreateCricketCasinoModule {

}
