import {Component, OnInit, OnDestroy} from '@angular/core';
import {GameOverService, MarketService} from '../../../../_api/index';
import {ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService, ToastrService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';
import * as XLSX from 'ts-xlsx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare const $: any;

interface ListManage {
  id: string;
  sid: string;
  eid: string;
  result: string;
  game_over: string;
  title: string;
  marketId: string;
  rate: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-bookmaker',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService, GameOverService]
})
export class ListComponent implements OnInit, OnDestroy {

  page = {start: 1, end: 5};
  user: any;
  aList: ListManage[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;
  id: string;
  sportId: string;
  title: string;
  errorMsg: '';
  errorMsg2: '';
  pageLoad: any;
  eventName: string;

  frmUpload: FormGroup;
  arrayBuffer: any;
  fileData: any = [];
  file: File;
  fileError = '';

  constructor(private service: MarketService, private formBuilder: FormBuilder,
              private loadScript: ScriptLoaderService, private service2: GameOverService,
              private route: ActivatedRoute, private spinner: NgxSpinnerService, private toastr: ToastrService
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.errorMsg2 = '';
    this.id = this.route.snapshot.params.id;
    this.sportId = this.route.snapshot.params.sid;
    this.eventName = window.sessionStorage.getItem('title');
    this.applyFilters();

    this.frmUpload = this.formBuilder.group({
      eid: [this.id, Validators.required],
      sid: [this.sportId, Validators.required],
      data: [''],
    });
  }

  ngOnDestroy(): void {
    this.aList = [];
  }


  applyFilters() {
    this.spinner.show();
    const data = this.id;
    this.service.casinoListLoad(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }
    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      const items = res.data;
      if (items.length) {
        this.aList = res.data;
      } else {
        this.isEmpty = true;
      }

      this.page.end = this.page.end + items.length - 1;
      this.loadScript.load('script', 'assets/js/datatables.init.js');
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    } else {
      // this.isEmpty = true;
      this.spinner.hide();
      this.errorMsg = res.success.message;
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }

  changeStatus(item: ListManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + item.title + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.casinostatus(this.cItem.marketId, this.cItem.status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }


  gameOver(eid, mId, markettype, sid, title) {
    let myArrayOfThings;
    myArrayOfThings = [
      {id: 0, name: 0},
      {id: 1, name: 1},
      {id: 2, name: 2},
      {id: 3, name: 3},
      {id: 4, name: 4},
      {id: 5, name: 5},
      {id: 6, name: 6},
      {id: 7, name: 7},
      {id: 8, name: 8},
      {id: 9, name: 9}

    ];


    const options = {};
    $.map(myArrayOfThings,
      // tslint:disable-next-line:only-arrow-functions
      function(o) {
        options[o.id] = o.name;
      });
    swal.fire({
      title: 'Confirm Game Over?',
      text: title,
      input: 'select',
      inputOptions: options,
      // content: select,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid, mId, result, markettype, sid);
        }
      });

  }

  initGameOver(id, mId, results, markettype, sid) {
    const data = {eventId: id, marketId: mId, winResult: results.value, mType: markettype, sportId: sid};
    // console.log(data);
    this.service2.gameOverCasino(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, item, eid) {
    swal.fire({
      title: 'Confirm Game Recall?',
      text: item.title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    }).then((result) => {
      if (result.value) {
        this.initGameRecall(id, eid);
      }
    });
  }

  initGameRecall(id, eid) {
    const data = {marketId: id, eventId: eid, mType: 'cricket_casino'};
    this.service2.gameRecallCasino(data).subscribe((res) => this.onGameOver(res));
  }


  abundant(eid, mid, type, sid, title) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    }).then((result) => {
      if (result.value) {
        this.initGameAbundant(mid, eid);
      }
    });
  }

  initGameAbundant(mid, eid) {
    const data = {marketId: mid, eventId: eid, mType: 'cricket_casino'};
    this.service2.abundantcasino(data).subscribe((res) => this.onGameOver(res));
  }

  incomingfile(event) {
    this.file = event.target.files[0];
    this.Upload();
  }

  Upload() {
    const fileData = [];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      // tslint:disable-next-line:variable-name
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
      this.fileData.push(XLSX.utils.sheet_to_json(worksheet, {raw: true}));
    };
    fileReader.readAsArrayBuffer(this.file);

  }


  submitForm() {
    if (this.fileData.length > 0) {
      this.frmUpload.patchValue({
        data: this.fileData[0] ? this.fileData[0] : this.fileData
      });
      /// console.log(this.frmUpload.value);
      if (this.frmUpload.valid) {
        this.errorMsg2 = '';
        this.service.casinoimport(this.frmUpload.value).subscribe((res) => this.onSuccessJack(res));
      }
    }
  }

  onSuccessJack(res) {
    if (res.status === 1) {
      this.fileData = [];
      this.frmUpload.reset();
      $('#import-files').modal('hide');
      this.applyFilters();
    } else {
      this.errorMsg2 = res.error.message;
    }
  }


}
