import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CasinoComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateCricketCasinoComponent} from './create-cricketcasino/index.component';
import {CreateCricketCasinoModule} from './create-cricketcasino/index.module';

import {UpdateCricketCasinoComponent} from './update-cricketcasino/index.component';
import {UpdateCricketCasinoModule} from './update-cricketcasino/index.module';
const routes: Routes = [
  {
    path: '',
    component: CasinoComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-cricketcasino/:eid/:sid',
        component: CreateCricketCasinoComponent
      },
      {
        path: 'update-cricketcasino/:eid/:sid',
        component: UpdateCricketCasinoComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateCricketCasinoModule, UpdateCricketCasinoModule
  ], exports: [
    RouterModule
  ], declarations: [ CasinoComponent ]
})
export class CasinoModule {

}
