import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BinaryComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';

const routes: Routes = [
  {
    path: '',
    component: BinaryComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule
  ], exports: [
    RouterModule
  ], declarations: [ BinaryComponent ]
})
export class BinaryModule {

}
