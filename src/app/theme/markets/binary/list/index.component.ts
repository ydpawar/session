import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {MarketService, GameOverService} from '../../../../_api/index';
import {ActivatedRoute} from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import {AuthIdentityService, DataService, ScriptLoaderService, ToastrService} from '../../../../_services';
import { NgxSpinnerService} from 'ngx-spinner';
import * as XLSX from 'ts-xlsx';
declare const $: any;

interface ListManage {
  id: string;
  sportId: string;
  name: string;
  type: string;
  time: string;
  marketId: string;
  title: string;
  sid: string;
  game_over: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-binary',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, GameOverService, NgxSpinnerService]
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  frmUpload: FormGroup;
  page = {start: 1, end: 5};
  user: any;
  aList: ListManage[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;
  id: string;
  sportId: string;
  eventName: string;
  setUrl: any;
  arrayBuffer: any;
  fileData: any = [];
  file: File;
  fileError = '';
  errorMsg = '';
  pageLoad: string;

  constructor(private service: MarketService, private formBuilder: FormBuilder,
              private service2: GameOverService,
              private loadScript: ScriptLoaderService,
              private route: ActivatedRoute, private spinner: NgxSpinnerService, private toastr: ToastrService,
              private dataServices: DataService

  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.sportId = this.route.snapshot.params.sid;
    this.applyFilters();
    this.eventName = window.sessionStorage.getItem('title');
    this.setUrl = this.dataServices.getSportMarketPath(this.sportId);
    this.frmUpload = this.formBuilder.group({
      eid: [this.id, Validators.required],
      sid: [this.sportId, Validators.required],
      data: [''],
    });
  }

  ngAfterViewInit() {
  }

  ngOnDestroy(): void {
    this.aList = [];
    window.localStorage.removeItem('title');
  }

  applyFilters() {
    this.spinner.show();
    this.service.binaryListLoad(this.id, this.sportId).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1  && res.data !== undefined) {
      // this.isLoading = false;
        const items = res.data;
        if (items.length) {
          this.aList = res.data;
        } else {
          this.isEmpty = true;
        }

        this.page.end = this.page.end + items.length - 1;
        this.loadScript.load('script', 'assets/js/datatables.init.js');
        this.spinner.hide();
        setTimeout(() => { this.spinner.hide(); }, 500);
    } else {
      this.spinner.hide();
      this.errorMsg = res.success.message;
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }

  refreshFancy(eid, sid) {
    swal.fire({
      title: 'Confirm Refresh Market?',
      text: 'You won`t be able to recover this entry!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.addBinary();
        }
      });
  }

  addBinary() {
    this.spinner.show();
    this.service.binaryAdd(this.id, this.sportId).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  changeStatus(item: ListManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + item.title + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.binarystatus(this.cItem.marketId, this.cItem.status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  // Suspended
  changeSuspendedStatus(item) {
    this.cItem = item;
    let stat = 'Suspended: no';
    // @ts-ignore
    if (item.suspended === 0) {
      stat = 'Suspended: yes';
    }

    swal.fire({
      title: 'You want to ' + stat,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    }).then((result) => {
      if (result.value) {
        this.initStatusSuspended(item);
      }
    });
  }

  initStatusSuspended(item) {
    this.spinner.show();
    this.service.binarysuspendedstatus(this.cItem.marketId).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }

  changetitle(eventid, mId, item, markettype, sid) {
    swal.fire({
     //  title: 'Multiple inputs',
      html:
        '<input id="title" readonly class="swal2-input" value="' + item.title + '" placeholder="Title">' +
        '<input id="rules" class="swal2-input" value="' + item.info + '" placeholder="Rules">',
      preConfirm:  () => {
        return new Promise( (resolve) => {
          resolve([
            $('#title').val(),
            $('#rules').val()
          ]);
        });
      },
      showCancelButton: true,
      confirmButtonText: 'Save',
      onOpen: () => {
        $('#title').focus();
      }
    }).then( (result) => {
      const data = {id: eventid, mid: mId, title: result.value[0], info: result.value[1]};
      this.service.changefancytitle(data).subscribe((res) => this.onChangeStatusSuccess(res));
    });

  }

  gameOver(eid, mId, item, markettype, sid) {
    swal.fire({
      title: 'Confirm Game Over?',
      text: item.title,
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off',
        placeholder: 'Winner Result',
        type: 'number',
        pattern: '[0-9]*',
      },
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid , mId, result, markettype, sid);
        }
      });

  }

  initGameOver(id, mId, results, markettype, sid) {
    const data = {eventId: id, marketId: mId, winResult: results.value, mType: markettype, sportId: sid};
    // console.log(data);
    this.service2.gameOverBinary(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, item, eid, mType) {
    swal.fire({
      title: 'Confirm Game Recall?',
      text: item.title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameRecall(id, eid, mType);
        }
      });
  }

  initGameRecall(id, eid, mtype) {
    const data = { marketId: id, eventId: eid, mType: mtype};
    this.service2.gameRecallBinary(data).subscribe((res) => this.onGameOver(res));
  }



  abundant(eid, mid, type, sid, title) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameAbundant(mid, eid, type);
        }
      });
  }

  initGameAbundant(mid, eid, type) {
    const data = { marketId: mid, eventId: eid, mType: type};
    this.service2.abundantBinary(data).subscribe((res) => this.onGameOver(res));
  }


  incomingfile(event) {
    this.file = event.target.files[0];
    this.Upload();
  }

  Upload() {
    const fileData = [];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      // tslint:disable-next-line:variable-name
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
      this.fileData.push(XLSX.utils.sheet_to_json(worksheet, {raw: true}));
    };
    fileReader.readAsArrayBuffer(this.file);

  }


  submitForm() {
    if (this.fileData.length > 0) {
      this.frmUpload.patchValue({
        data: this.fileData[0] ? this.fileData[0] : this.fileData
      });
      /// console.log(this.frmUpload.value);
      if (this.frmUpload.valid) {
        this.errorMsg = '';
        this.service.fancy2import(this.frmUpload.value).subscribe((res) => this.onSuccessJack(res));
      }
    }
  }

  onSuccessJack(res) {
    if (res.status === 1) {
      this.fileData = [];
      this.frmUpload.reset();
      $('#import-files').modal('hide');
      this.applyFilters();
    } else {
      this.errorMsg = res.error.message;
    }
  }
}
