import { Component, OnInit, AfterViewInit } from '@angular/core';


@Component({
  selector: 'app-fancy',
  templateUrl: './index.component.html',
})
export class FancyComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }
}

