import {Component, OnInit, AfterViewInit, OnDestroy, ViewChild, Renderer2, ElementRef} from '@angular/core';
import {MarketService, GameOverService} from '../../../../_api/index';
import {ActivatedRoute, Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import swal from 'sweetalert2';
import {AuthIdentityService, DataService, ScriptLoaderService, ToastrService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';
import * as XLSX from 'ts-xlsx';
import {Subject} from 'rxjs';
declare const $: any;

interface ListOfRunner {
  runner: string;
  secId: string;
}
interface ListManage {
  id: number;
  sid: number;
  eid: any;
  marketId: string;
  title: string;
  info: string;
  game_over: number;
  result: string;
  bet_allowed: number;
  suspended: number;
  ball_running: number;
  status: {
    text: string;
    cssClass: string;
  };
  tNum: number;
}

@Component({
  selector: 'app-fancy',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, GameOverService, NgxSpinnerService]
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  frmUpload: FormGroup;
  page = {start: 1, end: 5};
  user: any;
  aList: ListManage[] = [];
  rList: ListOfRunner[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;
  id: string;
  sportId: string;
  eventName: string;
  setUrl: any;
  arrayBuffer: any;
  fileData: any = [];
  file: File;
  fileError = '';
  errorMsg = '';
  pageLoad: string;

  dtOptions1: DataTables.Settings = {};
  dtTrigger: Subject<ListManage> = new Subject();

  dataTable: any;
  dtOptions: any;
  tableData = [];

  @ViewChild('dataTable', {static: false}) table;

  constructor(private service: MarketService, private formBuilder: FormBuilder,
              private service2: GameOverService,
              private loadScript: ScriptLoaderService,
              private route: ActivatedRoute, private spinner: NgxSpinnerService,
              private toastr: ToastrService,
              private dataServices: DataService,
              private renderer: Renderer2,
              private router: Router,
              private elRef: ElementRef

  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.sportId = this.route.snapshot.params.sid;
    this.applyFilters();
    this.eventName = window.sessionStorage.getItem('title');
    this.setUrl = this.dataServices.getSportMarketPath(this.sportId);
    this.frmUpload = this.formBuilder.group({
      eid: [this.id, Validators.required],
      sid: [this.sportId, Validators.required],
      data: [''],
    });
  }

  ngAfterViewInit() {
    document.querySelector('body').addEventListener('click', (event) => {
      const target: any = event.target; // Cast EventTarget into an Element
      if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'info') {
          this.changetitle(data.eid, data.marketid, data.title, data.info);
        } else if (data.type === 'status') {
          this.changeStatus(data.eid, data.marketid, data.title, data.info, data.status);
        } else if (data.type === 'game-over') {
          // eid, mId, title, markettype, sid
          this.gameOver(data.eid, data.marketid, data.title, data.mtype, data.sport);
        } else if (data.type === 'game-recall') {
          this.gameRecall(data.marketid, data.eid, data.title);
        } else if (data.type === 'abundant') {
          this.abundant(data.eid, data.marketid, data.mtype, data.sport, data.title);
        } else if (data.type === '') {
          this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/fancy']);
         // this.router.navigate([]).then(result => {
         //   window.open('/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/fancy', '_blank');
         // });
        }
      } else  if (target.tagName.toLowerCase() === 'i' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'info') {
          this.changetitle(data.eid, data.marketid, data.title, data.info);
        }
      } else if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('linksClicks')) {
        const data = target.dataset;
        this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/fancy']);
       /* this.router.navigate([]).then(result => {
          window.open('/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/fancy', '_blank');
        });*/

      }
    });
  }

  onSuccess1(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.tableData = [];

      if (res.data !== 'null') {
        this.aList = res.data;
        this.rList = res.runners;
      } else {
        this.isEmpty = true;
      }
      this.spinner.hide();
      this.tableData = res.data;

      this.initDataTableData();
      // this.bindEvents();

      this.spinner.hide();
    } else {
      this.spinner.hide();
      this.errorMsg = res.success.message;
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.DataTable(this.dtOptions);
    }
  }

  initDataTableData() {
    // console.log('initDataTableData');
    this.dtOptions = {
      destroy: true,
      data: this.tableData,
      pagingType: 'full_numbers',
      pageLength: 25,
      processing: true,
      ordering:  false,
      // dom: 'Bfrtip',
      // buttons: [
      //   'copy', 'csv', 'excel', 'print'
      // ],
      columns: [
        {title: 'title', data: 'title'},
        {title: 'rule', data: 'info'},
        {
          title: 'Action',
          render: (data: any, type: any, full: any) => {
            // console.log('fl', full);
            let htm = '';
            if (full.game_over === 0) {
              let cls = 'btn-danger';
              let status = 'Inactive';
              if (full.status === 1) {
                cls = 'btn-success';
                status = 'Active';
              }
              htm = '<a  class="btn btn-xs bg-success text-light px-1 mr-1 " data-type="info" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-info="' + full.info + '"><i class="mdi mdi-square-edit-outline modalClicks" data-type="info" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-info="' + full.info + '"></i></a>';
              htm += '<a   data-type="status" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-status="' + full.status + '" class="btn btn-xs btn-dark text-light ' + cls + ' success mr-1 modalClicks">' + status + '</a>';
              htm += '<a class="btn btn-xs btn-dark text-light mr-1 linksClicks" data-eid="' + full.eid + '" data-sport="' + this.sportId + '" data-marketid="' + full.marketId + '">Setting</a>';
            }
            return htm;
          }
        },
        {
          title: 'Result',
          render: (data: any, type: any, full: any) => {
            let htm = '';
            if (full.game_over === 0 && this.user.role !== 7 ) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="game-over" data-eid="' + full.eid + '" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-title="' + full.title + '" data-mtype="fancy" data-marketid="' + full.marketId + '">Game Over</a>';
              htm += '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="abundant" data-eid="' + full.eid + '" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-title="' + full.title + '" data-mtype="fancy" data-marketid="' + full.marketId + '">abundant</a>';
            }

            if (full.game_over === 1 && this.user.role !== 7) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="game-recall" data-eid="' + full.eid + '" data-id="' + this.id + '"  data-title="' + full.title + '" data-marketid="' + full.marketId + '">game recall</a>';
              htm += '<a class="btn btn-xs border border-dark text-dark text-uppercase">result : ' + full.result + '</a>';
            }

            if (full.game_over === 1 && this.user.role === 7) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase">result : ' + full.result + '</a>';
            }

            return htm;
          }
        }
      ]
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }

  filterById(): void {
    this.dataTable.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  ngOnDestroy(): void {
    this.aList = [];
    window.localStorage.removeItem('title');
  }

  applyFilters() {
    this.spinner.show();
    this.service.fancyListLoad(this.id, this.sportId).subscribe((res) => this.onSuccess1(res));
  }

  onSuccess(res) {
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      const items = res.data;
      if (items.length) {
        this.aList = res.data;
      } else {
        this.isEmpty = true;
      }

      this.page.end = this.page.end + items.length - 1;
      this.loadScript.load('script', 'assets/js/datatables.init.js');
      this.spinner.hide();
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    } else {
      this.spinner.hide();
      this.errorMsg = res.success.message;
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }

  refreshFancy(eid, sid) {
    swal.fire({
      title: 'Confirm Refresh Market?',
      text: 'You won`t be able to recover this entry!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.addFancy();
        }
      });
  }

  addFancy() {
    this.service.fancyAdd(this.id, this.sportId).subscribe((res) => this.onChangeStatusSuccess(res));
  }


  changeStatus(eid, marketid, title, info, status) {

   // this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (status === '1') {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + title + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus(marketid, status);
        }
      });
  }

  initStatus(marketid, status) {
    this.service.fancystatus(marketid, status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }

  changetitle(eventid, mId, title, info) {
    swal.fire({
      //  title: 'Multiple inputs',
      html:
        '<input id="title" readonly class="swal2-input" value="' + title + '" placeholder="Title">' +
        '<input id="rules" class="swal2-input" value="' + info + '" placeholder="Rules">',
      preConfirm: () => {
        return new Promise((resolve) => {
          resolve([
            $('#title').val(),
            $('#rules').val()
          ]);
        });
      },
      showCancelButton: true,
      confirmButtonText: 'Save',
      onOpen: () => {
        $('#title').focus();
      }
    }).then((result) => {
      const data = {eid: eventid, mid: mId, title: result.value[0], info: result.value[1]};
      this.service.changefancytitle(data).subscribe((res) => this.onChangeStatusSuccess(res));
    });

  }


  gameOver(eid, mId, title, markettype, sid) {
    swal.fire({
      title: 'Confirm Game Over?',
      text: title,
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off',
        placeholder: 'Winner Result',
        type: 'number',
        pattern: '[0-9]*',
      },
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid, mId, result, markettype, sid);
        }
      });
  }

  initGameOver(id, mId, results, markettype, sid) {
    const data = {eventId: id, marketId: mId, winResult: results.value, mType: markettype, sportId: sid};
    this.service2.gameOverManualFancy(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, eid, title) {
    swal.fire({
      title: 'Confirm Game Recall?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    }).then((result) => {
      if (result.value) {
        this.initGameRecall(id, eid);
      }
    });
  }

  initGameRecall(id, eid) {
    const data = {marketId: id, eventId: eid, mType: 'fancy'};
    this.service2.gameRecallFancy(data).subscribe((res) => this.onGameOver(res));
  }


  abundant(eid, mid, type, sid, title) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    }).then((result) => {
      if (result.value) {
        this.initGameAbundant(mid, eid);
      }
    });
  }

  initGameAbundant(mid, eid) {
    const data = {marketId: mid, eventId: eid, mType: 'fancy'};
    this.service2.abundantFancy(data).subscribe((res) => this.onGameOver(res));
  }


  incomingfile(event) {
    this.file = event.target.files[0];
    this.Upload();
  }

  Upload() {
    const fileData = [];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      // tslint:disable-next-line:variable-name
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
      this.fileData.push(XLSX.utils.sheet_to_json(worksheet, {raw: true}));
    };
    fileReader.readAsArrayBuffer(this.file);

  }


  submitForm() {
    if (this.fileData.length > 0) {
      this.frmUpload.patchValue({
        data: this.fileData[0] ? this.fileData[0] : this.fileData
      });
      /// console.log(this.frmUpload.value);
      if (this.frmUpload.valid) {
        this.errorMsg = '';
        this.service.fancy2import(this.frmUpload.value).subscribe((res) => this.onSuccessJack(res));
      }
    }
  }

  onSuccessJack(res) {
    if (res.status === 1) {
      this.fileData = [];
      this.frmUpload.reset();
      $('#import-files').modal('hide');
      this.applyFilters();
    } else {
      this.errorMsg = res.error.message;
    }
  }
}
