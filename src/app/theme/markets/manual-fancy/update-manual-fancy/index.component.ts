import {Component, ViewEncapsulation, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';
import swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthIdentityService} from '../../../../_services';

declare const $: any;

@Component({
  selector: 'app-update-manual-fancy',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MarketService]
})
export class UpdateManualFancyComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('diff', {static: false}) diff: ElementRef;
  frm: FormGroup;
  eid: string;
  sportId: string;
  mid: string;
  // tslint:disable-next-line:variable-name
  event_title: string;
  aList: any = [];
  cItem: string;
  fList: any = [];
  betList: any = [];
  maxLoss: string;
  // tslint:disable-next-line:ban-types
  diffrance: Number = 1;
  // tslint:disable-next-line:ban-types
  ldiffrance: Number = 0;
  rateArr: any = [];
  ballrunning = false;
  suspend = false;
  lastPrice: string;
  lastYesRate: string;
  lastNoRate: string;
  fancyMarketBookData: any = [];
  timeleft = 60;
  sesstionTimeOut: string;
  stime = false;
  tmp: number;
  downloadTimer11: any;
  downloadTimer: any;
  betListFlag = true;
  public interval1000: any;
  public interval300: any;
  cNoPrice: string;
  cYesPrice: string;
  cNoRate: string;
  cYesRate: string;
  cBallRunning: number;
  cSuspended: number;
  marketStatus: string;
  timerSubscriptionBetList: any;
  user: any;
  // aList: UpdateManualFancyManage[] = [];
  // cItem: UpdateManualFancyManage;
  isEmpty = false;
  activity = 1;
  isDestroy: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
    // this.frm = new FormGroup({
    //   no_val: new FormControl(),
    //   yes_val: new FormControl(),
    //   no_rate: new FormControl(),
    //   yes_rate: new FormControl(),
    //   eid: new FormControl(),
    //   sid: new FormControl(),
    //   mid: new FormControl()
    // });
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.mid = this.route.snapshot.params.mid;
    this.getSessionData();
    this.getBetData();
    // console.log(this.eid + ' ' + this.sportId);
    // this.diff.nativeElement.value = 1;
  }


  getSessionData() {

    this.service.fancy2MarketLoad(this.mid, this.activity).subscribe((res) => {
      this.activity = 2;
      this.intSessionData(res);
    });
  }

  intSessionData(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      // this.betList = res.data.betList;
      this.mid = res.data.items.market_id;
      this.event_title = res.data.title;
      this.sesstionTimeOut = res.data.items.sessionSuspendedTime;
      this.rateArr = res.data.rateOption;

      this.marketStatus = this.fList.status;
    }
  }

  getBetData() {
    this.service.fancybetList(this.mid).subscribe((res) => {
      this.intBetData(res);
      if (!this.isDestroy) {
        const xhmBetData = setTimeout(() => {
          clearTimeout(xhmBetData);
          this.getBetData();
        }, 3000);
      }
    });
  }

  intBetData(res) {
    if (res !== 1) {
      this.betList = res.data.items;
    }
  }

  ngOnDestroy() {
    this.isDestroy = true;
    this.betList = [];
    this.fList = [];
    if (this.interval1000) {
      clearInterval(this.interval1000);
      clearInterval(this.interval300);
    }

    if (this.timerSubscriptionBetList) {
      this.timerSubscriptionBetList.unsubscribe();
    }

  }

  getFormData() {

    // console.log(this.frm);
    // alert(this.frm.value.no_val);
    if (this.frm.value.no_val === '0' || this.frm.value.no_val === '00' || this.frm.value.no_val === '000' || this.frm.value.no_val === '0000' || this.frm.value.no_val === '00000' || this.frm.value.no_val === '000000' || this.frm.value.no_val === '') {
      this.ballrunning = true;
      this.cBallRunning = 1;
      /*  this.frm.patchValue({
          no_val: 0,
        });*/
      this.lastPrice = '';

      const data1 = {ball_running: true, suspended: false, mid: this.mid, sid: this.sportId, event_name: this.event_title};
      if (data1) {
        $('#no_run').val('');
        this.service.update(data1).subscribe((res) => this.onSuccess(res));
      }
      if (this.tmp === 0) {
        this.tmp = 1;
      }
      clearInterval(this.downloadTimer11);
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }

    } else {
      const elemnt1: any = document.getElementById('no_run') as HTMLElement;
      elemnt1.select();

      this.ballrunning = false;
      this.frm.patchValue({
        yes_val: Number(this.frm.value.no_val) + Number(this.diffrance)
      });
      this.lastPrice = this.frm.value.no_val;
      this.lastYesRate = this.frm.value.yes_rate;
      this.lastNoRate = this.frm.value.no_rate;

      this.cBallRunning = 0;
      this.cSuspended = 0;
      this.cYesPrice = this.frm.value.yes_val;
      this.cNoPrice = this.frm.value.no_val;
      this.cNoRate = this.frm.value.no_rate;
      this.cYesRate = this.frm.value.yes_rate;

      const data = this.frm.value;
      data.eid = this.eid;
      data.mid = this.mid;
      data.sid = this.sportId;
      data.event_name = this.event_title;
      data.ball_running = false;
      data.suspended = false;

      if (this.frm.valid) {
        this.service.update(data).subscribe((res) => this.onSuccess(res));
      }

      if (this.tmp === undefined) {
        this.tmp = 0;
      } else {
        clearInterval(this.downloadTimer11);
      }
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }
    }
  }

  setSession() {
    let downloadTimer;
    if (this.tmp === 1) {
      clearInterval(downloadTimer);
    }
    let timeleft = parseInt(this.sesstionTimeOut, 10);
    const elemnt: any = document.getElementById('countdowntimer') as HTMLElement;
    // console.log(elemnt);
    elemnt.textContent = timeleft + 's';
    downloadTimer = setInterval(() => {
      timeleft--;
      const nn = timeleft >= 10 ? timeleft : '0' + timeleft;
      elemnt.textContent = nn + 's';
      if (timeleft <= 0) {
        clearInterval(downloadTimer);
      }
      if (timeleft === 0) {
        const elemnt1: any = document.getElementById('suspended') as HTMLElement;
        elemnt1.click();
      }
    }, 1000);
    this.downloadTimer11 = downloadTimer;

    if (timeleft === 0) {
      // debugger;
      const data1 = {ball_running: false, suspended: true, mid: this.mid, sid: this.sportId, event_name: this.event_title};
      this.service.update(data1).subscribe((res) => this.onSuccess(res));
    }

  }


  createForm() {
    this.frm = this.formBuilder.group({
      no_val: ['', Validators.required],
      yes_val: ['', Validators.required],
      no_rate: ['100', Validators.required],
      yes_rate: ['100', Validators.required]
    });
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      //  this.betList = res.data.betList;
    }
  }

  suspended() {
    const data1 = {ball_running: false, suspended: true, mid: this.mid, sid: this.sportId, event_name: this.event_title};
    this.cSuspended = 1;
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    if (data1) {
      this.service.update(data1).subscribe((res) => this.onSuccess(res));
    }
  }


  getBookDataFancyNew() {
    this.spinner.show();
    const data = {userId: 1, marketId: this.mid};
    this.service.getBookDataFancyNew(data).subscribe((res) => this.onSuccessBookDataFancyNew(res));
  }

  onSuccessBookDataFancyNew(res) {
    if (res.status === 1) {
      let i = 0;
      const min = res.data.min;
      const max = res.data.max;
      const dataReturn = [];
      for (i = min; i <= max; i++) {
        const betlist = res.data.betList;
        let total = 0;
        let winVal1 = 0;
        let winVal2 = 0;
        let lossVal1 = 0;
        let lossVal2 = 0;
        if (res.data.userRole !== 4) {
          // tslint:disable-next-line:only-arrow-functions
          betlist.forEach(function(bets) {
            if (bets.bType === 'no' && bets.price > i) {
              winVal1 = winVal1 + (((bets.win) * (bets.apl) / 100));
            } else if (bets.bType === 'yes' && bets.price <= i) {
              winVal2 = winVal2 + (((bets.win) * (bets.apl) / 100));
            } else if (bets.bType === 'no' && bets.price <= i) {
              lossVal1 = lossVal1 + (((bets.loss) * (bets.apl) / 100));
            } else if (bets.bType === 'yes' && bets.price > i) {
              lossVal2 = lossVal2 + (((bets.loss) * (bets.apl) / 100));
            }
          });
          total = (lossVal1 + lossVal2) - (winVal1 + winVal2);
        } else {
          // tslint:disable-next-line:only-arrow-functions
          betlist.forEach(function(bets) {
            if (bets.bType === 'no' && bets.price > i) {
              winVal1 = winVal1 + bets.win;
            } else if (bets.bType === 'yes' && bets.price <= i) {
              winVal2 = winVal2 + bets.win;
            } else if (bets.bType === 'no' && bets.price <= i) {
              lossVal1 = lossVal1 + bets.loss;
            } else if (bets.bType === 'yes' && bets.price > i) {
              lossVal2 = lossVal2 + bets.loss;
            }
          });
          total = (winVal1 + winVal2) - (lossVal1 + lossVal2);
        }

        dataReturn.push({price: i, profitLoss: Math.round(total)});
      }

      const bookDataFancyNew = [];
      if (dataReturn.length) {
        let priceVal;
        let i = 0;
        let start = 0;
        let startPl = 0;
        let end = 0;

        // tslint:disable-next-line:only-arrow-functions
        dataReturn.forEach(function(d, index) {
          if (index === 0) {
            bookDataFancyNew.push({price: d.price + ' or less', profitLoss: d.profitLoss});
          } else {
            if (startPl !== d.profitLoss) {
              if (end !== 0) {
                if (start === end) {
                  priceVal = start;
                } else {
                  priceVal = start + ' - ' + end;
                }
                bookDataFancyNew.push({price: priceVal, profitLoss: startPl});
              }

              start = d.price;
              end = d.price;

            } else {
              end = d.price;
            }

            if (index === (dataReturn.length - 1)) {
              bookDataFancyNew.push({price: start + ' or more', profitLoss: startPl});
            }

          }

          startPl = d.profitLoss;
          i++;
        });
      }
      $('#bookModal').modal();
      this.fancyMarketBookData = bookDataFancyNew;
    }
    this.spinner.hide();
  }


  marketBook() {
    const data1 = {mid: this.mid, sid: this.sportId, eid: this.eid, m_type: 'fancy2'};
    if (data1) {
      this.service.marketBook(data1).subscribe((res) => this.onSuccessBook(res));
    }
  }

  onSuccessBook(res) {

    // console.log(res);
    $('#bookModal').modal();
    if (res.status === 1) {

      this.fancyMarketBookData = [];
      res.data.forEach(item => {
        this.fancyMarketBookData.push(item);
      });
    }
  }

  closeModal(id: string) {
    $('#' + id).toggleClass('show');
    $('body').removeClass('modal-open');
    clearInterval(this.interval1000);
  }

  /* getFormData() {
     this.frm.patchValue({
       eid: this.eid, sid: this.sportId
     });
     const data = this.frm.value;
     return data;
   } */

  ngAfterViewInit() {
    const nthis = this;
    // tslint:disable-next-line:only-arrow-functions
    document.onkeyup = function(e) {
      if (e.shiftKey && (e.which === 192)) { // sft+0
        nthis.diffrance = 0;
      } else if (e.shiftKey && (e.which === 49 || e.which === 97)) { // sft+1
        nthis.diffrance = 1;
      } else if (e.shiftKey && (e.which === 50 || e.which === 98)) { // sft+2
        nthis.diffrance = 2;
      } else if (e.shiftKey && (e.which === 51 || e.which === 99)) { // sft+3
        nthis.diffrance = 3;
      } else if (e.shiftKey && e.which === 65) { // sft+a
        nthis.diffrance = 1;
        nthis.frm.patchValue({no_rate: '100', yes_rate: '100'});
      } else if (e.shiftKey && e.which === 83) { // sft+s
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '110', yes_rate: '90'});
      } else if (e.shiftKey && e.which === 90) { // sft+z
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '115', yes_rate: '85'});
      } else if (e.shiftKey && e.which === 88) { // sft+x
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '120', yes_rate: '80'});
      } else if (e.shiftKey && e.which === 68) { // sft+d
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '120', yes_rate: '90'});
      } else if (e.shiftKey && e.which === 67) { // sft+c
        nthis.diffrance = 0;
        nthis.frm.patchValue({no_rate: '125', yes_rate: '75'});
      }
    };


  }


  get frmNoVal() {
    return this.frm.get('no_val');
  }

  get frmYesVal() {
    return this.frm.get('yes_val');
  }

  get frmNoRate() {
    return this.frm.get('no_rate');
  }

  get frmYesRate() {
    return this.frm.get('yes_rate');
  }


  limit(element) {
    // tslint:disable-next-line:variable-name
    const max_chars = 3;
    if (!this.validate(element.target.value)) {
      this.frm.patchValue({
        no_val: element.target.value.substr(0, element.target.value.length - 1)
      });
    }

    if (element.target.value.length > max_chars) {
      if (this.validate(element.target.value)) {
        this.frm.patchValue({
          no_val: element.target.value.substr(0, max_chars)
        });
      }
    }
  }

  setDiff() {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
  }


  diffCheck(element) {
    // debugger;
    if (!this.validate(element.target.value)) {
      this.diffrance = element.target.value.substr(0, element.target.value.length - 1);
      //  alert(this.diffrance);
    }
  }

  validate(s) {
    // var rgx = /^[0-9]*\.?[0-9]*$/;
    const rgx = /^\d+$/;
    return s.match(rgx);
  }

  addRate(rate: string) {
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();

    const newRate = rate.split('/');
    // this.ldiffrance =  this.diffrance;
    this.diffrance = 0;
    if (newRate[0] === '100' && newRate[1] === '100') {
      this.diffrance = 1;
    }
    this.frm.patchValue({
      yes_rate: newRate[0],
      no_rate: newRate[1]
    });
  }

  changeStatus(id) {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    swal.fire({
      title: 'Confirm Change Status?',
      text: 'You won\'t be able to recover this entry!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    }).then((result) => {
      if (result.value) {
        this.initStatus(id);
      }
    });
  }

  initStatus(id) {
    const data = {id};
    this.service.status(this.mid, data).subscribe((res) => this.onManualSessionChangeStatusSuccess(res));
  }

  onManualSessionChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.getSessionData();
    }
  }


}
