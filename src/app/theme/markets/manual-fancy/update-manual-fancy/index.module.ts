import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateManualFancyComponent } from './index.component';
import {NgxSpinnerModule} from 'ngx-spinner';

const routes: Routes = [
  {
    path: '',
    component: UpdateManualFancyComponent
  },
  {
    path: 'update-manual-fancy/:eid/:sid/:mid',
    component: UpdateManualFancyComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateManualFancyComponent
  ]
})
export class UpdateManualFancyModule {

}
