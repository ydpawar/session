import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ManualFancyComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateManualFancyComponent} from './create-manual-fancy/index.component';
import {CreateManualFancyModule} from './create-manual-fancy/index.module';

import {UpdateManualFancyComponent} from './update-manual-fancy/index.component';
import {UpdateManualFancyModule} from './update-manual-fancy/index.module';
const routes: Routes = [
  {
    path: '',
    component: ManualFancyComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-manual-fancy/:eid/:sid',
        component: CreateManualFancyComponent
      },
      {
        path: 'update-manual-fancy/:eid/:sid',
        component: UpdateManualFancyComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateManualFancyModule, UpdateManualFancyModule
  ], exports: [
    RouterModule
  ], declarations: [ ManualFancyComponent ]
})
export class ManualFancyModule {

}
