import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { UpdateOddEvenComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateOddEvenComponent
  },
  {
    path: 'update-odd-even/:eid/:sid/:mid',
    component: UpdateOddEvenComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateOddEvenComponent
  ]
})
export class UpdateOddEvenModule {

}
