import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateOddEvenComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateOddEvenComponent
  },
  {
    path: 'create-odd-even/:eid/:sid',
    component: CreateOddEvenComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateOddEvenComponent
  ]
})
export class CreateOddEvenModule {

}
