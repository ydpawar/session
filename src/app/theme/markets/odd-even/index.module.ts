import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OddEvenComponent } from './index.component';
import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateOddEvenComponent} from './create-odd-even/index.component';
import {CreateOddEvenModule} from './create-odd-even/index.module';
import {UpdateOddEvenComponent} from './update-odd-even/index.component';
import {UpdateOddEvenModule} from './update-odd-even/index.module';
const routes: Routes = [
  {
    path: '',
    component: OddEvenComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-odd-even/:eid/:sid',
        component: CreateOddEvenComponent
      },
      {
        path: 'update-odd-even/:eid/:sid',
        component: UpdateOddEvenComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateOddEvenModule, UpdateOddEvenModule
  ], exports: [
    RouterModule
  ], declarations: [ OddEvenComponent ]
})
export class OddEvenModule {

}
