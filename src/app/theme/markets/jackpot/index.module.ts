import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JackpotComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';

import {CreateJackpotGameComponent} from './create-jackpotgame/index.component';
import {CreateJackpotGameModule} from './create-jackpotgame/index.module';
import {UpdateJackpotGameComponent} from './update-jackpotgame/index.component';
import {UpdateJackpotGameModule} from './update-jackpotgame/index.module';
import {JackpotDataModule} from './jackpot-data/index.module';
import {JackpotDataComponent} from './jackpot-data/index.component';
import {AddRunnersJackpotModule} from './add-runners/index.module';
import {AddRunnersJackpotComponent} from './add-runners/index.component';
import {BetListModule} from './bet-list/index.module';
import {BetListComponent} from './bet-list/index.component';

const routes: Routes = [
  {
    path: '',
    component: JackpotComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: ':id/jackpot-data/:iid',
        component: JackpotDataComponent
      },
      {
        path: ':id/bet-list/:iid',
        component: BetListComponent
      },
      {
        path: ':id/add-runners/:iid',
        component: AddRunnersJackpotComponent
      },
      {
        path: 'create-jackpotgame/:eid/:sid',
        component: CreateJackpotGameComponent
      },
      {
        path: 'update-jackpotgame/:eid/:sid',
        component: UpdateJackpotGameComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateJackpotGameModule, UpdateJackpotGameModule, JackpotDataModule, AddRunnersJackpotModule, BetListModule
  ], exports: [
    RouterModule
  ], declarations: [ JackpotComponent ]
})
export class JackpotModule {

}
