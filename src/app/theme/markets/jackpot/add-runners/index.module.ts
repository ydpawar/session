import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddRunnersJackpotComponent } from './index.component';

const routes: Routes = [
  {
    path: ':id/add-runners/:iid',
    component: AddRunnersJackpotComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    AddRunnersJackpotComponent
  ]
})
export class AddRunnersJackpotModule {

}
