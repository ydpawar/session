import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';

@Component({
  selector: 'app-create-jackpot-runners',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService]
})
export class AddRunnersJackpotComponent implements OnInit {
  frm: FormGroup;
  CList: any;
  id: any;
  eid: string;
  mid: any;
  sportId: string;
  runner1: string;
  runner2: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.id = this.route.snapshot.params.id;
    this.mid = this.route.snapshot.params.iid;
    this.sportId = this.route.snapshot.params.sid;
    this.getRunner();
  }

  getRunner() {
    this.service.jackpotEventRunners(this.id).subscribe((res) => this.onRunnerSuccess(res));
  }

  onRunnerSuccess(res) {
    if (res) {
      this.CList = res.data;
      this.runner1 = res.data.runner.runner1;
      this.runner2 = res.data.runner.runner2;
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      team_a: ['', Validators.required],
      team_a_player: ['', Validators.required],
      team_b: ['', Validators.required],
      team_b_player: ['', Validators.required],
      rate: ['', Validators.required],
      eid: [''],
      sid: [''],
      mid: ['']
    });
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.addJackpotRunner(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/markets/jackpot/' + this.id + '/jackpot-data/' + this.mid]);
    }
  }

  getFormData() {
    this.frm.patchValue({
      eid: this.eid, sid: this.sportId, mid: this.mid
    });
    const data = this.frm.value;
    return data;
  }

  get frmTeamA() {
    return this.frm.get('team_a');
  }

  get frmrunnerA() {
    return this.frm.get('team_a_player');
  }

  get frmTeamB() {
    return this.frm.get('team_b');
  }

  get frmrunnerB() {
    return this.frm.get('team_b_player');
  }

  get frmrate() {
    return this.frm.get('rate');
  }
}
