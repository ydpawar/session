import {Component, OnInit, AfterViewInit} from '@angular/core';
import {GameOverService, MarketService} from '../../../../_api/index';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import * as XLSX from 'ts-xlsx';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services';

declare var $;

export interface BetListManage {
  sportId: string;
  name: string;
  type: string;
  title: string;
  suspended: string;
  game_over: string;
  result: string;
  eventtitle: string;
  time: string;
  secId: any;
  mid: any;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-bet-list',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService, GameOverService]
})
export class BetListComponent implements OnInit, AfterViewInit {
  page = {start: 1, end: 5};
  frmUpload: FormGroup;
  aList: BetListManage[] = [];
  aListData: BetListManage[] = [];
  cItem: BetListManage;
  isEmpty = false;
  isLoading = false;
  id: any;
  sportId: string;
  eid: string;
  mid: string;
  type: string;
  user: any;
  arrayBuffer: any;
  file: File;
  fileData: any = [];
  fileError = '';
  editRowIndex = -1;
  isEditRow = false;
  editRowData: any = {};
  errorMsg = '';
  title: string;
  eventtitle: string;
  suspended: string;
  result: string;
  // tslint:disable-next-line:variable-name
  game_over: string;

  constructor(private service: MarketService,
              private loadScript: ScriptLoaderService,  private service2: GameOverService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder, private spinner: NgxSpinnerService
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.mid = this.route.snapshot.params.iid;
    this.eid = this.route.snapshot.params.id;
    this.applyFilters();

    this.frmUpload = this.formBuilder.group({
      eid: [this.eid, Validators.required],
      mid: [this.mid, Validators.required],
      data: [''],
    });

  }

  ngAfterViewInit() {
  }

  applyFilters() {
    this.spinner.show();
    const data = this.mid;
    this.service.jackpotbetList(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      const items = res.data.item;
      this.aListData = items.event;
      if (items.bet_list.length > 0) {
        this.aList = items.bet_list;
        this.eventtitle = items.event.title;
      } else {
        this.aList = [];
        this.isEmpty = true;
      }

      setTimeout(() => {
        this.spinner.hide();
        this.loadScript.load('script', 'assets/js/datatables.init.js');
      }, 500);

    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  /**
   * Open setting modal
   * @param ID modal id
   */
  setting_modal(ID) {
    $('#' + ID).modal();
    $('body').removeClass('modal-open');
  }


  incomingfile(event) {
    this.file = event.target.files[0];
    this.Upload();
  }

  Upload() {
    const fileData = [];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      // tslint:disable-next-line:variable-name
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
      this.fileData.push(XLSX.utils.sheet_to_json(worksheet, {raw: true}));
    };
    fileReader.readAsArrayBuffer(this.file);

  }


  submitForm() {
    if (this.fileData.length > 0) {
      this.frmUpload.patchValue({
        data: this.fileData[0] ? this.fileData[0] : this.fileData
      });
      if (this.frmUpload.valid) {
        this.errorMsg = '';
        this.service.jackpotimport(this.frmUpload.value).subscribe((res) => this.onSuccessJack(res));
      }
    }
  }

  onSuccessJack(res) {
    if (res.status === 1) {
      this.fileData = [];
      this.frmUpload.reset();
      $('#import-files').modal('hide');
      this.applyFilters();
    } else {
      this.errorMsg = res.error.message;
    }
  }

}
