import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MarketService } from '../../../../_api/index';

@Component({
  selector: 'app-create-jackpot',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService]
})
export class CreateJackpotGameComponent implements OnInit, OnDestroy {
  frm: FormGroup;
  CList: any;
  eid: string;
  sportId: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getCategory();
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;

  }

  ngOnDestroy(): void {
    this.CList = [];
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: ['', Validators.required],
      type: ['', Validators.required],
      eid: [''],
      sid: ['']
    });
  }

  getCategory() {
      this.service.getgametype().subscribe((res) => this.onRcategorySuccess(res));
  }
  onRcategorySuccess(res) {
    if (res) {
      this.CList = res.data;
    }
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.createjackpotgame(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/markets/jackpot/' + this.eid + '/' + this.sportId]);
    }
  }

  getFormData() {
    this.frm.patchValue({
      eid: this.eid, sid: this.sportId
    });
    const data = this.frm.value;
    return data;
  }

  get frmtitle() { return this.frm.get('title'); }
  get frmtype() { return this.frm.get('type'); }

}
