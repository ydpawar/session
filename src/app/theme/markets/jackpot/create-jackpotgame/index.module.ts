import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CreateJackpotGameComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateJackpotGameComponent
  },
  {
    path: 'create-jackpotgame/:eid/:sid',
    component: CreateJackpotGameComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateJackpotGameComponent
  ]
})
export class CreateJackpotGameModule {

}
