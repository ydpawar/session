import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JackpotDataComponent } from './index.component';
import { NgxSpinnerModule } from 'ngx-spinner';

const routes: Routes = [
  {
    path: '',
    component: JackpotDataComponent
  },
  {
    path: ':id/jackpot-data/:iid',
    component: JackpotDataComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    JackpotDataComponent
  ]
})
export class JackpotDataModule {

}
