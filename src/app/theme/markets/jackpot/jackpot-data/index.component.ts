import {Component, OnInit, AfterViewInit} from '@angular/core';
import {GameOverService, MarketService} from '../../../../_api/index';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
// @ts-ignore
import * as XLSX from 'ts-xlsx';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService, ToastrService} from '../../../../_services';

declare var $;

export interface JackpotDataManage {
  sportId: string;
  name: string;
  type: string;
  title: string;
  suspended: string;
  game_over: string;
  result: string;
  eventtitle: string;
  time: string;
  secId: any;
  mid: any;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-jackpot-data',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService, GameOverService]
})
export class JackpotDataComponent implements OnInit, AfterViewInit {
  page = {start: 1, end: 5};
  frmUpload: FormGroup;
  aList: JackpotDataManage[] = [];
  aListData: JackpotDataManage[] = [];
  cItem: JackpotDataManage;
  isEmpty = false;
  isLoading = false;
  id: any;
  sportId: string;
  eid: string;
  mid: string;
  type: string;
  user: any;
  arrayBuffer: any;
  file: File;
  fileData: any = [];
  fileError = '';
  editRowIndex = -1;
  isEditRow = false;
  editRowData: any = {};
  errorMsg = '';
  title: string;
  eventtitle: string;
  suspended: string;
  result: string;
  // tslint:disable-next-line:variable-name
  game_over: string;
  pageLoad: string;
  activity = 1;

  constructor(private service: MarketService,
              private loadScript: ScriptLoaderService,  private service2: GameOverService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private toastr: ToastrService
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.mid = this.route.snapshot.params.iid;
    this.eid = this.route.snapshot.params.id;
    this.applyFilters();

    this.frmUpload = this.formBuilder.group({
      eid: [this.eid, Validators.required],
      mid: [this.mid, Validators.required],
      data: [''],
    });

  }

  ngAfterViewInit() {
  }


  applyFilters() {
    this.spinner.show();
    const data = this.mid;
    this.service.jackpotgamerunner(data, this.activity).subscribe((res) => this.onSuccess(res));
    this.activity = 2;

  }

  onSuccess(res) {
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }
    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1) {
      const items = res.data.item;
      this.aListData = items.event;
      if (items.jackpotdata.length > 0) {
        this.aList = items.jackpotdata;
        this.type = items.event.type;
        this.eventtitle = items.event.title;
        this.game_over = items.event.game_over;
        this.result = items.event.result;
      } else {
        this.aList = [];
        this.isEmpty = true;
      }

      setTimeout(() => {
        this.spinner.hide();
        this.loadScript.load('script', 'assets/js/datatables.init.js');
      }, 500);

    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  deleteAll() {
      swal.fire({
        title: 'Confirm Delete All?', // ' + item.name + '
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Sure!'
      })
      .then((result) => {
     // console.log(result);
     if (result.value) {
          this.initdeleteAll();
        }
      });
  }

  initdeleteAll() {
    this.service.jackpotGamedelete(this.mid).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  changetitle(mId, item) {
    swal.fire({
      //  title: 'Multiple inputs',
      html:
        '<label style="float:left">Team A Player</label>' +
        '<input id="team_a_player" class="swal2-input form-control" value="' + item.col_2 + '" placeholder="Team A Player">' +
        '<label style="float:left">Team B Player</label>' +
        '<input id="team_b_player" class="swal2-input form-control" value="' + item.col_4 + '" placeholder="Team B Player">' +
        '<label style="float:left">Rate</label>' +
        '<input id="rate" class="swal2-input form-control" value="' + item.rate + '" placeholder="Rate">',

      preConfirm: () => {
        return new Promise((resolve) => {
          resolve([
            $('#team_a_player').val(),
            $('#team_b_player').val(),
            $('#rate').val()
          ]);
        });
      },
      showCancelButton: true,
      confirmButtonText: 'Save',
      onOpen: () => {
        $('#team_a_player').focus();
      }
    }).then((result) => {
      const data = {mid: item.mid, team_a_player: result.value[0], team_b_player: result.value[1], rate: result.value[2]};
      this.service.changejackpotdata(data, item.mid, item.secId).subscribe((res) => this.onChangeStatusSuccess(res));
    });

  }

  changeSuspendAll() {
    swal.fire({
      title: 'Confirm Suspended All?', // ' + item.name + '
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initSuspendStatus(2);
        }
      });
  }

  changeSuspendStatus(item: JackpotDataManage) {
    this.cItem = item;
    let statustxt = 'Suspended: no';
    // @ts-ignore
    if (item.suspended === 0) {
      statustxt = 'Suspended: yes';
    }

    swal.fire({
      title: 'You want to ' + statustxt + '?', // ' + item.name + '
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initSuspendStatus(1);
        }
      });
  }

  initSuspendStatus(sutype) {
    this.service.jackpotrunnersuspend(this.mid, this.cItem.secId, this.eid, this.type, sutype).subscribe((res) => this.onChangeStatusSuccess(res));
  }


  changeStatus(item: JackpotDataManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + '?', // ' + item.name + '
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.jackpotrunnerstatus(this.mid, this.cItem.secId, this.eid, this.type).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }


  /**
   * Open setting modal
   * @param ID modal id
   */
  setting_modal(ID) {
    $('#' + ID).modal();
    $('body').removeClass('modal-open');
  }


  incomingfile(event) {
    this.file = event.target.files[0];
    this.Upload();
  }

  Upload() {
    const fileData = [];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, {type: 'binary'});
      // tslint:disable-next-line:variable-name
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      // console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
      this.fileData.push(XLSX.utils.sheet_to_json(worksheet, {raw: true}));
    };
    fileReader.readAsArrayBuffer(this.file);

  }


  submitForm() {
    if (this.fileData.length > 0) {
      const data = {data: this.fileData[0] ? this.fileData[0] : this.fileData, eid: this.id, mid: this.mid};
        this.spinner.show();
        this.errorMsg = '';
        this.service.jackpotimport(data).subscribe((res) => this.onSuccessJack(res));

    }
  }

  onSuccessJack(res) {
    if (res.status === 1) {
      this.fileData = [];
      this.frmUpload.reset();
      this.spinner.hide();
      $('#import-files').modal('hide');
      $('#frmUploadXls').trigger("reset");
      this.applyFilters();
    } else {
      this.errorMsg = res.error.message;
    }
  }

  gameOver(eid, mId, secId, title, markettype, sid) {
    swal.fire({
      title: 'Confirm This is Winner?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid, mId, secId, markettype, sid);
        }
      });

  }

  initGameOver(id, mId, secId, markettype, sid) {
    const data = {eventId: id, marketId: mId, winResult: secId, mType: markettype, sportId: sid, gameid: this.type};
    // console.log(data);
    this.service2.gameOverJackpot(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }
  gameRecall(eid, mId, secId, title, markettype, sid) {
    swal.fire({
      title: 'Confirm Game Recall?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameRecall(mId, eid, sid);
        }
      });
  }

  initGameRecall(id, eid, sid) {
    const data = {marketId: id, eventId: eid, mType: 'jackpot', sportId: sid, gameid: this.type};
    this.service2.gameRecallJackpot(data).subscribe((res) => this.onGameOver(res));
  }


  abundant(eid, mid, type, sid, title) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameAbundant(mid, eid);
        }
      });
  }

  initGameAbundant(mid, eid) {
    const data = { marketId: mid, eventId: eid, mType: 'jackpot', gameid: this.type};
    this.service2.abundantjackpot(data).subscribe((res) => this.onGameOver(res));
  }

}
