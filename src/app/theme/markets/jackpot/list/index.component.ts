import {Component, OnInit, AfterViewInit} from '@angular/core';
import {MarketService} from '../../../../_api/index';
import {ActivatedRoute} from '@angular/router';
import swal from 'sweetalert2';
import {DataService, ScriptLoaderService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';
import {FormGroup, FormBuilder} from '@angular/forms';
import {ToastrService} from './../../../../_services/toastr.service';

declare const $: any;

interface ListManage {
  id: string;
  eid: string;
  sportId: string;
  title: string;
  type: string;
  time: string;
  marketId: string;
  game_over: string;
  status: {
    text: string;
    cssClass: string;
  };
}

@Component({
  selector: 'app-jackpot',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService]
})
export class ListComponent implements OnInit, AfterViewInit {
  page = {start: 1, end: 5};
  frm: FormGroup;
  aList: ListManage[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;
  id: string;
  sportId: string;
  eventName: string;
  setUrl: any;

  constructor(private service: MarketService,
              private loadScript: ScriptLoaderService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private dataServices: DataService
  ) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.sportId = this.route.snapshot.params.sid;
    this.applyFilters();
    this.eventName = window.sessionStorage.getItem('title');
    this.setUrl = this.dataServices.getSportMarketPath(this.sportId);
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    this.spinner.show();
    const data = this.id;
    this.service.jackpotgameList(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      const items = res.data.items;
      if (items.length) {
        this.aList = res.data.items;
      } else {
        this.isEmpty = true;
      }

      this.page.end = this.page.end + items.length - 1;
      this.loadScript.load('script', 'assets/js/datatables.init.js');
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
      // this.config.totalItems = res.data.count;
    } else {
      this.spinner.hide();
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  changemarkettitle(item, secId) {
    swal.fire({
      html:
        '<label style="float:left">Title</label>' +
        '<input id="title" class="swal2-input form-control" value="' + item.title + '" placeholder="Rate">',

      preConfirm: () => {
        return new Promise((resolve) => {
          resolve([
            $('#title').val(),

          ]);
        });
      },
      showCancelButton: true,
      confirmButtonText: 'Save',
      onOpen: () => {
        $('#title').focus();
      }
    }).then((result) => {
      const data = {title: result.value[0]};
      this.service.changejackpotmarkettitle(data, item.marketId).subscribe((res) => this.onChangeStatusSuccess(res));
    });
  }

  changeStatus(item: ListManage) {
    this.cItem = item;
    let statustxt = 'Active';
    // @ts-ignore
    if (item.status === 1) {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + item.title + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }

  initStatus() {
    this.service.jackpotmarketstatus(this.cItem.marketId, this.cItem.status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }

}
