import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateJackpotGameComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateJackpotGameComponent
  },
  {
    path: 'update-cricketcasino/:eid/:sid/:mid',
    component: UpdateJackpotGameComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateJackpotGameComponent
  ]
})
export class UpdateJackpotGameModule {

}
