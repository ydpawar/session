import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MeterComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateMeterComponent} from './create-meter/index.component';
import {CreateMeterModule} from './create-meter/index.module';

import {UpdateMeterComponent} from './update-meter/index.component';
import {UpdateMeterModule} from './update-meter/index.module';
const routes: Routes = [
  {
    path: '',
    component: MeterComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-meter/:eid/:sid',
        component: CreateMeterComponent
      },
      {
        path: 'update-meter/:eid/:sid',
        component: UpdateMeterComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateMeterModule, UpdateMeterModule
  ], exports: [
    RouterModule
  ], declarations: [ MeterComponent ]
})
export class MeterModule {

}
