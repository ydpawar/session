import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';
import { NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-create-meter',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService]
})
export class CreateMeterComponent implements OnInit {
  frms: FormGroup;
  eventname: string;
  eid: string;
  sportId: string;
  minstake: any;
  maxstake: any;
  maxprofit: any;
  bet_delay: any;
  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.applyFilters();
  }

  onSuccessRs(res) {
    if (res.status === 1) {
      this.eventname = res.eventname;
    }
  }

  applyFilters() {
    const data = {sid: this.sportId, type: 3, eid: this.eid };
    this.service.geteventName(this.eid).subscribe((res) => this.onSuccessRs(res));
    this.service.getsportMarketSetting(data).subscribe((res) => this.onSuccessResult(res));
  }

  onSuccessResult(res) {
    if (res.status !== undefined && res.status === 1) {
      this.minstake = res.data.min_stake;
      this.maxstake = res.data.max_stake;
      this.maxprofit = res.data.max_profit_limit;
      this.bet_delay = res.data.bet_delay;
      this.frms.patchValue({
        // tslint:disable-next-line:max-line-length
        eid: this.eid, sid: this.sportId, minstake: this.minstake, maxstake: this.maxstake, bet_delay: this.bet_delay, maxprofit: this.maxprofit
      });
    }
  }

  createForm() {
    this.frms = this.formBuilder.group({
      title: ['', Validators.required],
      eid: this.eid,
      sid: this.sportId,
      minstake: [this.minstake],
      maxstake: [this.maxstake],
      maxprofit: [this.maxprofit],
      bet_delay: [this.bet_delay]

    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frms.valid) {
      this.spinner.show();
      this.frms.reset();
      this.service.createmeter(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frms.reset();
      this.spinner.hide();
      this.router.navigate(['/markets/meter/' + this.eid + '/' + this.sportId]);
    }
  }

  getFormData() {
    this.frms.patchValue({
      eid: this.eid, sid: this.sportId
    });
    const data = this.frms.value;
    return data;
  }

  get frmtitle() {
    return this.frms.get('title');
  }

  get frmminstake() {
    return this.frms.get('minstake');
  }

  get frmmaxstake() {
    return this.frms.get('maxstake');
  }

  get frmmaxprofit() {
    return this.frms.get('maxprofit');
  }

  get frmbetdelay() {
    return this.frms.get('bet_delay');
  }

}
