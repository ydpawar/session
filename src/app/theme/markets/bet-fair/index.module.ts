import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BetFairComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
/*import {CreateSingleFancyComponent} from './create-single-fancy/index.component';
import {CreateSingleFancyModule} from './create-single-fancy/index.module';

import {UpdateSingleFancyComponent} from './update-single-fancy/index.component';
import {UpdateSingleFancyModule} from './update-single-fancy/index.module';*/
const routes: Routes = [
  {
    path: '',
    component: BetFairComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
     /* {
        path: 'create-single-fancy/:eid/:sid',
        component: CreateSingleFancyComponent
      },
      {
        path: 'update-single-fancy/:eid/:sid',
        component: UpdateSingleFancyComponent
      },*/
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule
  ], exports: [
    RouterModule
  ], declarations: [ BetFairComponent ]
})
export class BetFairModule {

}
