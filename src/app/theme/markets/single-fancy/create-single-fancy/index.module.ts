import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateSingleFancyComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateSingleFancyComponent
  },
  {
    path: 'create-single-fancy/:eid/:sid',
    component: CreateSingleFancyComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateSingleFancyComponent
  ]
})
export class CreateSingleFancyModule {

}
