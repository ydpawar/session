import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { UpdateSingleFancyComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateSingleFancyComponent
  },
  {
    path: 'update-single-fancy/:eid/:sid/:mid',
    component: UpdateSingleFancyComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateSingleFancyComponent
  ]
})
export class UpdateSingleFancyModule {

}
