import {Component, ViewEncapsulation, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';
import swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthIdentityService} from '../../../../_services';

declare const $: any;

@Component({
  selector: 'app-update-single-fancy',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [MarketService]
})
export class UpdateSingleFancyComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('diff', {static: false}) diff: ElementRef;
  frm: FormGroup;
  eid: string;
  sportId: string;
  mid: string;
  runner: any;
  client: any;
  rate: any;
  size: any;
  // tslint:disable-next-line:variable-name
  event_title: string;
  aList: any = [];
  cItem: string;
  fList: any = [];
  betList: any = [];
  maxLoss: string;
  isBothAll: boolean = false;
  b_id: string;
  favTeam: string;
  user: any;
  // tslint:disable-next-line:ban-types
  diffrance: Number = 1;
  // tslint:disable-next-line:ban-types
  ldiffrance: Number = 0;
  rateArr: any = [];
  ballrunning = false;
  suspend = false;
  lastPrice: string;
  lastYesRate: string;
  lastNoRate: string;
  fancyMarketBookData: any = [];
  timeleft = 60;
  sesstionTimeOut: string;
  stime = false;
  tmp: number;
  downloadTimer11: any;
  downloadTimer: any;
  betListFlag = true;
  public interval1000: any;
  public interval300: any;
  cNoPrice: string;
  cYesPrice: string;
  cNoRate: string;
  cYesRate: string;
  cBallRunning: number;
  cSuspended: number;
  marketStatus: string;
  timerSubscriptionBetList: any;
  activity = 1;
  // aList: UpdateManualFancyManage[] = [];
  // cItem: UpdateManualFancyManage;
  isEmpty = false;
  isDestroy: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
    this.createForm();
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.mid = this.route.snapshot.params.mid;
    this.getSessionData();
    this.getBetData();
  }

  getSessionData() {

    this.service.fancy3MarketLoad(this.mid, this.activity).subscribe((res) => {
      this.activity = 2;
      this.intSessionData(res);
    });
  }

  intSessionData(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      // this.betList = res.data.betList;
      this.mid = res.data.items.market_id;
      this.event_title = res.data.title;
      this.sesstionTimeOut = res.data.items.sessionSuspendedTime;
      this.marketStatus = this.fList.status;
    }
  }

  getBetData() {
    this.service.fancybetList(this.mid).subscribe((res) => {
      this.intBetData(res);
      if (!this.isDestroy) {
        const xhmBetData = setTimeout(() => {
          clearTimeout(xhmBetData);
          this.getBetData();
        }, 3000);
      }
    });
  }

  intBetData(res) {
    if (res !== 1) {
      this.betList = res.data.items;
    }
  }

  ngOnDestroy() {
    this.isDestroy = true;
    this.betList = [];
    if (this.interval1000) {
      clearInterval(this.interval1000);
      clearInterval(this.interval300);
    }

    if (this.timerSubscriptionBetList) {
      this.timerSubscriptionBetList.unsubscribe();
    }

  }

  getFormData() {
    // console.log(this.frm);
    if (this.frm.value.no_val === '0' || this.frm.value.no_val === '00' || this.frm.value.no_val === '000' || this.frm.value.no_val === '0000' || this.frm.value.no_val === '00000' || this.frm.value.no_val === '000000' || this.frm.value.no_val === '') {
      this.ballrunning = true;
      this.cBallRunning = 1;
      this.lastPrice = '';

      const data1 = {ball_running: true, suspended: false, mid: this.mid, sid: this.sportId, event_name: this.event_title};
      if (data1) {
        $('#no_run').val('');
        this.service.singlefancyupdate(data1).subscribe((res) => this.onSuccess(res));
      }
      if (this.tmp === 0) {
        this.tmp = 1;
      }
      clearInterval(this.downloadTimer11);
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }

    } else {


      const elemnt1: any = document.getElementById('no_run') as HTMLElement;
      elemnt1.select();

      this.ballrunning = false;
      this.frm.patchValue({
        yes_val: Number(this.frm.value.no_val) + Number(this.diffrance)
      });
      this.lastPrice = this.frm.value.no_val;
      this.cBallRunning = 0;
      this.cSuspended = 0;
      this.cYesPrice = this.frm.value.yes_val;
      this.cNoPrice = this.frm.value.no_val;

      const data = this.frm.value;
      data.eid = this.eid;
      data.mid = this.mid;
      data.sid = this.sportId;
      data.event_name = this.event_title;
      data.ball_running = false;
      data.suspended = false;
      data.both = '0';

      if (this.isBothAll) { //CHECK BOTH OR NOT
        data.both = '1';
      }
      if (this.frm.valid) {
        this.service.singlefancyupdate(data).subscribe((res) => this.onSuccess(res));
      }

      if (this.tmp === undefined) {
        this.tmp = 0;
      } else {
        clearInterval(this.downloadTimer11);
      }
      if (this.sesstionTimeOut !== '0') {
        this.setSession();
      }
    }
  }

  setSession() {
    let downloadTimer;
    if (this.tmp === 1) {
      clearInterval(downloadTimer);
    }
    let timeleft = parseInt(this.sesstionTimeOut, 10);
    const elemnt: any = document.getElementById('countdowntimer') as HTMLElement;
    // console.log(elemnt);
    elemnt.textContent = timeleft + 's';
    downloadTimer = setInterval(() => {
      timeleft--;
      const nn = timeleft >= 10 ? timeleft : '0' + timeleft;
      elemnt.textContent = nn + 's';
      if (timeleft <= 0) {
        clearInterval(downloadTimer);
      }
      if (timeleft === 0) {
        const elemnt1: any = document.getElementById('suspended') as HTMLElement;
        elemnt1.click();
      }
    }, 1000);
    this.downloadTimer11 = downloadTimer;

    if (timeleft === 0) {
      // debugger;
      const data1 = {ball_running: false, suspended: true, mid: this.mid, sid: this.sportId, event_name: this.event_title};
      this.service.singlefancyupdate(data1).subscribe((res) => this.onSuccess(res));
    }

  }


  createForm() {
    this.frm = this.formBuilder.group({
      no_val: ['', Validators.required],
      yes_val: ['', Validators.required],
      no_rate: ['100'],
      yes_rate: ['100']
    });
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.fList = res.data.items;
      // this.betList = res.data.betList;
    }
  }

  suspended() {
    const data1 = {ball_running: false, suspended: true, mid: this.mid, sid: this.sportId, event_name: this.event_title};
    this.cSuspended = 1;
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    if (data1) {
      this.service.singlefancyupdate(data1).subscribe((res) => this.onSuccess(res));
    }
  }


  ngAfterViewInit() {
    const nthis = this;
    // tslint:disable-next-line:only-arrow-functions
    document.onkeyup = function(e) {
      if (e.shiftKey && (e.which === 192)) { // sft+0
        nthis.diffrance = nthis.ldiffrance = 0;
      } else if (e.shiftKey && (e.which === 49 || e.which === 97)) { // sft+1
        nthis.diffrance = nthis.ldiffrance = 1;
      } else if (e.shiftKey && (e.which === 50 || e.which === 98)) { // sft+2
        nthis.diffrance = nthis.ldiffrance = 2;
      } else if (e.shiftKey && (e.which === 51 || e.which === 99)) { // sft+3
        nthis.diffrance = nthis.ldiffrance = 3;
      } else if (e.shiftKey && (e.which === 52 || e.which === 100)) { // sft+4
        nthis.diffrance = nthis.ldiffrance = 4;
      } else if (e.shiftKey && (e.which === 53 || e.which === 101)) { // sft+5
        nthis.diffrance = nthis.ldiffrance = 5;
      } else if (e.shiftKey && (e.which === 54 || e.which === 102)) { // sft+6
        nthis.diffrance = nthis.ldiffrance = 6;
      } else if (e.shiftKey && (e.which === 55 || e.which === 103)) { // sft+7
        nthis.diffrance = nthis.ldiffrance = 7;
      } else if (e.shiftKey && (e.which === 56 || e.which === 104)) { // sft+8
        nthis.diffrance = nthis.ldiffrance = 8;
      } else if (e.shiftKey && (e.which === 57 || e.which === 105)) { // sft+9
        nthis.diffrance = nthis.ldiffrance = 9;
      } else if (e.shiftKey && e.which === 65) { // sft+a
        nthis.diffrance = nthis.ldiffrance = 1;
        nthis.frm.patchValue({no_rate: '100', yes_rate: '100'});
      } else if (e.shiftKey && e.which === 83) { // sft+s
        nthis.diffrance = nthis.ldiffrance = 0;
        nthis.frm.patchValue({no_rate: '110', yes_rate: '90'});
      } else if (e.shiftKey && e.which === 90) { // sft+z
        nthis.diffrance = nthis.ldiffrance = 0;
        nthis.frm.patchValue({no_rate: '115', yes_rate: '85'});
      } else if (e.shiftKey && e.which === 88) { // sft+x
        nthis.diffrance = nthis.ldiffrance = 0;
        nthis.frm.patchValue({no_rate: '120', yes_rate: '80'});
      } else if (e.shiftKey && e.which === 68) { // sft+d
        nthis.diffrance = nthis.ldiffrance = 0;
        nthis.frm.patchValue({no_rate: '120', yes_rate: '90'});
      } else if (e.shiftKey && e.which === 67) { // sft+c
        nthis.diffrance = nthis.ldiffrance = 0;
        nthis.frm.patchValue({no_rate: '125', yes_rate: '75'});
      }
    };


  }

  setDiff() {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
  }


  diffCheck(element) {
    // debugger;
    if (!this.validate(element.target.value)) {
      this.diffrance = element.target.value.substr(0, element.target.value.length - 1);
      this.ldiffrance = element.target.value.substr(0, element.target.value.length - 1);
    }
  }

  selectTeam(sId) {
    // debugger;
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    this.isBothAll = !this.isBothAll;
    console.log(this.isBothAll);
    if (this.isBothAll) {
      this.b_id = '1';
      if (this.ldiffrance > 0) {
        this.diffrance = this.ldiffrance;
      }
      this.favTeam = 'Only Back';
      this.b_id = '0';
    } else {
      this.ldiffrance = this.diffrance;
      this.favTeam = '';
      this.b_id = '1';


      this.diffrance = 0;
    }
  }


  get frmNoVal() {
    return this.frm.get('no_val');
  }

  get frmYesVal() {
    return this.frm.get('yes_val');
  }


  limit(element) {
    // tslint:disable-next-line:variable-name
    const max_chars = 3;
    if (!this.validate(element.target.value)) {
      this.frm.patchValue({
        no_val: element.target.value.substr(0, element.target.value.length - 1)
      });
    }

    if (element.target.value.length > max_chars) {
      if (this.validate(element.target.value)) {
        this.frm.patchValue({
          no_val: element.target.value.substr(0, max_chars)
        });
      }
    }
  }


  validate(s) {
    // var rgx = /^[0-9]*\.?[0-9]*$/;
    const rgx = /^\d+$/;
    return s.match(rgx);
  }

  addRate(rate: string) {
    const elemnt1: any = document.getElementById('no_run');
    elemnt1.focus();

    const newRate = rate.split('/');
    // this.ldiffrance =  this.diffrance;
    this.diffrance = 0;
    if (newRate[0] === '100' && newRate[1] === '100') {
      this.diffrance = 1;
    }
    this.frm.patchValue({
      yes_rate: newRate[0]
    });
  }

  changeStatus(id) {
    const elemnt1: any = document.getElementById('no_run') as HTMLElement;
    elemnt1.focus();
    swal.fire({
      title: 'Confirm Change Status?',
      text: 'You won\'t be able to recover this entry!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    }).then((result) => {
      if (result.value) {
        this.initStatus(id);
      }
    });
  }

  initStatus(id) {
    const data = {id};
    this.service.singlefancystatus(this.mid, data).subscribe((res) => this.onManualSessionChangeStatusSuccess(res));
  }

  onManualSessionChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.getSessionData();
    }
  }

  getBookDataFancy3New() {
    this.spinner.show();
    const data = {userId: 1, marketId: this.mid};
    this.service.getBookDataFancy3New(data).subscribe((res) => this.onSuccessBookDataFancy3New(res));
  }

  onSuccessBookDataFancy3New(res) {
    if (res.status === 1) {
      const i = 0;
      // let min = res.data.min; let max = res.data.max;
      const dataReturn = [];
      // for ( i = min; i <= max; i++ ) {
      const betlist = res.data.betList;
      let yesPl = 0;
      let noPl = 0;
      let yesWinVal = 0;
      let yesLossVal = 0;
      let noWinVal = 0;
      let noLossVal = 0;
      if (res.data.userRole !== 4) {
        if (betlist !== undefined && betlist != null) {
          // tslint:disable-next-line:only-arrow-functions
          betlist.forEach(function(bets) {
            if (bets.bType === 'back') {
              yesWinVal = yesWinVal + (((bets.win) * (bets.apl) / 100));
              yesLossVal = yesLossVal + (((bets.loss) * (bets.apl) / 100));
            } else if (bets.bType === 'lay') {
              noWinVal = noWinVal + (((bets.win) * (bets.apl) / 100));
              noLossVal = noLossVal + (((bets.loss) * (bets.apl) / 100));
            }
          });
          yesPl = (-1) * (yesWinVal - noLossVal);
          noPl = (-1) * (noWinVal - yesLossVal);
        }
      } else {
        if (betlist !== undefined && betlist != null) {
          // tslint:disable-next-line:only-arrow-functions
          betlist.forEach(function(bets) {
            if (bets.bType === 'back') {
              yesWinVal = yesWinVal + bets.win;
              yesLossVal = yesLossVal + bets.loss;
            } else if (bets.bType === 'lay') {
              noWinVal = noWinVal + bets.win;
              noLossVal = noLossVal + bets.loss;
            }
          });
          yesPl = (yesWinVal - noLossVal);
          noPl = (noWinVal - yesLossVal);
        }
      }

      dataReturn['yesPl'] = Math.round(yesPl);
      dataReturn['noPl'] = Math.round(noPl);
      // }

      $('#bookModal').modal();
      this.fancyMarketBookData = dataReturn;
    }
    this.spinner.hide();
  }


  marketBook() {
    const data1 = {mid: this.mid, sid: this.sportId, eid: this.eid, m_type: 'fancy3'};
    if (data1) {
      this.service.marketBook(data1).subscribe((res) => this.onSuccessBook(res));
    }
  }

  onSuccessBook(res) {

    // console.log(res);
    $('#bookModal').modal();
    if (res.status === 1) {
      this.fancyMarketBookData = [];
      res.data.forEach(item => {
        //  alert(item[0].yes_profitloss);
        this.fancyMarketBookData.push(item);

      });
    }
  }

  closeModal(id: string) {
    $('#' + id).toggleClass('show');
    $('body').removeClass('modal-open');
    clearInterval(this.interval1000);
  }


}
