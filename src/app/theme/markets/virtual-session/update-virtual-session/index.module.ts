import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateVirtualSessionComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UpdateVirtualSessionComponent
  },
  {
    path: 'update-virtual-session/:eid/:sid/:mid',
    component: UpdateVirtualSessionComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateVirtualSessionComponent
  ]
})
export class UpdateVirtualSessionModule {

}
