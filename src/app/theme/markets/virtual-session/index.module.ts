import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VirtualSessionComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateVirtualSessionComponent} from './create-virtual-session/index.component';
import {CreateVirtualSessionModule} from './create-virtual-session/index.module';

import {UpdateVirtualSessionComponent} from './update-virtual-session/index.component';
import {UpdateVirtualSessionModule} from './update-virtual-session/index.module';
const routes: Routes = [
  {
    path: '',
    component: VirtualSessionComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-virtual-session/:eid/:sid',
        component: CreateVirtualSessionComponent
      },
      {
        path: 'update-virtual-session/:eid/:sid',
        component: UpdateVirtualSessionComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateVirtualSessionModule, UpdateVirtualSessionModule
  ], exports: [
    RouterModule
  ], declarations: [ VirtualSessionComponent ]
})
export class VirtualSessionModule {

}
