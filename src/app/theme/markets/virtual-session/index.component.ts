import { Component, OnInit, AfterViewInit } from '@angular/core';


@Component({
  selector: 'app-virtual-session',
  templateUrl: './index.component.html',
})
export class VirtualSessionComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }
}

