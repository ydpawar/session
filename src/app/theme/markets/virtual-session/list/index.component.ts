import {Component, OnInit, AfterViewInit, ViewChild, ElementRef, Renderer2} from '@angular/core';
import {MarketService, GameOverService} from '../../../../_api/index';
import {ActivatedRoute, Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import swal from 'sweetalert2';
import {AuthIdentityService, DataService, ScriptLoaderService, ToastrService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';
import {Subject} from 'rxjs';
declare const $: any;

interface ListOfRunner {
  runner: string;
  secId: string;
}
interface ListManage {
  id: number;
  sid: number;
  eid: any;
  marketId: string;
  title: string;
  info: string;
  runners: any;
  game_over: number;
  result: string;
  bet_allowed: number;
  suspended: number;
  ball_running: number;
  status: {
    text: string;
    cssClass: string;
  };
  tNum: number;
}


@Component({
  selector: 'app-virtual-session',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, GameOverService, NgxSpinnerService]
})
export class ListComponent implements OnInit, AfterViewInit {
  page = {start: 1, end: 5};
  setUrl: any;
  aList: ListManage[] = [];
  rList: ListOfRunner[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;
  id: string;
  sportId: string;
  eventName: string;
  title: string;
  user: any;
  errorMsg: '';
  pageLoad: any;

  dtOptions1: DataTables.Settings = {};
  dtTrigger: Subject<ListManage> = new Subject();

  dataTable: any;
  dtOptions: any;
  tableData = [];

  indexId: number = 0;

  @ViewChild('dataTable', {static: false}) table;

  constructor(private service: MarketService, private service2: GameOverService,
              private loadScript: ScriptLoaderService, private formBuilder: FormBuilder,
              private route: ActivatedRoute, private spinner: NgxSpinnerService, private toastr: ToastrService,
              private dataService: DataService,
              private renderer: Renderer2,
              private elRef: ElementRef,
              private router: Router,
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.sportId = this.route.snapshot.params.sid;
    this.setUrl = this.dataService.getSportMarketPath(this.sportId);
    this.applyFilters();
    this.eventName = window.sessionStorage.getItem('title');
  }

  ngAfterViewInit() {

    document.querySelector('body').addEventListener('click', (event) => {
      const target: any = event.target; // Cast EventTarget into an Element
      if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        // console.log(data);

        if (data.type === 'info') {
          this.changetitle(data.eid, data.marketid, data.title, data.info);
        } else if (data.type === 'status') {
          this.changeStatus(data.eid, data.marketid, data.title, data.info, data.status);
        } else if (data.type === 'game-over') {
          // (eid, mId, runners, markettype, sid, title)
          // alert(data.runners);
          //  console.log( data.runners);
          this.gameOver(data.eid, data.marketid, data.mtype, data.sport, data.title, data.rwindex);
        } else if (data.type === 'game-recall') {
          this.gameRecall(data.marketid, data.eid, data.title);
        } else if (data.type === 'abundant') {
          this.abundant(data.eid, data.marketid, data.mtype, data.sport, data.title);
        } else if (data.type === '') {
          this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/virtual_cricket']);
        }
      } else  if (target.tagName.toLowerCase() === 'i' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'info') {
          this.changetitle(data.eid, data.marketid, data.title, data.info);
        }
      } else if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('linksClicks')) {
        const data = target.dataset;
        if (target.hasAttribute('target')) {
          this.router.navigate([]).then(result => {
            window.open('/markets/virtual-session/update-virtual-session/' + data.id + '/' + data.sport + '/' + data.marketid, '_blank');
          });
        } else {
        /*  this.router.navigate([]).then(result => {
            window.open('/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/virtual_cricket', '_blank');
          });*/
          this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/virtual_cricket']);
        }
      }
    });

  }

  applyFilters() {
    this.spinner.show();
    this.service.virtualSessionListLoad(this.id, this.sportId).subscribe((res) => this.onSuccess1(res));
  }


  onSuccess1(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.tableData = [];

      if (res.data !== 'null') {
        this.aList = res.data;
      } else {
        this.isEmpty = true;
      }
      this.spinner.hide();
      this.tableData = res.data;
      this.initDataTableData();

      this.spinner.hide();
    } else {
      this.spinner.hide();
      this.errorMsg = res.success.message;
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.DataTable(this.dtOptions);
    }
  }

  initDataTableData() {
    // console.log('initDataTableData');
    this.dtOptions = {
      destroy: true,
      data: this.tableData,
      pagingType: 'full_numbers',
      pageLength: 25,
      processing: true,
      ordering:  false,
      // dom: 'Bfrtip',
      // buttons: [
      //   'copy', 'csv', 'excel', 'print'
      // ],
      columns: [
        {title: 'title', data: 'title'},
        {title: 'rule', data: 'info'},
        {
          title: 'Action',
          render: (data: any, type: any, full: any) => {
            // console.log('fl', full);
            let htm = '';
            if (full.game_over === 0) {
              let cls = 'btn-danger';
              let status = 'Inactive';
              if (full.status === 1) {
                cls = 'btn-success';
                status = 'Active';
              }
              htm = '<a  class="btn btn-xs bg-success text-light px-1 mr-1 " data-type="info" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-info="' + full.info + '"><i class="mdi mdi-square-edit-outline modalClicks" data-type="info" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-info="' + full.info + '"></i></a>';
              htm += '<a target="_blank" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-marketid="' + full.marketId + '" class="btn btn-xs bg-dark text-light mr-1 linksClicks">Edit</a>';
              htm += '<a   data-type="status" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-status="' + full.status + '" class="btn btn-xs btn-dark text-light ' + cls + ' success mr-1 modalClicks">' + status + '</a>';
              htm += '<a class="btn btn-xs btn-dark text-light mr-1 linksClicks" data-eid="' + full.eid + '" data-sport="' + this.sportId + '" data-marketid="' + full.marketId + '">Setting</a>';
            }
            return htm;
          }
        },
        {
          title: 'Result',
          render: (data: any, type: any, full: any, meta: any) => {
            // console.log(meta);
            let htm = '';
            if (full.game_over === 0 && this.user.role !== 7 ) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="game-over" data-eid="' + full.eid + '" data-id="' + this.id + '" data-rwindex="' + meta.row + '"  data-sport="' + this.sportId + '" data-title="' + full.title + '" data-mtype="virtual_cricket" data-marketid="' + full.marketId + '">Game Over</a>';
              htm += '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="abundant" data-eid="' + full.eid + '" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-title="' + full.title + '" data-mtype="virtual_cricket" data-marketid="' + full.marketId + '">abundant</a>';
            }

            if (full.game_over === 1 && this.user.role !== 7) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="game-recall" data-eid="' + full.eid + '" data-id="' + this.id + '"  data-title="' + full.title + '" data-marketid="' + full.marketId + '">game recall</a>';
              htm += '<a class="btn btn-xs border border-dark text-dark text-uppercase">result : ' + full.result + '</a>';
            }

            if (full.game_over === 1 && this.user.role === 7) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase">result : ' + full.result + '</a>';
            }

            return htm;
          }
        }
      ]
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }

  filterById(): void {
    this.dataTable.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }



  onSuccess(res) {
    // declare const $: any;
    if (this.pageLoad === '2') {
      $('#scroll-horizontal-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      const items = res.data;
      if (items.length) {
        this.aList = res.data;
      } else {
        this.isEmpty = true;
      }

      this.page.end = this.page.end + items.length - 1;
      this.loadScript.load('script', 'assets/js/datatables.init.js');
      setTimeout(() => {
        this.spinner.hide();
      }, 500);

    } else {
      this.isEmpty = true;
      this.spinner.hide();
      this.errorMsg = res.success.message;
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  changeStatus(eid, marketid, title, info, status) {

    let statustxt = 'Active';
    // @ts-ignore
    if (status === '1') {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + title + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus(marketid, status);
        }
      });
  }

  initStatus(marketid, status) {
    this.service.virtualstatus(marketid, status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }

  changetitle(eventid, mId, title, info) {
    swal.fire({
      html:
        '<input id="title" readonly class="swal2-input" value="' + title + '" placeholder="Title">' +
        '<input id="rules" class="swal2-input" value="' + info + '" placeholder="Rules">',
      preConfirm: () => {
        return new Promise((resolve) => {
          resolve([
            $('#title').val(),
            $('#rules').val()
          ]);
        });
      },
      showCancelButton: true,
      confirmButtonText: 'Save',
      onOpen: () => {
        $('#rules').focus();
      }
    }).then((result) => {
      const data = {eid: eventid, mid: mId, title: result.value[0], info: result.value[1]};
      this.service.changeVirtualtitle(data).subscribe((res) => this.onChangeStatusSuccess(res));
    });
  }

  gameOver(eid, mId, markettype, sid, title, rwId) {
    // console.log(runners);
    const runner = JSON.parse(this.aList[rwId].runners);
    let myArrayOfThings;
    if (runner[2] && runner[2].secId) {
      myArrayOfThings = [
        {id: runner[0].secId + '_' + runner[0].runner, name: runner[0].runner},
        {id: runner[1].secId + '_' + runner[1].runner, name: runner[1].runner},
        {id: runner[2].secId + '_' + runner[2].runner, name: runner[2].runner}
      ];
    } else {
      myArrayOfThings = [
        {id: runner[0].secId + '_' + runner[0].runner, name: runner[0].runner},
        {id: runner[1].secId + '_' + runner[1].runner, name: runner[1].runner},
      ];
    }

    const options = {};
    $.map(myArrayOfThings,
      // tslint:disable-next-line:only-arrow-functions
      function(o) {
        options[o.id] = o.name;
      });
    swal.fire({
      title: 'Confirm Game Over?',
      text: title,
      input: 'select',
      inputOptions: options,
      // content: select,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid, mId, result, markettype, sid);
        }
      });
  }

  initGameOver(id, mId, results, markettype, sid) {
    if (id && results.value) {
      const data = {eventId: id, marketId: mId, winResult: results.value, secId: results.value, mType: 'virtual_cricket', sportId: sid};
      // console.log(data);
      this.service2.gameOverBookmaker(data).subscribe((res) => this.onGameOver(res));
    }
  }

  onGameOver(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, eid, title) {
    swal.fire({
      title: 'Confirm Game Recall?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameRecall(id, eid);
        }
      });
  }

  initGameRecall(id, eid) {
    const data = {marketId: id, eventId: eid, mType: 'virtual_cricket'};
    this.service2.gameRecallBookmaker(data).subscribe((res) => this.onGameOver(res));
  }

  abundant(eid, mid, type, sid, title) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameAbundant(mid, eid);
        }
      });
  }

  initGameAbundant(mid, eid) {
    const data = {marketId: mid, eventId: eid, mType: 'virtual_cricket'};
    this.service2.abundantbookmaker(data).subscribe((res) => this.onGameOver(res));
  }

}
