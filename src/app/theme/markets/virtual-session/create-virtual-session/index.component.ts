import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MarketService } from '../../../../_api/index';
import { NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-create-virtual-session',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService]
})
export class CreateVirtualSessionComponent implements OnInit {
  frm: FormGroup;
  eid: string;
  sportId: string;
  eventname: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.service.geteventName(this.eid).subscribe((res) => this.onSuccessRs(res));
    this.applySetting();
  }

  applySetting() {
    const data = {sid: this.sportId, type: 2 };
    this.service.getsportMarketSetting(data).subscribe((res) => this.onSuccessResult(res));
  }

  onSuccessResult(res) {
    if (res.status !== undefined && res.status === 1) {
      this.frm.patchValue({
        eid: this.eid,
        sid: this.sportId,
        minstake: res.data.min_stake,
        maxstake: res.data.max_stake,
        bet_delay: res.data.bet_delay,
        maxprofit: res.data.max_profit_limit
      });
    }
  }

  onSuccessRs(res) {
    if (res.status === 1) {
      this.eventname = res.eventname;
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: ['', Validators.required],
      runner: ['', Validators.required],
      runner2: ['', Validators.required],
      runner3: [''],
      eid: [''],
      sid: [''],
      minstake: ['', Validators.required],
      maxstake: ['', Validators.required],
      maxprofit: ['', Validators.required],
      bet_delay: ['', Validators.required]
    });
  }


  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
      this.spinner.show();
      this.service.createvirtualsession(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.spinner.hide();
      this.router.navigate(['/markets/virtual-session/' + this.eid + '/' + this.sportId]);
    }
  }

  getFormData() {
    this.frm.patchValue({
      eid: this.eid, sid: this.sportId
    });
    const data = this.frm.value;
    return data;
  }

  get frmtitle() { return this.frm.get('title'); }
  get frmrunner() { return this.frm.get('runner'); }
  get frmrunner2() { return this.frm.get('runner2'); }
  get frmminstake() { return this.frm.get('minstake'); }
  get frmmaxstake() { return this.frm.get('maxstake'); }
  get frmmaxprofit() { return this.frm.get('maxprofit'); }
  get frmbetdelay() {  return this.frm.get('bet_delay'); }



}
