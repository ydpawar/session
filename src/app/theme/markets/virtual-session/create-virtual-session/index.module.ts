import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateVirtualSessionComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateVirtualSessionComponent
  },
  {
    path: 'create-virtual-session/:eid/:sid',
    component: CreateVirtualSessionComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateVirtualSessionComponent
  ]
})
export class CreateVirtualSessionModule {

}
