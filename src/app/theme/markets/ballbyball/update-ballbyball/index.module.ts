import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {UpdateBallbyballComponent} from './index.component';
import {NgxSpinnerModule} from 'ngx-spinner';

const routes: Routes = [
  {
    path: '',
    component: UpdateBallbyballComponent
  },
  {
    path: 'update-ballbyball/:eid/:sid/:mid',
    component: UpdateBallbyballComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    UpdateBallbyballComponent
  ]
})
export class UpdateBallbyballModule {

}
