import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateBallbyballComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: CreateBallbyballComponent
  },
  {
    path: 'create-ballbyball/:eid/:sid',
    component: CreateBallbyballComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, NgxSpinnerModule
  ], exports: [
    RouterModule
  ], declarations: [
    CreateBallbyballComponent
  ]
})
export class CreateBallbyballModule {

}
