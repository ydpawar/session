import {Component, OnInit, AfterViewInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {MarketService} from '../../../../_api/index';
import { NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-create-ballbyball',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, NgxSpinnerService]
})
export class CreateBallbyballComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  runnerArr: any;
  eid: string;
  sportId: string;
  eventname: string;

  constructor(
    private formBuilder: FormBuilder,
    private service: MarketService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    this.createForm();
  }


  ngOnInit() {
    this.eid = this.route.snapshot.params.eid;
    this.sportId = this.route.snapshot.params.sid;
    this.setTitle();
  }

  setTitle() {
    this.service.getEventTitle(this.eid).subscribe((res) => {
      if (res.status === 1) {
        this.eventname = res.data.title;
        this.runnerArr = res.data.runner;
      }
    });

    const data = {sid: this.sportId, type: 3, eid: this.eid};
    this.service.getsportMarketSetting(data).subscribe((res) => this.onSuccessResult(res));
  }

  onSuccessResult(res) {
    if (res.status !== undefined && res.status === 1) {
      this.frm.patchValue({
        eid: this.eid,
        sid: this.sportId,
        minstake: res.data.min_stake,
        maxstake: res.data.max_stake,
        bet_delay: res.data.bet_delay,
        maxprofit: res.data.max_profit_limit
      });
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: ['', Validators.required],
      eid: ['', Validators.required],
      sid: ['', Validators.required],
      minstake: ['', Validators.required],
      maxstake: ['', Validators.required],
      maxprofit: ['', Validators.required],
      bet_delay: ['', Validators.required],
      sec_id: ['', Validators.required],
      start_over: ['', Validators.required],
      end_over: ['', Validators.required]
    });
  }


  submitForm() {
    const data = this.getFormData();
    // console.log(data);
    if (this.frm.valid) {
      this.spinner.show();
      this.service.createballbyball(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.spinner.hide();
      this.router.navigate(['/markets/ballbyball/' + this.eid + '/' + this.sportId]);
    }
  }


  getFormData() {
    this.frm.patchValue({
      eid: this.eid, sid: this.sportId
    });
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmtitle() {
    return this.frm.get('title');
  }

  get frmStartOver() {
    return this.frm.get('start_over');
  }

  get frmEndOver() {
    return this.frm.get('end_over');
  }

  get frmminstake() {
    return this.frm.get('minstake');
  }

  get frmmaxstake() {
    return this.frm.get('maxstake');
  }

  get frmmaxprofit() {
    return this.frm.get('maxprofit');
  }

  get frmbetdelay() {
    return this.frm.get('bet_delay');
  }

}
