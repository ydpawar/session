import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {BallbyballComponent} from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';
import {CreateBallbyballComponent} from './create-ballbyball/index.component';
import {CreateBallbyballModule} from './create-ballbyball/index.module';

import {UpdateBallbyballComponent} from './update-ballbyball/index.component';
import {UpdateBallbyballModule} from './update-ballbyball/index.module';

const routes: Routes = [
  {
    path: '',
    component: BallbyballComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'create-ballbyball/:eid/:sid',
        component: CreateBallbyballComponent
      },
      {
        path: 'update-ballbyball/:eid/:sid',
        component: UpdateBallbyballComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, CreateBallbyballModule, UpdateBallbyballModule
  ], exports: [
    RouterModule
  ], declarations: [BallbyballComponent]
})
export class BallbyballModule {

}
