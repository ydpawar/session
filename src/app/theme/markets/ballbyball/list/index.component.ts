import {Component, OnInit, AfterViewInit, ViewChild, Renderer2, ElementRef} from '@angular/core';
import {MarketService, GameOverService} from '../../../../_api/index';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';
import {AuthIdentityService, ScriptLoaderService, ToastrService, DataService} from '../../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';
import {Subject} from 'rxjs';

declare const $: any;

interface ListOfRunner {
  runner: string;
  secId: string;
}

interface ListManage {
  id: string;
  eid: string;
  page: { start: 1, end: 5 };
  team: string;
  marketId: string;
  result: string;
  info: string;
  title: string;
  sid: string;
  game_over: string;
  SecId: string;
  over: string;
  ball: string;
  editOn: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-ballbyball',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [MarketService, GameOverService, NgxSpinnerService]
})
export class ListComponent implements OnInit, AfterViewInit {

  pageLoad: string;
  SelectedId: string;
  page = {start: 1, end: 5};
  user: any;
  aList: ListManage[] = [];
  rList: ListOfRunner[] = [];
  cItem: ListManage;
  isEmpty = false;
  isLoading = false;
  id: string;
  sportId: string;
  eventName: string;
  setUrl: any;
  errorMsg: '';

  dtOptions1: DataTables.Settings = {};
  dtTrigger: Subject<ListManage> = new Subject();

  dataTable: any;
  dtOptions: any;
  tableData = [];

  @ViewChild('dataTable', {static: false}) table;

  constructor(private service: MarketService, private service2: GameOverService,
              private loadScript: ScriptLoaderService,
              private route: ActivatedRoute, private spinner: NgxSpinnerService, private toastr: ToastrService,
              private dataServices: DataService,
              private renderer: Renderer2,
              private router: Router,
              private elRef: ElementRef
  ) {
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.sportId = this.route.snapshot.params.sid;
    this.eventName = window.sessionStorage.getItem('title');
    this.SelectedId = 'All';
    this.setUrl = this.dataServices.getSportMarketPath(this.sportId);
    this.applyFilters();
    // this.test();
  }

  ngAfterViewInit() {
    document.querySelector('body').addEventListener('click', (event) => {
      const target: any = event.target; // Cast EventTarget into an Element
      if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'info') {
          this.changetitle(data.eid, data.marketid, data.title, data.info);
        } else if (data.type === 'status') {
          this.changeStatus(data.eid, data.marketid, data.title, data.info, data.status);
        } else if (data.type === 'game-over') {
          // eid, mId, title, markettype, sid
          this.gameOver(data.eid, data.marketid, data.title, data.mtype, data.sport);
        } else if (data.type === 'game-recall') {
          this.gameRecall(data.marketid, data.eid, data.title);
        } else if (data.type === 'abundant') {
          this.abundant(data.eid, data.marketid, data.mtype, data.sport, data.title);
        } else if (data.type === '') {
          this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/ballbyball']);
        }
      } else  if (target.tagName.toLowerCase() === 'i' && $(target).hasClass('modalClicks')) {
        const data = target.dataset;
        if (data.type === 'info') {
          this.changetitle(data.eid, data.marketid, data.title, data.info);
        }
      } else if (target.tagName.toLowerCase() === 'a' && $(target).hasClass('linksClicks')) {
        const data = target.dataset;
        if (target.hasAttribute('target')) {
          this.router.navigate([]).then(result => {
            window.open('/markets/ballbyball/update-ballbyball/' + data.id + '/' + data.sport + '/' + data.marketid, '_blank');
          });
        } else {
          /*this.router.navigate([]).then(result => {
            window.open('/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/ballbyball', '_blank');
          });*/
          this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/ballbyball']);
        }

      }
    });

  }

 /* bindEvents() {
    setTimeout(() => {
      const linksClicks = this.elRef.nativeElement.querySelectorAll('.linksClicks');
      const modalClicks = this.elRef.nativeElement.querySelectorAll('.modalClicks');
     // console.log(linksClicks);
      for (let i = 0; i < linksClicks.length; i++) {
        this.renderer.listen(linksClicks[i], 'click', (event) => {
          if (event.target.hasAttribute('target')) {
            const data = event.target.dataset;
            this.router.navigate(['/markets/ballbyball/update-ballbyball/' + data.id + '/' + data.sport + '/' + data.marketid]);
          } else {
            const data = event.target.dataset;
            this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/ballbyball']);
          }
        });
      }

      for (let i = 0; i < modalClicks.length; i++) {
        this.renderer.listen(modalClicks[i], 'click', (event) => {
          const data = event.target.dataset;
        //  console.log('ddd');
          alert(data.type);
          if (data.type === 'info') {
            this.changetitle(data.eid, data.marketid, data.title, data.info);
          } else if (data.type === 'status') {
            this.changeStatus(data.eid, data.marketid, data.title, data.info, data.status);
          } else if (data.type === '') {
            this.router.navigate(['/markets/limits/set-limits/' + data.eid + '/' + data.marketid + '/' + data.sport + '/ballbyball']);
          }
        });
      }
    }, 1000);
  }*/

  test() {
    const that = this;
    // this.tableData = res.data;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        this.service.BallbyballList(this.id, this.sportId, this.SelectedId).subscribe((resp: any) => {
          that.rList = resp.data;
          callback({
            recordsTotal: resp.data.length,
            recordsFiltered: 0,
            data: that.rList
          });
        });
      },
      columns: [
        {title: 'title', data: 'title'},
        {title: 'team', data: 'team'},
        {title: 'over', data: 'over'},
        {title: 'ball', data: 'ball'},
        {title: 'rule', data: 'info'},
      ]
    };
    // this.dtOptions = {
    //   data: this.tableData,
    //   processing: true,
    //   columns: [
    //     {title: 'title', data: 'title'},
    //     {title: 'team', data: 'team'},
    //     {title: 'over', data: 'over'},
    //     {title: 'ball', data: 'ball'},
    //     {title: 'rule', data: 'info'},
    //   ]
    // };
    // this.dataTable = $(this.table.nativeElement);
    // this.dataTable.DataTable(this.dtOptions);

  }

  applyFilters() {
    this.spinner.show();
    this.service.BallbyballList(this.id, this.sportId, this.SelectedId).subscribe((res) => this.onSuccess1(res));
  }

  onSuccess(res) {
    if (this.pageLoad === '2') {
      $('#ballbyball-datatable').dataTable().fnDestroy();
    }

    this.pageLoad = '2';
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.isLoading = false;
      const data: ListManage[] = [];

      if (res.data !== 'null') {
        this.aList = res.data;
        this.rList = res.runners;
      } else {
        this.isEmpty = true;
      }
      // setTimeout(() => {
      this.loadScript.load('script', 'assets/js/datatables.init.js');
      this.spinner.hide();
      // }, 500);
      // this.config.totalItems = res.data.count;

    } else {
      this.spinner.hide();
      this.errorMsg = res.success.message;
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }

  onSuccess1(res) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.tableData = [];

      if (res.data !== 'null') {
        this.aList = res.data;
        this.rList = res.runners;
      } else {
        this.isEmpty = true;
      }
      this.spinner.hide();
      this.tableData = res.data;

      this.initDataTableData();
     // this.bindEvents();

      this.spinner.hide();
    } else {
      this.spinner.hide();
      this.errorMsg = res.success.message;
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.DataTable(this.dtOptions);
    }
  }

  initDataTableData() {
    // console.log('initDataTableData');
    this.dtOptions = {
      destroy: true,
      data: this.tableData,
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      ordering:  false,
      // dom: 'Bfrtip',
      // buttons: [
      //   'copy', 'csv', 'excel', 'print'
      // ],
      columns: [
        {title: 'title', data: 'title'},
        {title: 'team', data: 'team'},
        {title: 'over', data: 'over'},
        {title: 'ball', data: 'ball'},
        {title: 'rule', data: 'info'},
        {
          title: 'Action',
          render: (data: any, type: any, full: any) => {
            // console.log('fl', full);
            let htm = '';
            if (full.game_over === 0) {
              let cls = 'btn-danger';
              let status = 'Inactive';
              if (full.status === 1) {
                cls = 'btn-success';
                status = 'Active';
              }
              htm = '<a  class="btn btn-xs bg-success text-light px-1 mr-1 " data-type="info" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-info="' + full.info + '"><i class="mdi mdi-square-edit-outline modalClicks" data-type="info" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-info="' + full.info + '"></i></a>';
              htm += '<a target="_blank" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-marketid="' + full.marketId + '" class="btn btn-xs bg-dark text-light mr-1 linksClicks">Edit</a>';
              htm += '<a   data-type="status" data-eid="' + full.eid + '" data-marketid="' + full.marketId + '" data-title="' + full.title + '" data-status="' + full.status + '" class="btn btn-xs btn-dark text-light ' + cls + ' success mr-1 modalClicks">' + status + '</a>';
              htm += '<a class="btn btn-xs btn-dark text-light mr-1 linksClicks" data-eid="' + full.eid + '" data-sport="' + this.sportId + '" data-marketid="' + full.marketId + '">Setting</a>';
            }
            return htm;
          }
        },
        {
          title: 'Result',
          render: (data: any, type: any, full: any) => {
            let htm = '';
            if (full.game_over === 0 && this.user.role !== 7 ) {
               htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="game-over" data-eid="' + full.eid + '" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-title="' + full.title + '" data-mtype="ballbyball" data-marketid="' + full.marketId + '">Game Over</a>';
               htm += '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="abundant" data-eid="' + full.eid + '" data-id="' + this.id + '" data-sport="' + this.sportId + '" data-title="' + full.title + '" data-mtype="ballbyball" data-marketid="' + full.marketId + '">abundant</a>';
            }

            if (full.game_over === 1 && this.user.role !== 7) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase mr-1 modalClicks" data-type="game-recall" data-eid="' + full.eid + '" data-id="' + this.id + '"  data-title="' + full.title + '" data-marketid="' + full.marketId + '">game recall</a>';
              htm += '<a class="btn btn-xs border border-dark text-dark text-uppercase">result : ' + full.result + '</a>';
            }

            if (full.game_over === 1 && this.user.role === 7) {
              htm = '<a class="btn btn-xs border border-dark text-dark text-uppercase">result : ' + full.result + '</a>';
            }

            return htm;
          }
        }
      ]
    };
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }

  filterById(): void {
    this.dataTable.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  changeStatus(eid, marketid, title, info, status) {
   // this.cItem = item;

   // alert(status);
    let statustxt = 'Active';
    // @ts-ignore
    if (status === '1') {
      statustxt = 'In-Active';
    }

    swal.fire({
      title: 'You want to ' + statustxt + ' ' + title + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#dd3333',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus(marketid, status);
        }
      });
  }

  initStatus(marketid, status) {
    this.service.ballbyballstatus(marketid, status).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.toastr.success(res.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.message);
    }
  }

  changetitle(eventid, mId, title, info) {
    swal.fire({
      //  title: 'Multiple inputs',
      html:
        '<input id="title" readonly class="swal2-input" value="' + title + '" placeholder="Title">' +
        '<input id="rules" class="swal2-input" value="' + info + '" placeholder="Rules">',
      preConfirm: () => {
        return new Promise((resolve) => {
          resolve([
            $('#title').val(),
            $('#rules').val()
          ]);
        });
      },
      showCancelButton: true,
      confirmButtonText: 'Save',
      onOpen: () => {
        $('#rules').focus();
      }
    }).then((result) => {
      if (result && !result.dismiss) {
        const data = {eid: eventid, mid: mId, title: result.value[0], info: result.value[1]};
        this.service.changeballbyballtitle(data).subscribe((res) => this.onChangeStatusSuccess(res));
      }
    });

  }

  gameOver(eid, mId, title, markettype, sid) {
    swal.fire({
      title: 'Confirm Game Over?',
      text: title,
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off',
        placeholder: 'Winner Result',
        type: 'number',
        pattern: '[0-9]*',
      },
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(eid, mId, result, markettype, sid);
        }
      });

  }

  initGameOver(id, mId, results, markettype, sid) {
    const data = {eventId: id, marketId: mId, winResult: results.value, mType: markettype, sportId: sid};
    // console.log(data);
    this.service2.gameOverManualBallbyball(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, eid, title) {
    swal.fire({
      title: 'Confirm Game Recall?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameRecall(id, eid);
        }
      });
  }

  initGameRecall(id, eid) {
    const data = {marketId: id, eventId: eid, mType: 'ballbyball'};
    this.service2.gameRecallBallbyball(data).subscribe((res) => this.onGameOver(res));
  }

  abundant(eid, mid, type, sid, title) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    })

      .then((result) => {
        if (result.value) {
          this.initGameAbundant(mid, eid);
        }
      });
  }

  initGameAbundant(mid, eid) {
    const data = {marketId: mid, eventId: eid, mType: 'ballbyball'};
    this.service2.abundantballbyball(data).subscribe((res) => this.onGameOver(res));
  }


}
