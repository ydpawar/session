import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../_api/index';
import { Router } from '@angular/router';
import { AuthIdentityService, ToastrService } from './../../_services/index';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DashboardService, NgxSpinnerService]
})
export class HomeComponent implements OnInit {
  dataList: any = [];
  eventList: any = [];
  marketList: any = [];
  isActiveTabs: string;
  isEmpty = true;
  user: any;
  title = 'Dashboard';
  breadcrumb: any = [];
  isDash: any;

  constructor(private service: DashboardService, private route: Router, private spinner: NgxSpinnerService, private authIdentity: AuthIdentityService, private toaster: ToastrService) {
    this.isActiveTabs = 'all';
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.getDataList(this.isActiveTabs);
  }

  async getDataList(type) {
    this.spinner.show();
    await this.service.getList(type).subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      (data) => {
        this.onSuccessDataList(data);
      },
      error => {
        this.spinner.hide();
        this.toaster.error(error.message, 'Something want wrong..!');
      });
  }

  onSuccessDataList(response) {
    if (response.status !== undefined && response.status) {
        this.isEmpty = true;
        this.eventList = this.marketList = this.dataList = [];

        if ( response.data.length > 0 ) {
          this.dataList = response.data;
          if (response.data[0].name === 'Dashboard') {
            this.isDash = 1;
          }
         // alert(this.isDash);
        }

        if ( response.event.length > 0 ) {
          this.isEmpty = false;
          this.eventList = response.event;
        }

        if ( response.sport.length > 0 ) {
          this.isEmpty = false;
          this.marketList = response.sport;
        }
    }

    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
