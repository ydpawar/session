import {Component, OnInit, AfterViewInit} from '@angular/core';
import {CricketService} from '../../../_api/index';
import {FormGroup, FormBuilder} from '@angular/forms';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import {NgxSpinnerService} from 'ngx-spinner';

declare const $: any;

interface SportManage {
  id: string;
  sportId: string;
  sport_name: string;
  overall_profit_limit: string;
  bet_delay: string;
  status: {
    text: string;
    cssClass: string;
  };
}

class SportManageObj implements SportManage {
  id: string;
  sportId: string;
  // tslint:disable-next-line:variable-name
  sport_name: string;
  // tslint:disable-next-line:variable-name
  overall_profit_limit: string;
  // tslint:disable-next-line:variable-name
  bet_delay: string;
  status: {
    text: string;
    cssClass: string;
  };
}


@Component({
  selector: 'app-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class SportComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  frmUpload: FormGroup;
  page = {start: 1, end: 5};
  aList: SportManage[] = [];
  cItem: SportManage;
  isEmpty = false;
  isLoading = false;

  constructor(private service: CricketService, private loadScript: ScriptLoaderService, private formBuilder: FormBuilder, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }


  applyFilters() {
    this.spinner.show();
    const data = '';
    this.service.sportListLoad(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      // this.isLoading = false;
      const items = res.data;
      const data: SportManage[] = [];

      if (items.length) {
        this.aList = [];
        for (const item of items) {
          const cData: SportManage = new SportManageObj();

          cData.id = item.id;
          cData.sportId = item.sportId;
          cData.sport_name = item.name;
          cData.overall_profit_limit = item.overall_profit_limit;
          cData.bet_delay = item.bet_delay;
          cData.status = {
            text: item.status === 1 ? 'Active' : 'Inactive',
            cssClass: item.status === 1 ? 'success' : 'dark'
          };

          data.push(cData);
        }
      } else {
        this.isEmpty = true;
      }

      this.aList = data;
      // console.log(data);
      this.page.end = this.page.end + items.length - 1;
      setTimeout(() => {
        this.loadTreeScript();
      }, 500);
    } else {
      setTimeout(() => {
        this.spinner.hide();
      }, 500);
    }
  }


  createForm() {
    this.frm = this.formBuilder.group({
      max_profit_limit: [''],
      bet_delay: [''],
      id: ['']
    });
  }


  changeStatus(item: SportManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.sport_name + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }


  initStatus() {
    this.service.changestatus(this.cItem.id, this.cItem.status.text).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadTreeScript() {
    this.loadScript.load('script', 'assets/js/datatables.init.js');
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

}
