import {NgModule} from '@angular/core';
// import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import { SportsComponent } from './index.component';

import { SportComponent } from './sport/sport.component';
import { SportModule } from './sport/index.module';


import { CreateSportComponent } from './create-sport/index.component';
import { CreateSportModule } from './create-sport/index.module';

import {AuthGuard, AuthGuardChild} from './../../_guards/auth.guard';
import {AuthIdentityService} from './../../_services/auth-identity.service';

// import {NgxPaginationModule} from 'ngx-pagination';

const routes: Routes = [
  {
    path: '',
    component: SportsComponent,
    children: [
      {
        path: '',
        component: SportComponent,
      },
      {
        path: 'create-sport',
        component: CreateSportComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    SportModule,
    CreateSportModule
  ],
  exports: [RouterModule],
  declarations: [SportsComponent],
})
export class SportsModule {
}
