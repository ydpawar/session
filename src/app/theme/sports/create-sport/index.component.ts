import {Component, OnInit, AfterViewInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CricketService} from '../../../_api/index';

@Component({
  selector: 'app-create-sport',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService]
})
export class CreateSportComponent implements OnInit, AfterViewInit {
  frm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private router: Router,
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      sport_name: ['', Validators.required],
      sport_id: ['', Validators.required],
      overall_profit_limit: ['', Validators.required],
      bet_delay: ['', Validators.required]
    });
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.createnewsportevent(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/sport']);
    }
  }


  getFormData() {
    const data = this.frm.value;
    return data;
  }

  ngAfterViewInit() {

  }

  get frmSportName() {
    return this.frm.get('sport_name');
  }

  get frmSportId() {
    return this.frm.get('sport_id');
  }

  get frmOverall_profit_limit() {
    return this.frm.get('overall_profit_limit');
  }

  get frmBetDelay() {
    return this.frm.get('bet_delay');
  }

}
