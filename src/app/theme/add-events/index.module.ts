import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AddEventsComponent } from './index.component';

import { EventsComponent } from './events/index.component';
import { EventsModule } from './events/index.module';

const routes: Routes = [
  {
    path: '',
    component: AddEventsComponent,
    children: [
      {
        path: '',
        component: EventsComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    EventsModule,
  ],
  exports: [RouterModule],
  declarations: [AddEventsComponent],
})
export class AddEventsModule {
}
