import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

import { EventsComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, NgxSpinnerModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    EventsComponent
  ]
})
export class EventsModule {

}
