import {Component, OnInit, AfterViewInit} from '@angular/core';
import {CricketService} from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import {FormGroup, FormBuilder} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';

declare const $: any;

@Component({
  selector: 'app-sport',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [CricketService, NgxSpinnerService]
})
export class EventsComponent implements OnInit, AfterViewInit {
  frm: FormGroup;
  page = {start: 1, end: 5};
  sportList: any = [];
  aList = [];
  eventsList = [];
  isEmpty = false;
  isLoading = false;
  dataLoad = 0;
  private cItem: any;


  constructor(
    private formBuilder: FormBuilder,
    private service: CricketService,
    private loadScript: ScriptLoaderService,
    private spinner: NgxSpinnerService
  ) {
    this.initSports();
    this.createForm();
  }

  ngOnInit() {

  }

  initSports() {
    this.sportList = [
      {
        name: 'Cricket',
        slug: 'cricket',
        sportId: 4
      },
      {
        name: 'Football',
        slug: 'football',
        sportId: 1
      },
      {
        name: 'Tennies',
        slug: 'tennies',
        sportId: 2
      },
      {
        name: 'Volleyball',
        slug: 'volleyball',
        sportId: 998917
      },
      {
        name: 'Basketball',
        slug: 'basketball',
        sportId: 7522
      },
      {
        name: 'Dart',
        slug: 'dart',
        sportId: 3503
      },
      {
        name: 'Boxing',
        slug: 'boxing',
        sportId: 6
      }
    ];
  }

  createForm() {
    this.frm = this.formBuilder.group({
      dataVal: [''],
    });
  }

  ngAfterViewInit() {
  }

  getSportEvents(sportid, sname: string = '') {
    const s = $('#cricketView').attr('class');
    if (sname && (this.eventsList[sname] === undefined || this.eventsList[sname].length === 0)) {
      this.spinner.show();
      const data = {sid: sportid};
      this.service.eventListLoad(data, sportid).subscribe((res) => this.onSuccessEventsData(res, sname));
    }
  }

  onSuccessEventsData(res, sname) {
    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      const items = res.data[sname];
      if (items) {
        this.eventsList[sname] = items;
      } else {
        this.eventsList[sname] = [];
        //  this.isEmpty = true;
      }
      setTimeout(() => {
        this.loadTreeScript();
      }, 500);
    }
  }


  changeStatus(item, sportId: number, sportSlug: string) {
    this.cItem = item;
    swal.fire({
      title: 'You want to add this event?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    }).then((result) => {
      if (result.value) {
        this.initStatus(sportId, sportSlug);
      }
    });
  }

  initStatus(sportId, sportSlug) {
    const data = this.getFormData();
    this.service.addserries(data).subscribe((res) => this.onChangeStatusSuccess(res, sportId, sportSlug));
  }

  getFormData() {
    this.frm.patchValue({
      dataVal: this.cItem
    });
    const data = this.frm.value;
    return data;
  }

  onChangeStatusSuccess(res, sportId, sportSlug) {
    this.frm.reset();
    if (res.status === 1) {
     // this.getSportEvents(sportId, sportSlug);
    }
  }

  loadTreeScript() {
    const toggler = document.getElementsByClassName('caret');
    let i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener('click', function() {
        // alert('test');
        this.parentElement.querySelector('.nested').classList.toggle('active');
        this.classList.toggle('caret-down');
      });
    }

    setTimeout(() => {
      this.spinner.hide();
    }, 200);
  }

}
