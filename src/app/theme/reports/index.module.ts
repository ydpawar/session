import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { ReportsComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    children: [
      {
        path: 'activity-log',
        loadChildren: './activity-log/index.module#ActivityLogModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [ReportsComponent]
})
export class ReportsModule {
}
